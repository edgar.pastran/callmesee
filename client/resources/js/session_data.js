/**************************************************************************
 * DEFINICION DE VARIABLES GLOBALES - INICIO
 **************************************************************************/
// VARIABLES ASIGNADAS
// Nombre de la aplicacion
var applicationName = "Hola Doctor";
// Cantidad de barras usadas en la animacion de carga
// Se usa con el CSS loading.css
var loadingAnimationBars = 4;
// Tiempo en que se consulta al servidor por nuevos mensajes pendientes
var timeoutLoadNewMessages = 30 * 1000;
// Tiempo (segundos) maximo de espera de un asesor para atender una consulta
var maxTimeWaitForAdviser = 120;
// Tiempo en que se consulta al servidor por la respuesta de un asesor a una consulta
var timeoutCheckAdviserResponse = 5 * 1000;

// VARIABLES CALCULADAS
var urlBase = window.location.protocol + "//" + window.location.host + "/";
var isLocal = ((window.location.host == "localhost") || (window.location.host.startsWith("10.")) || (window.location.host.startsWith("pablotest")));
urlBase += (isLocal) ? (window.location.pathname.split('/')[1] + "/"): "";
// console.log("URL BASE : "+urlBase);
var urlClient = urlBase.concat("client/");
// console.log("URL CLIENT : "+urlClient);
var urlServer = urlBase.concat("server/php/");
// console.log("URL SERVER : "+urlServer);
var currentURL = urlBase.concat(window.location.pathname.substring(window.location.pathname.substring(1).indexOf("/")+2));
// console.log("CURRENT URL : "+currentURL);
var currentPage = currentURL.substring(currentURL.lastIndexOf('/')+1);
// console.log("CURRENT PAGE : "+currentPage);
/**************************************************************************
 * DEFINICION DE VARIABLES GLOBALES - FIN
 **************************************************************************/

/**************************************************************************
 * VALIDACION DE PRIVILEGIOS DE ACCESO DE USUARIO - INICIO
 **************************************************************************/
var usuario = getUserData();
// console.log("USUARIO: "+JSON.stringify(usuario));
var pagesWithoutUser = [
    urlClient.concat("login.html"),
    urlClient.concat("recover.html"),
    urlClient.concat("resend.html"),
    urlClient.concat("signup.html"),
    urlClient.concat("verification.html")
];
if (usuario == null) {
    if (pagesWithoutUser.indexOf(currentURL) < 0) {
        document.location.href = urlClient.concat("login.html");
    }
}
else if (currentURL.indexOf(usuario.directorio_vistas) == -1) {
    document.location.href = urlClient.concat(usuario.directorio_vistas+"index.html");
}
/**************************************************************************
 * VALIDACION DE PRIVILEGIOS DE ACCESO DE USUARIO - FIN
 **************************************************************************/

/**************************************************************************
* DEFINICION DE FUNCIONES PARA LA GESTION DE VARIABLES DE SESION - INICIO
**************************************************************************/
function setSessionData(name, data) {
    window.localStorage.setItem(name, JSON.stringify(data));
}

function getSessionData(name) {
    var data = window.localStorage.getItem(name);
    return (data != undefined)?JSON.parse(data):null;
}

function closeSession() {
    window.localStorage.clear();
    document.location.href = urlClient.concat("login.html");
};

function getUserData() {
    return getSessionData(applicationName.concat('-user'));
}

function setUserData(data) {
    setSessionData(applicationName.concat('-user'), data);
}

function addItemIntoCart(id, quantity, item) {
    var user = getUserData();
    if (!user.hasOwnProperty('cart')) {
        user.cart = {};
    }
    if (id in user.cart) {
        user.cart[id]['quantity'] += parseInt(quantity);
        user.cart[id]['item'] = item;
    }
    else {
        user.cart[id] = {
            'quantity': parseInt(quantity),
            'item': item
        }
    }
    setUserData(user);
}

function deleteItemIntoCart(id) {
    var user = getUserData();
    if ((user.hasOwnProperty('cart')) && (id in user.cart)) {
        delete user.cart[id];
        setUserData(user);
    }
}

function getCartData() {
    var user = getUserData();
    if (user.hasOwnProperty('cart')) {
        return user.cart;
    }
    return null;
}

function clearCart() {
    var user = getUserData();
    if (user.hasOwnProperty('cart')) {
        user.cart = {};
        setUserData(user);
    }
}
/**************************************************************************
 * DEFINICION DE FUNCIONES PARA LA GESTION DE VARIABLES DE SESION - FIN
 **************************************************************************/
