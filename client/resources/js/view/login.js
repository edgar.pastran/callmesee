var urlServerPage = urlServer.concat("login.php");

(function() {
    'use strict';
    $(userSignin);
})();

/**************************************************************************
 * VALIDACION DE FORMULARIO DE LOGIN - INICIO
 **************************************************************************/
function userSignin() {

    if (!$.fn.validate) return;

    var $form = $('#user-login');
    $form.validate({
        errorPlacement: errorPlacementInput,
        // Form rules
        rules: {
            username: {
                required: true,
                email: true,
                rangelength: [6, 50]
            },
            password: {
                required: true,
                rangelength: [6, 20]
            }
        },
        submitHandler: function() {
            login();
        }
    });
}

function login() {
    var usuario = {};
    usuario.id_usuario = $('#username').val();
    usuario.clave = $('#password').val();
    var params = {"operacion": "iniciar_sesion", "usuario": JSON.stringify(usuario)};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log("EXITO TRUE: "+JSON.stringify(respuesta));
                setUserData(respuesta.usuario);
                document.location.href = urlClient.concat(respuesta.usuario.directorio_vistas+"index.html");
            }
            else {
                // console.log("EXITO FALSE: "+JSON.stringify(respuesta));
                swal(respuesta.mensaje, '', 'error');
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}
/**************************************************************************
 * VALIDACION DE FORMULARIO DE LOGIN - FIN
 **************************************************************************/
