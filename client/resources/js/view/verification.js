var urlServerPage = urlServer.concat("verification.php");

$(function() {
    verificateUser();
});

/**************************************************************************
 * VERIFICACION DE LA URL DE LA PAGINA- INICIO
 **************************************************************************/
function verificateUser() {
    var email = getUrlParameter('email');
    var code = getUrlParameter('code');
    if (email != "" && code != "") {
        var usuario = {};
        usuario.id_usuario = email;
        usuario.codigo_verificacion = code;
        var params = {"operacion": "verificar_usuario", "usuario": JSON.stringify(usuario)};
        // console.log("DATA TO SEND: "+JSON.stringify(params));
        $.ajax({
            type: "POST",
            url: urlServerPage,
            data: params,
            success: function(respuesta) {
                if (respuesta.exito == true) {
                    // console.log("EXITO TRUE: "+JSON.stringify(respuesta));
                    swal({
                        title: 'Usuario verificado',
                        text: 'Ya puedes ingresar con tu usuario',
                        type: 'success',
                        confirmButtonText: 'Ok',
                        closeOnConfirm: false
                    },
                    function () {
                        document.location.href = urlClient.concat("login.html");
                    });
                }
                else {
                    // console.log("EXITO FALSE: "+JSON.stringify(respuesta));
                    document.location.href = urlClient.concat("login.html");
                }
            },
            error: function(respuesta) {
                console.log("ERROR: "+JSON.stringify(respuesta));
            }
        });
    }
    else {
        document.location.href = urlClient.concat("login.html");
    }
}
/**************************************************************************
 * VERIFICACION DE LA URL DE LA PAGINA- FIN
 **************************************************************************/
