var urlServerPage = urlServer.concat("admin/packages.php");
var tabla = null;
var column_data = 5;
var formulario = $('#formulario-paquete');

$(function() {
    $(validateForm);
    tabla = createDataTable($('#datatable-paquetes'));
    initTable();
    initSelects();
});

function validateForm() {

    if (!$.fn.validate) return;

    var $form = formulario;
    $form.validate({
        errorPlacement: errorPlacementInput,
        // Form rules
        rules: {
            categoria: {
                required: true
            },
            nombre: {
                required: true,
                rangelength: [1, 50]
            },
            tipo: {
                required: true
            },
            precio: {
                required: true,
                number: true,
                min: {
                    depends: function (element) {
                        var tipo = $('#tipo').val();
                        return ((tipo == "" || tipo == $TIPO_PAQUETE_GRATIS)?parseInt('0'):parseInt('1'));
                    }
                }
            },
            llamadas: {
                required: true,
                number: true,
                min: 1
            },
            minutos: {
                required: true,
                number: true,
                min: 1
            },
            dias: {
                required: false,
                number: true,
                min: 1
            }
        },
        submitHandler: function() {
            processSave();
        }
    });
}

function initTable() {
    var params = {"operacion": "consultar"};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log("EXITO TRUE: "+JSON.stringify(respuesta));
                for (i=0; i<respuesta.data.length; i++) {
                    var record = respuesta.data[i];
                    loadRecord(record);
                }
            }
            else {
                // console.log("EXITO FALSE: "+JSON.stringify(respuesta));
                swal(respuesta.mensaje, '', 'error');
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}

function initSelects() {
    var params = {"operacion": "consultar_categorias_tipos"};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log("EXITO TRUE: "+JSON.stringify(respuesta));
                $('#categoria').text(" ");
                $('#categoria').append('<option value="" selected>Seleccione...</option>');
                for (i=0; i<respuesta.data.categorias.length; i++) {
                    var categoria = respuesta.data.categorias[i];
                    $('#categoria').append('<option value="' + categoria.id_categoria + '">' + categoria.nombre + '</option>');
                }
                $('#tipo').text(" ");
                $('#tipo').append('<option value="" selected>Seleccione...</option>');
                for (i=0; i<respuesta.data.tipos.length; i++) {
                    var tipo = respuesta.data.tipos[i];
                    $('#tipo').append('<option value="' + tipo.id + '">' + tipo.value + '</option>');
                }
            }
            else {
                // console.log("EXITO FALSE: "+JSON.stringify(respuesta));
                swal(respuesta.mensaje, '', 'error');
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}

function loadRecord(record) {
    var acciones = '';
    // Editar
    acciones +=
        '<button class="btn green btn-outline tooltips btn-action edit" type="button" data-original-title="Editar">'+
        '<i class="ion-edit"></i>'+
        '</button>';
    // Consultar
    acciones +=
        '<button class="btn yellow-gold btn-outline tooltips btn-action view" type="button" data-original-title="Consultar">'+
        '<i class="ion-eye"></i>'+
        '</button>';
    if (record.estado == $REGISTRO_ACTIVO) {
        // Desactivar
        acciones +=
            '<button class="btn red btn-outline tooltips btn-action lock" type="button" data-original-title="Desactivar">'+
            '<i class="ion-locked"></i>'+
            '</button>';
    }
    else if (record.estado == $REGISTRO_INACTIVO) {
        // Activar
        acciones +=
            '<button class="btn green-jungle btn-outline tooltips btn-action unlock" type="button" data-original-title="Activar">'+
            '<i class="ion-unlocked"></i>'+
            '</button>';
    }

    // Dibujar la fila
    var fila = tabla.row.add([
        record._categoria_nombre,
        record.nombre,
        $.number(record.precio, 2, $PUNTO_DECIMAL, $PUNTO_MILES),
        record.llamadas,
        acciones,
        JSON.stringify(record)
    ]).draw( false ).node();

    // Personalizar acciones sobre la fila
    $(fila).attr("id", record.id_paquete);
    $(fila).find('td:nth-child(3)').addClass('text-right');
    $(fila).find('td:nth-child(4)').addClass('text-center');
    $(fila).find('td:nth-child(5)').addClass('text-center');
    $(fila).find('.edit').click(function() {
        editRecord(fila);
    });
    $(fila).find('.view').click(function() {
        viewRecord(fila);
    });
    $(fila).find('.lock').click(function() {
        lockRecord(fila);
    });
    $(fila).find('.unlock').click(function() {
        unlockRecord(fila);
    });

    handleTooltips();
}

function loadRecordData(record) {
    $('#id_paquete').val(record.id_paquete);
    $('#categoria').val(record.id_categoria);
    $('#nombre').val(record.nombre);
    $('#tipo').val(record.tipo);
    $('#precio').val(record.precio);
    $('#llamadas').val(record.llamadas);
    $('#minutos').val(record.minutos);
    $('#dias').val(record.dias);
}

function editRecord(fila) {
    var id = $(fila).attr("id");
    var record = JSON.parse(tabla.row('#'+id).data()[column_data]);

    $('#title-operacion').html("Editar");
    cleanForm(formulario);
    $('#operacion').val("editar");
    loadRecordData(record);
    enableForm(formulario);
    checkType();

    $('#modal-paquete').modal();
}

function viewRecord(fila) {
    var id = $(fila).attr("id");
    var record = JSON.parse(tabla.row('#'+id).data()[column_data]);

    $('#title-operacion').html("Consultar");
    cleanForm(formulario);
    $('#operacion').val("consultar");
    loadRecordData(record);
    disableForm(formulario);

    $('#modal-paquete').modal();
}

function lockRecord(fila) {
    var id = $(fila).attr("id");
    var operacion = "desactivar";
    var params = {"operacion": operacion, "id_paquete": id};
    //console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log(JSON.stringify(respuesta));
                tabla.row('#'+id).remove();
                loadRecord(respuesta.data);
                swal('El paquete ha sido desactivado', '', 'success');
            }
            else {
                console.log(JSON.stringify(respuesta));
            }
        },
        error: function(respuesta) {
            console.log(JSON.stringify(respuesta));
        }
    });
}

function unlockRecord(fila) {
    var id = $(fila).attr("id");
    var operacion = "activar";
    var params = {"operacion": operacion, "id_paquete": id};
    //console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log(JSON.stringify(respuesta));
                tabla.row('#'+id).remove();
                loadRecord(respuesta.data);
                swal('El paquete ha sido activado', '', 'success');
            }
            else {
                console.log(JSON.stringify(respuesta));
            }
        },
        error: function(respuesta) {
            console.log(JSON.stringify(respuesta));
        }
    });
}

function updateRecord() {
    var operacion = $('#operacion').val();
    var id = $("#id_paquete").val();
    var paquete = JSON.parse(tabla.row('#'+id).data()[column_data]);
    paquete.id_categoria = $("#categoria").val();
    paquete.nombre = $('#nombre').val();
    paquete.tipo = $('#tipo').val();
    paquete.precio = $('#precio').val();
    paquete.llamadas = $('#llamadas').val();
    paquete.minutos = $('#minutos').val();
    paquete.dias = (($('#dias').val() != "")?$('#dias').val():null);
    var params = {"operacion": operacion, "paquete": paquete};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log(JSON.stringify(respuesta));
                tabla.row('#'+id).remove();
                loadRecord(respuesta.data);
                $('#modal-paquete .close').click();
                swal('El paquete ha sido actualizado', '', 'success');
            }
            else {
                console.log(JSON.stringify(respuesta));
            }
        },
        error: function(respuesta) {
            console.log(JSON.stringify(respuesta));
        }
    });
}

function addRecord() {
    var operacion = $('#operacion').val();
    var paquete = {};
    paquete.id_categoria = $("#categoria").val();
    paquete.nombre = $('#nombre').val();
    paquete.tipo = $('#tipo').val();
    paquete.precio = $('#precio').val();
    paquete.llamadas = $('#llamadas').val();
    paquete.minutos = $('#minutos').val();
    paquete.dias = (($('#dias').val() != "")?$('#dias').val():null);
    var params = {"operacion": operacion, "paquete": paquete};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log(JSON.stringify(respuesta));
                loadRecord(respuesta.data);
                $('#modal-paquete .close').click();
                swal('El paquete ha sido creado', '', 'success');
            }
            else {
                console.log(JSON.stringify(respuesta));
            }
        },
        error: function(respuesta) {
            console.log(JSON.stringify(respuesta));
        }
    });
}

function processSave() {
    var operacion = $('#operacion').val();
    if (operacion != "") {
        if (operacion == "consultar") {
            $('#modal-paquete .close').click();
        }
        else if (operacion == "editar") {
            updateRecord();
        }
        else if (operacion == "nuevo") {
            addRecord();
        }
    }
}

$('#btn-nuevo').click(function() {
    $('#title-operacion').html("Nuevo");
    cleanForm(formulario);
    $('#operacion').val("nuevo");
    enableForm(formulario);

    $('#modal-paquete').modal();
});

function checkType() {
    if ($('#tipo').val() == $TIPO_PAQUETE_GRATIS) {
        disableElement($('#precio'));
        $('#precio').val('0');
    }
    else {
        enableElement($('#precio'));
    }
}

$('#tipo').change(function() {
    checkType();
});
