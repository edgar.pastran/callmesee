var urlServerPage = urlServer.concat("admin/categories.php");
var tabla = null;
var column_data = 3;
var formulario = $('#formulario-categoria');

$(function() {
    $(validateForm);
    tabla = createDataTable($('#datatable-categorias'));
    initTable();
});

function validateForm() {

    if (!$.fn.validate) return;

    var $form = formulario;
    $form.validate({
        errorPlacement: errorPlacementInput,
        // Form rules
        rules: {
            nombre: {
                required: true,
                rangelength: [1, 50]
            },
            descripcion: {
                required: false,
                rangelength: [1, 250]
            }
        },
        submitHandler: function() {
            processSave();
        }
    });
}

function initTable() {
    var params = {"operacion": "consultar"};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log("EXITO TRUE: "+JSON.stringify(respuesta));
                for (i=0; i<respuesta.data.length; i++) {
                    var record = respuesta.data[i];
                    loadRecord(record);
                }
            }
            else {
                // console.log("EXITO FALSE: "+JSON.stringify(respuesta));
                swal(respuesta.mensaje, '', 'error');
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}

function loadRecord(record) {
    var acciones = '';
    // Editar
    acciones +=
        '<button class="btn green btn-outline tooltips btn-action edit" type="button" data-original-title="Editar">'+
        '<i class="ion-edit"></i>'+
        '</button>';
    // Consultar
    acciones +=
        '<button class="btn yellow-gold btn-outline tooltips btn-action view" type="button" data-original-title="Consultar">'+
        '<i class="ion-eye"></i>'+
        '</button>';
    if (record.estado == $REGISTRO_ACTIVO) {
        // Desactivar
        acciones +=
            '<button class="btn red btn-outline tooltips btn-action lock" type="button" data-original-title="Desactivar">'+
            '<i class="ion-locked"></i>'+
            '</button>';
    }
    else if (record.estado == $REGISTRO_INACTIVO) {
        // Activar
        acciones +=
            '<button class="btn green-jungle btn-outline tooltips btn-action unlock" type="button" data-original-title="Activar">'+
            '<i class="ion-unlocked"></i>'+
            '</button>';
    }

    // Dibujar la fila
    var fila = tabla.row.add([
        record.nombre,
        record.descripcion,
        acciones,
        JSON.stringify(record)
    ]).draw( false ).node();

    // Personalizar acciones sobre la fila
    $(fila).attr("id", record.id_categoria);
    $(fila).find('td:nth-child(3)').addClass('text-center');
    $(fila).find('.edit').click(function() {
        editRecord(fila);
    });
    $(fila).find('.view').click(function() {
        viewRecord(fila);
    });
    $(fila).find('.lock').click(function() {
        lockRecord(fila);
    });
    $(fila).find('.unlock').click(function() {
        unlockRecord(fila);
    });

    handleTooltips();
}

function loadRecordData(record) {
    $('#id_categoria').val(record.id_categoria);
    $('#nombre').val(record.nombre);
    $('#descripcion').val(record.descripcion);
}

function editRecord(fila) {
    var id = $(fila).attr("id");
    var record = JSON.parse(tabla.row('#'+id).data()[column_data]);

    $('#title-operacion').html("Editar");
    cleanForm(formulario);
    $('#operacion').val("editar");
    loadRecordData(record);
    enableForm(formulario);

    $('#modal-categoria').modal();
}

function viewRecord(fila) {
    var id = $(fila).attr("id");
    var record = JSON.parse(tabla.row('#'+id).data()[column_data]);

    $('#title-operacion').html("Consultar");
    cleanForm(formulario);
    $('#operacion').val("consultar");
    loadRecordData(record);
    disableForm(formulario);

    $('#modal-categoria').modal();
}

function lockRecord(fila) {
    var id = $(fila).attr("id");
    var operacion = "desactivar";
    var params = {"operacion": operacion, "id_categoria": id};
    //console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log(JSON.stringify(respuesta));
                tabla.row('#'+id).remove();
                loadRecord(respuesta.data);
                swal('La categoria ha sido desactivada', '', 'success');
            }
            else {
                console.log(JSON.stringify(respuesta));
            }
        },
        error: function(respuesta) {
            console.log(JSON.stringify(respuesta));
        }
    });
}

function unlockRecord(fila) {
    var id = $(fila).attr("id");
    var operacion = "activar";
    var params = {"operacion": operacion, "id_categoria": id};
    //console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log(JSON.stringify(respuesta));
                tabla.row('#'+id).remove();
                loadRecord(respuesta.data);
                swal('La categoria ha sido activada', '', 'success');
            }
            else {
                console.log(JSON.stringify(respuesta));
            }
        },
        error: function(respuesta) {
            console.log(JSON.stringify(respuesta));
        }
    });
}

function updateRecord() {
    var operacion = $('#operacion').val();
    var id = $("#id_categoria").val();
    var categoria = JSON.parse(tabla.row('#'+id).data()[column_data]);
    categoria.nombre = $('#nombre').val();
    categoria.descripcion = $('#descripcion').val();
    var params = {"operacion": operacion, "categoria": categoria};
    //console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log(JSON.stringify(respuesta));
                tabla.row('#'+id).remove();
                loadRecord(respuesta.data);
                $('#modal-categoria .close').click();
                swal('La categoria ha sido actualizada', '', 'success');
            }
            else {
                console.log(JSON.stringify(respuesta));
            }
        },
        error: function(respuesta) {
            console.log(JSON.stringify(respuesta));
        }
    });
}

function addRecord() {
    var operacion = $('#operacion').val();
    var categoria = {};
    categoria.nombre = $('#nombre').val();
    categoria.descripcion = $('#descripcion').val();
    var params = {"operacion": operacion, "categoria": categoria};
    //console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log(JSON.stringify(respuesta));
                loadRecord(respuesta.data);
                $('#modal-categoria .close').click();
                swal('La categoria ha sido creada', '', 'success');
            }
            else {
                console.log(JSON.stringify(respuesta));
            }
        },
        error: function(respuesta) {
            console.log(JSON.stringify(respuesta));
        }
    });
}

function processSave() {
    var operacion = $('#operacion').val();
    if (operacion != "") {
        if (operacion == "consultar") {
            $('#modal-categoria .close').click();
        }
        else if (operacion == "editar") {
            updateRecord();
        }
        else if (operacion == "nuevo") {
            addRecord();
        }
    }
}

$('#btn-nuevo').click(function() {
    $('#title-operacion').html("Nueva");
    cleanForm(formulario);
    $('#operacion').val("nuevo");
    enableForm(formulario);

    $('#modal-categoria').modal();
});
