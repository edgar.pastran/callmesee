var urlServerPage = urlServer.concat("admin/customers.php");
var tabla = null;
var column_data = 5;
var formulario = $('#formulario-cliente');

$(function() {
    $(validateForm);
    tabla = createDataTable($('#datatable-clientes'));
    initTable();
});

function validateForm() {

    if (!$.fn.validate) return;

    var $form = formulario;
    $form.validate({
        errorPlacement: errorPlacementInput,
        // Form rules
        rules: {
            correo: {
                required: true,
                email: true
            },
            nombre: {
                required: true,
                rangelength: [2, 50]
            },
            apellido: {
                required: true,
                rangelength: [2, 250]
            },
            telefono: {
                required: false,
                digits: true,
                rangelength: [7, 20]
            }
        },
        submitHandler: function() {
            processSave();
        }
    });
}

function initTable() {
    var params = {"operacion": "consultar"};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log("EXITO TRUE: "+JSON.stringify(respuesta));
                for (i=0; i<respuesta.data.length; i++) {
                    var record = respuesta.data[i];
                    loadRecord(record);
                }
            }
            else {
                // console.log("EXITO FALSE: "+JSON.stringify(respuesta));
                swal(respuesta.mensaje, '', 'error');
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}

function loadRecord(record) {
    var imagen = '';
    imagen += '<img class="rounded-circle thumb32 message_user_from_image" src="'+record._imagen+'" alt="user">';
    var acciones = '';
    // Editar
    // acciones +=
    //     '<button class="btn green btn-outline tooltips btn-action edit" type="button" data-original-title="Editar">'+
    //     '<i class="ion-edit"></i>'+
    //     '</button>';
    // Consultar
    acciones +=
        '<button class="btn yellow-gold btn-outline tooltips btn-action view" type="button" data-original-title="Consultar">'+
        '<i class="ion-eye"></i>'+
        '</button>';
    if (record.estado == $REGISTRO_ACTIVO) {
        // Desactivar
        acciones +=
            '<button class="btn red btn-outline tooltips btn-action lock" type="button" data-original-title="Desactivar">'+
            '<i class="ion-locked"></i>'+
            '</button>';
    }
    else if (record.estado == $REGISTRO_INACTIVO) {
        // Activar
        acciones +=
            '<button class="btn green-jungle btn-outline tooltips btn-action unlock" type="button" data-original-title="Activar">'+
            '<i class="ion-unlocked"></i>'+
            '</button>';
    }

    // Dibujar la fila
    var fila = tabla.row.add([
        record.id_usuario,
        record._nombres,
        record._apellidos,
        imagen,
        acciones,
        JSON.stringify(record)
    ]).draw( false ).node();

    // Personalizar acciones sobre la fila
    $(fila).attr("id", record.id_cliente);
    $(fila).find('td:nth-child(4)').addClass('text-center');
    $(fila).find('td:nth-child(5)').addClass('text-center');
    $(fila).find('.edit').click(function() {
        editRecord(fila);
    });
    $(fila).find('.view').click(function() {
        viewRecord(fila);
    });
    $(fila).find('.lock').click(function() {
        lockRecord(fila);
    });
    $(fila).find('.unlock').click(function() {
        unlockRecord(fila);
    });

    handleTooltips();
}

function loadRecordData(record) {
    $('#id_cliente').val(record.id_cliente);
    $('#correo').val(record.id_usuario);
    $('#nombre').val(record._nombres);
    $('#apellido').val(record._apellidos);
    $('#telefono').val(record._telefono);

    $('#imagen').attr("src", record._imagen);
}

function editRecord(fila) {
    var id = $(fila).attr("id");
    var record = JSON.parse(tabla.row('#'+id).data()[column_data]);

    $('#title-operacion').html("Editar");
    cleanForm(formulario);
    $('#operacion').val("editar");
    // Colocar imagen por defecto
    $('#imagen').attr("src", $ARCHIVO_IMAGEN_USUARIO);
    // Hacer click en el boton Remover Imagen (Nueva)
    $('.btn.red.fileinput-exists').click();
    loadRecordData(record);
    enableForm(formulario);
    // Deshabilitar el correo
    $('#correo').prop("disabled", true);
    // Habilitar boton Remover Imgen
    if ($('#btn_remover_imagen').hasClass('default')) {
        $('#btn_remover_imagen').removeClass('default');
        $('#btn_remover_imagen').addClass('red');
    }
    // Mostrar el primer tab
    $('.nav.nav-tabs .list-group-item:nth-child(1)').click();

    $('#modal-cliente').modal();
}

function viewRecord(fila) {
    var id = $(fila).attr("id");
    var record = JSON.parse(tabla.row('#'+id).data()[column_data]);

    $('#title-operacion').html("Consultar");
    cleanForm(formulario);
    $('#operacion').val("consultar");
    // Colocar imagen por defecto
    $('#imagen').attr("src", $ARCHIVO_IMAGEN_USUARIO);
    // Hacer click en el boton Remover Imagen (Nueva)
    $('.btn.red.fileinput-exists').click();
    loadRecordData(record);
    disableForm(formulario);
    // Deshabilitar boton Remover Imgen
    if ($('#btn_remover_imagen').hasClass('red')) {
        $('#btn_remover_imagen').removeClass('red');
        $('#btn_remover_imagen').addClass('default');
    }
    // Mostrar el primer tab
    $('.nav.nav-tabs .list-group-item:nth-child(1)').click();

    $('#modal-cliente').modal();
}

function lockRecord(fila) {
    var id = $(fila).attr("id");
    var operacion = "desactivar";
    var params = {"operacion": operacion, "id_cliente": id};
    //console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log(JSON.stringify(respuesta));
                tabla.row('#'+id).remove();
                loadRecord(respuesta.data);
                swal('El cliente ha sido desactivado', '', 'success');
            }
            else {
                console.log(JSON.stringify(respuesta));
            }
        },
        error: function(respuesta) {
            console.log(JSON.stringify(respuesta));
        }
    });
}

function unlockRecord(fila) {
    var id = $(fila).attr("id");
    var operacion = "activar";
    var params = {"operacion": operacion, "id_cliente": id};
    //console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log(JSON.stringify(respuesta));
                tabla.row('#'+id).remove();
                loadRecord(respuesta.data);
                swal('El cliente ha sido activado', '', 'success');
            }
            else {
                console.log(JSON.stringify(respuesta));
            }
        },
        error: function(respuesta) {
            console.log(JSON.stringify(respuesta));
        }
    });
}

function updateRecord() {
    var id = $("#id_cliente").val();
    var cliente = JSON.parse(tabla.row('#'+id).data()[column_data]);
    cliente.usuario = {};
    cliente.usuario.id_usuario = $('#correo').val();
    cliente.usuario.nombres = $('#nombre').val();
    cliente.usuario.apellidos = $('#apellido').val();
    cliente.usuario.telefono = $('#telefono').val();

    var params = new FormData();
    params.append('operacion', $('#operacion').val());
    params.append('cliente', JSON.stringify(cliente));
    params.append("imagen", $("#imagen_nueva")[0]["files"][0]);
    //console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        cache: false,
        url: urlServerPage,
        data: params,
        contentType: false,
        processData: false,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log(JSON.stringify(respuesta));
                tabla.row('#'+id).remove();
                loadRecord(respuesta.data);
                $('#modal-cliente .close').click();
                swal('El cliente ha sido actualizado', '', 'success');
            }
            else {
                console.log(JSON.stringify(respuesta));
            }
        },
        error: function(respuesta) {
            console.log(JSON.stringify(respuesta));
        }
    });
}

function addRecord() {
    var cliente = {};
    cliente.usuario = {};
    cliente.usuario.id_usuario = $('#correo').val();
    cliente.usuario.correo = $('#correo').val();
    cliente.usuario.nombres = $('#nombre').val();
    cliente.usuario.apellidos = $('#apellido').val();
    cliente.usuario.telefono = $('#telefono').val();

    var params = new FormData();
    params.append('operacion', $('#operacion').val());
    params.append('cliente', JSON.stringify(cliente));
    params.append("imagen", $("#imagen_nueva")[0]["files"][0]);
    // console.log(params);
    $.ajax({
        type: "POST",
        cache: false,
        url: urlServerPage,
        data: params,
        contentType: false,
        processData: false,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log(JSON.stringify(respuesta));
                loadRecord(respuesta.data);
                $('#modal-cliente .close').click();
                swal('El cliente ha sido creado', '', 'success');
            }
            else {
                swal(respuesta.mensaje, '', 'error');
                console.log(JSON.stringify(respuesta));
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}

function processSave() {
    var operacion = $('#operacion').val();
    if (operacion != "") {
        if (operacion == "consultar") {
            $('#modal-cliente .close').click();
        }
        else if (operacion == "editar") {
            updateRecord();
        }
        else if (operacion == "nuevo") {
            addRecord();
        }
    }
}

$('#btn-nuevo').click(function() {
    $('#title-operacion').html("Nuevo");
    cleanForm(formulario);
    $('#operacion').val("nuevo");
    // Colocar imagen por defecto
    $('#imagen').attr("src", $ARCHIVO_IMAGEN_USUARIO);
    // Hacer click en el boton Remover Imagen (Nueva)
    $('.btn.red.fileinput-exists').click();
    enableForm(formulario);
    // Habilitar boton Remover Imgen
    if ($('#btn_remover_imagen').hasClass('default')) {
        $('#btn_remover_imagen').removeClass('default');
        $('#btn_remover_imagen').addClass('red');
    }
    // Mostrar el primer tab
    $('.nav.nav-tabs .list-group-item:nth-child(1)').click();

    $('#modal-cliente').modal();
});

$('#btn_remover_imagen').click(function(e) {
    e.preventDefault();
    var id_cliente = $('#id_cliente').val();
    if ($('#btn_remover_imagen').hasClass('red') && id_cliente != "") {
        swal({
            title: 'Estas seguro?',
            text: 'Si remueves la imagen se perderá!',
            type: 'warning',
            showCancelButton: true,
            cancelButtonText: 'No, me arrepenti',
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Si, removerla',
            closeOnConfirm: false
        },
        function () {
            removeImage();
        });
    }
});

function removeImage() {
    var id = $('#id_cliente').val();
    var cliente = {};
    cliente.id_cliente = id;
    cliente.usuario = {};
    cliente.usuario.id_usuario = $('#correo').val();
    var params = {"operacion": "remover_imagen", "cliente": JSON.stringify(cliente)};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function (respuesta) {
            if (respuesta.exito == true) {
                // console.log("EXITO TRUE: "+JSON.stringify(respuesta));
                tabla.row('#'+id).remove();
                loadRecord(respuesta.data);
                $('#imagen').attr("src", $ARCHIVO_IMAGEN_USUARIO);
                swal('La imagen ha sido removida', '', 'success');
            }
            else {
                console.log("EXITO FALSE: " + JSON.stringify(respuesta));
            }
        },
        error: function (respuesta) {
            console.log("ERROR: " + JSON.stringify(respuesta));
        }
    });
}