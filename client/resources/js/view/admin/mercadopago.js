var urlServerPage = urlServer.concat("admin/mercadopago.php");
var formulario = $('#formulario-mercadopago');

$(function() {
    $(validateForm);
    loadData();
});

function validateForm() {

    if (!$.fn.validate) return;

    var $form = formulario;
    $form.validate({
        errorPlacement: errorPlacementInput,
        // Form rules
        rules: {
            mercadopago_client_id: {
                required: true,
                digits: true,
                rangelength: [1, 100]
            },
            mercadopago_client_secret: {
                required: true,
                rangelength: [1, 100]
            },
            mercadopago_public_key_sandbox: {
                required: true,
                rangelength: [1, 100]
            },
            mercadopago_access_token_sandbox: {
                required: true,
                rangelength: [1, 100]
            },
            mercadopago_public_key: {
                required: true,
                rangelength: [1, 100]
            },
            mercadopago_access_token: {
                required: true,
                rangelength: [1, 100]
            }
        },
        submitHandler: function() {
            processSave();
        }
    });
}

function loadData() {
    var params = {"operacion": "consultar"};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log("EXITO TRUE: "+JSON.stringify(respuesta));
                showData(respuesta.data);
            }
            else {
                // console.log("EXITO FALSE: "+JSON.stringify(respuesta));
                swal(respuesta.mensaje, '', 'error');
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}

function showData(data) {
    for (i=0; i<data.length; i++) {
        var configuracion = data[i];
        if ($('#'+configuracion.nombre).is(':text')) {
            $('#'+configuracion.nombre).val(configuracion.valor);
        }
        else if ($('#'+configuracion.nombre).is(':checkbox')) {
            $('#'+configuracion.nombre).prop('checked', configuracion.valor == $REGISTRO_ACTIVO);
        }
    }
}

$('#mercadopago_use_sandbox').change(function() {
    $('#mercadopago_use_production').prop('checked', !$('#mercadopago_use_sandbox').is(':checked'));
});

$('#mercadopago_use_production').change(function() {
    $('#mercadopago_use_sandbox').prop('checked', !$('#mercadopago_use_production').is(':checked'));
});

function processSave() {
    var configuraciones = [];
    $.each($(formulario).find('input'), function (i, element) {
        if ($(element).attr('id')) {
            var configuracion = {};
            configuracion.nombre = $(element).attr('id');
            if ($(element).is(':checkbox')) {
                configuracion.valor = ($(element).is(':checked')?$REGISTRO_ACTIVO:$REGISTRO_INACTIVO);
            }
            else {
                configuracion.valor = $(element).val();
            }
            configuraciones.push(configuracion);
        }
    });
    var params = {"operacion": "guardar", "configuraciones": configuraciones};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                showData(respuesta.data);
                swal('Los datos han sido actualizados', '', 'success');
            }
            else {
                // console.log("EXITO FALSE: "+JSON.stringify(respuesta));
                swal(respuesta.mensaje, '', 'error');
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}