var urlServerPage = urlServer.concat("admin/advisers.php");
var tabla = null;
var column_data = 5;
var formulario = $('#formulario-asesor');

$(function() {
    $(validateForm);
    tabla = createDataTable($('#datatable-asesores'));
    initTable();
    initSelects();
});

function validateForm() {

    if (!$.fn.validate) return;

    var $form = formulario;
    $form.validate({
        errorPlacement: errorPlacementInput,
        // Form rules
        rules: {
            correo: {
                required: true,
                email: true
            },
            nombre: {
                required: true,
                rangelength: [2, 50]
            },
            apellido: {
                required: true,
                rangelength: [2, 250]
            },
            telefono: {
                required: false,
                digits: true,
                rangelength: [7, 20]
            }
        },
        submitHandler: function() {
            processSave();
        }
    });
}

function initTable() {
    var params = {"operacion": "consultar"};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log("EXITO TRUE: "+JSON.stringify(respuesta));
                for (i=0; i<respuesta.data.length; i++) {
                    var record = respuesta.data[i];
                    loadRecord(record);
                }
            }
            else {
                // console.log("EXITO FALSE: "+JSON.stringify(respuesta));
                swal(respuesta.mensaje, '', 'error');
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}

function initSelects() {
    var params = {"operacion": "consultar_categorias"};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log("EXITO TRUE: "+JSON.stringify(respuesta));
                $('#categorias').text(" ");
                for (i=0; i<respuesta.data.length; i++) {
                    var categoria = respuesta.data[i];
                    $('#categorias').append('<option value="' + categoria.id_categoria + '">' + categoria.nombre + '</option>');
                }
                $('#categorias').multiSelect({
                    selectableHeader: 'Disponibles',
                    selectionHeader: 'Asignadas'
                });
            }
            else {
                // console.log("EXITO FALSE: "+JSON.stringify(respuesta));
                swal(respuesta.mensaje, '', 'error');
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}

function loadRecord(record) {
    var imagen = '';
    imagen += '<img class="rounded-circle thumb32 message_user_from_image" src="'+record._imagen+'" alt="user">';
    var acciones = '';
    // Editar
    acciones +=
        '<button class="btn green btn-outline tooltips btn-action edit" type="button" data-original-title="Editar">'+
        '<i class="ion-edit"></i>'+
        '</button>';
    // Consultar
    acciones +=
        '<button class="btn yellow-gold btn-outline tooltips btn-action view" type="button" data-original-title="Consultar">'+
        '<i class="ion-eye"></i>'+
        '</button>';
    if (record.estado == $REGISTRO_ACTIVO) {
        // Desactivar
        acciones +=
            '<button class="btn red btn-outline tooltips btn-action lock" type="button" data-original-title="Desactivar">'+
            '<i class="ion-locked"></i>'+
            '</button>';
    }
    else if (record.estado == $REGISTRO_INACTIVO) {
        // Activar
        acciones +=
            '<button class="btn green-jungle btn-outline tooltips btn-action unlock" type="button" data-original-title="Activar">'+
            '<i class="ion-unlocked"></i>'+
            '</button>';
    }

    // Dibujar la fila
    var fila = tabla.row.add([
        record.id_usuario,
        record._nombres,
        record._apellidos,
        imagen,
        acciones,
        JSON.stringify(record)
    ]).draw( false ).node();

    // Personalizar acciones sobre la fila
    $(fila).attr("id", record.id_asesor);
    $(fila).find('td:nth-child(4)').addClass('text-center');
    $(fila).find('td:nth-child(5)').addClass('text-center');
    $(fila).find('.edit').click(function() {
        editRecord(fila);
    });
    $(fila).find('.view').click(function() {
        viewRecord(fila);
    });
    $(fila).find('.lock').click(function() {
        lockRecord(fila);
    });
    $(fila).find('.unlock').click(function() {
        unlockRecord(fila);
    });

    handleTooltips();
}

function loadRecordData(record) {
    $('#id_asesor').val(record.id_asesor);
    $('#correo').val(record.id_usuario);
    $('#nombre').val(record._nombres);
    $('#apellido').val(record._apellidos);
    $('#telefono').val(record._telefono);

    $('#categorias').val(record.categorias);
    $('#categorias').multiSelect('select', record.categorias);

    $('#imagen').attr("src", record._imagen);
}

function editRecord(fila) {
    var id = $(fila).attr("id");
    var record = JSON.parse(tabla.row('#'+id).data()[column_data]);

    $('#title-operacion').html("Editar");
    cleanForm(formulario);
    $('#operacion').val("editar");
    // Resetear la lista de categorias
    $('#categorias').multiSelect('deselect_all');
    // Colocar imagen por defecto
    $('#imagen').attr("src", $ARCHIVO_IMAGEN_USUARIO);
    // Hacer click en el boton Remover Imagen (Nueva)
    $('.btn.red.fileinput-exists').click();
    loadRecordData(record);
    enableForm(formulario);
    // Deshabilitar el correo
    $('#correo').prop("disabled", true);
    // Refrescar la lista de categorias
    $('#categorias').multiSelect('refresh');
    // Habilitar boton Remover Imgen
    if ($('#btn_remover_imagen').hasClass('default')) {
        $('#btn_remover_imagen').removeClass('default');
        $('#btn_remover_imagen').addClass('red');
    }
    // Mostrar el primer tab
    $('.nav.nav-tabs .list-group-item:nth-child(1)').click();

    $('#modal-asesor').modal();
}

function viewRecord(fila) {
    var id = $(fila).attr("id");
    var record = JSON.parse(tabla.row('#'+id).data()[column_data]);

    $('#title-operacion').html("Consultar");
    cleanForm(formulario);
    $('#operacion').val("consultar");
    // Resetear la lista de categorias
    $('#categorias').multiSelect('deselect_all');
    // Colocar imagen por defecto
    $('#imagen').attr("src", $ARCHIVO_IMAGEN_USUARIO);
    // Hacer click en el boton Remover Imagen (Nueva)
    $('.btn.red.fileinput-exists').click();
    loadRecordData(record);
    disableForm(formulario);
    // Refrescar la lista de categorias
    $('#categorias').multiSelect('refresh');
    // Deshabilitar boton Remover Imgen
    if ($('#btn_remover_imagen').hasClass('red')) {
        $('#btn_remover_imagen').removeClass('red');
        $('#btn_remover_imagen').addClass('default');
    }
    // Mostrar el primer tab
    $('.nav.nav-tabs .list-group-item:nth-child(1)').click();

    $('#modal-asesor').modal();
}

function lockRecord(fila) {
    var id = $(fila).attr("id");
    var operacion = "desactivar";
    var params = {"operacion": operacion, "id_asesor": id};
    //console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log(JSON.stringify(respuesta));
                tabla.row('#'+id).remove();
                loadRecord(respuesta.data);
                swal('El asesor ha sido desactivado', '', 'success');
            }
            else {
                console.log(JSON.stringify(respuesta));
            }
        },
        error: function(respuesta) {
            console.log(JSON.stringify(respuesta));
        }
    });
}

function unlockRecord(fila) {
    var id = $(fila).attr("id");
    var operacion = "activar";
    var params = {"operacion": operacion, "id_asesor": id};
    //console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log(JSON.stringify(respuesta));
                tabla.row('#'+id).remove();
                loadRecord(respuesta.data);
                swal('El asesor ha sido activado', '', 'success');
            }
            else {
                console.log(JSON.stringify(respuesta));
            }
        },
        error: function(respuesta) {
            console.log(JSON.stringify(respuesta));
        }
    });
}

function updateRecord() {
    var id = $("#id_asesor").val();
    var asesor = JSON.parse(tabla.row('#'+id).data()[column_data]);
    asesor.usuario = {};
    asesor.usuario.id_usuario = $('#correo').val();
    asesor.usuario.nombres = $('#nombre').val();
    asesor.usuario.apellidos = $('#apellido').val();
    asesor.usuario.telefono = $('#telefono').val();
    asesor.categorias = $('#categorias').val();

    var params = new FormData();
    params.append('operacion', $('#operacion').val());
    params.append('asesor', JSON.stringify(asesor));
    params.append("imagen", $("#imagen_nueva")[0]["files"][0]);
    //console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        cache: false,
        url: urlServerPage,
        data: params,
        contentType: false,
        processData: false,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log(JSON.stringify(respuesta));
                tabla.row('#'+id).remove();
                loadRecord(respuesta.data);
                $('#modal-asesor .close').click();
                swal('El asesor ha sido actualizado', '', 'success');
            }
            else {
                console.log(JSON.stringify(respuesta));
            }
        },
        error: function(respuesta) {
            console.log(JSON.stringify(respuesta));
        }
    });
}

function addRecord() {
    var asesor = {};
    asesor.usuario = {};
    asesor.usuario.id_usuario = $('#correo').val();
    asesor.usuario.correo = $('#correo').val();
    asesor.usuario.nombres = $('#nombre').val();
    asesor.usuario.apellidos = $('#apellido').val();
    asesor.usuario.telefono = $('#telefono').val();
    asesor.categorias = $('#categorias').val();

    var params = new FormData();
    params.append('operacion', $('#operacion').val());
    params.append('asesor', JSON.stringify(asesor));
    params.append("imagen", $("#imagen_nueva")[0]["files"][0]);
    // console.log(params);
    $.ajax({
        type: "POST",
        cache: false,
        url: urlServerPage,
        data: params,
        contentType: false,
        processData: false,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log(JSON.stringify(respuesta));
                loadRecord(respuesta.data);
                $('#modal-asesor .close').click();
                swal('El asesor ha sido creado', '', 'success');
            }
            else {
                swal(respuesta.mensaje, '', 'error');
                console.log(JSON.stringify(respuesta));
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}

function processSave() {
    var operacion = $('#operacion').val();
    if (operacion != "") {
        if (operacion == "consultar") {
            $('#modal-asesor .close').click();
        }
        else if (operacion == "editar") {
            updateRecord();
        }
        else if (operacion == "nuevo") {
            addRecord();
        }
    }
}

$('#btn-nuevo').click(function() {
    $('#title-operacion').html("Nuevo");
    cleanForm(formulario);
    $('#operacion').val("nuevo");
    // Resetear la lista de categorias
    $('#categorias').multiSelect('deselect_all');
    // Colocar imagen por defecto
    $('#imagen').attr("src", $ARCHIVO_IMAGEN_USUARIO);
    // Hacer click en el boton Remover Imagen (Nueva)
    $('.btn.red.fileinput-exists').click();
    enableForm(formulario);
    // Refrescar la lista de categorias
    $('#categorias').multiSelect('refresh');
    // Habilitar boton Remover Imgen
    if ($('#btn_remover_imagen').hasClass('default')) {
        $('#btn_remover_imagen').removeClass('default');
        $('#btn_remover_imagen').addClass('red');
    }
    // Mostrar el primer tab
    $('.nav.nav-tabs .list-group-item:nth-child(1)').click();

    $('#modal-asesor').modal();
});

$('#btn_remover_imagen').click(function(e) {
    e.preventDefault();
    var id_asesor = $('#id_asesor').val();
    if ($('#btn_remover_imagen').hasClass('red') && id_asesor != "") {
        swal({
            title: 'Estas seguro?',
            text: 'Si remueves la imagen se perderá!',
            type: 'warning',
            showCancelButton: true,
            cancelButtonText: 'No, me arrepenti',
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Si, removerla',
            closeOnConfirm: false
        },
        function () {
            removeImage();
        });
    }
});

function removeImage() {
    var id = $('#id_asesor').val();
    var asesor = {};
    asesor.id_asesor = id;
    asesor.usuario = {};
    asesor.usuario.id_usuario = $('#correo').val();
    var params = {"operacion": "remover_imagen", "asesor": JSON.stringify(asesor)};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function (respuesta) {
            if (respuesta.exito == true) {
                // console.log("EXITO TRUE: "+JSON.stringify(respuesta));
                tabla.row('#'+id).remove();
                loadRecord(respuesta.data);
                $('#imagen').attr("src", $ARCHIVO_IMAGEN_USUARIO);
                swal('La imagen ha sido removida', '', 'success');
            }
            else {
                console.log("EXITO FALSE: " + JSON.stringify(respuesta));
            }
        },
        error: function (respuesta) {
            console.log("ERROR: " + JSON.stringify(respuesta));
        }
    });
}