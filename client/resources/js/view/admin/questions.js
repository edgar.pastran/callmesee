var urlServerPage = urlServer.concat("admin/questions.php");
// Preguntas
var tabla = null;
var column_data = 4;
var formulario = $('#formulario-pregunta');
// Repuestas
var tabla_respuestas = null;
var column_data_respuesta = 3;
var formulario_respuesta = $('#formulario-respuesta');

$(function() {
    $(validateForm);
    tabla = createDataTable($('#datatable-preguntas'));
    initTable();
    tabla_respuestas = createDataTable($('#datatable-respuestas'), false)
    $('div.toolbar').html(
        '<button id="btn-nueva-respuesta" class="btn btn-labeled btn-primary" type="button" onclick="nuevaRespuesta();">' +
        '<span class="btn-label">' +
        '<i class="ion-plus-round"></i>' +
        '</span>' +
        'Nuevo' +
        '</button>');
    initSelects();
});

function validateForm() {

    if (!$.fn.validate) return;

    var $form = formulario;
    $form.validate({
        errorPlacement: errorPlacementInput,
        // Form rules
        rules: {
            categoria: {
                required: true
            },
            enunciado: {
                required: true,
                rangelength: [1, 250]
            },
            orden: {
                required: true,
                number: true,
                min: 1
            }
        },
        submitHandler: function() {
            processSave();
        }
    });

    var $form2 = formulario_respuesta;
    $form2.validate({
        errorPlacement: errorPlacementInput,
        // Form rules
        rules: {
            enunciado_respuesta: {
                required: true,
                rangelength: [1, 250]
            },
            orden_respuesta: {
                required: {
                    depends: function (element) {
                        return ($('#operacion_respuesta').val() == "editar_respuesta");
                    }
                },
                number: {
                    depends: function (element) {
                        return ($('#operacion_respuesta').val() == "editar_respuesta");
                    }
                },
                min: {
                    depends: function (element) {
                        return (($('#operacion_respuesta').val() == "editar_respuesta")?parseInt('1'):parseInt('0'));
                    }
                }
            }
        },
        submitHandler: function() {
            processSaveResponse();
        }
    });
}

function initTable() {
    var params = {"operacion": "consultar"};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log("EXITO TRUE: "+JSON.stringify(respuesta));
                for (i=0; i<respuesta.data.length; i++) {
                    var record = respuesta.data[i];
                    loadRecord(record);
                }
            }
            else {
                // console.log("EXITO FALSE: "+JSON.stringify(respuesta));
                swal(respuesta.mensaje, '', 'error');
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}

function initSelects() {
    var params = {"operacion": "consultar_categorias"};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log("EXITO TRUE: "+JSON.stringify(respuesta));
                $('#categoria').text(" ");
                $('#categoria').append('<option value="" selected>Seleccione...</option>');
                for (i=0; i<respuesta.data.length; i++) {
                    var categoria = respuesta.data[i];
                    $('#categoria').append('<option value="' + categoria.id_categoria + '">' + categoria.nombre + '</option>');
                }
            }
            else {
                // console.log("EXITO FALSE: "+JSON.stringify(respuesta));
                swal(respuesta.mensaje, '', 'error');
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}

function loadRecord(record) {
    var acciones = '';
    // Respuestas
    acciones +=
        '<button class="btn purple-intense btn-outline tooltips btn-action responses" type="button" data-original-title="Respuestas">'+
        '<i class="ion-navicon-round"></i>'+
        '</button>';
    // Editar
    acciones +=
        '<button class="btn green btn-outline tooltips btn-action edit" type="button" data-original-title="Editar">'+
        '<i class="ion-edit"></i>'+
        '</button>';
    // Consultar
    acciones +=
        '<button class="btn yellow-gold btn-outline tooltips btn-action view" type="button" data-original-title="Consultar">'+
        '<i class="ion-eye"></i>'+
        '</button>';
    if (record.estado == $REGISTRO_ACTIVO) {
        // Desactivar
        acciones +=
            '<button class="btn red btn-outline tooltips btn-action lock" type="button" data-original-title="Desactivar">'+
            '<i class="ion-locked"></i>'+
            '</button>';
    }
    else if (record.estado == $REGISTRO_INACTIVO) {
        // Activar
        acciones +=
            '<button class="btn green-jungle btn-outline tooltips btn-action unlock" type="button" data-original-title="Activar">'+
            '<i class="ion-unlocked"></i>'+
            '</button>';
    }

    // Dibujar la fila
    var fila = tabla.row.add([
        record._categoria_nombre,
        record.enunciado,
        record.orden,
        acciones,
        JSON.stringify(record)
    ]).draw( false ).node();

    // Personalizar acciones sobre la fila
    $(fila).attr("id", record.id_pregunta);
    $(fila).find('td:nth-child(3)').addClass('text-center');
    $(fila).find('td:nth-child(4)').addClass('text-center');
    $(fila).find('.edit').click(function() {
        editRecord(fila);
    });
    $(fila).find('.view').click(function() {
        viewRecord(fila);
    });
    $(fila).find('.lock').click(function() {
        lockRecord(fila);
    });
    $(fila).find('.unlock').click(function() {
        unlockRecord(fila);
    });
    $(fila).find('.responses').click(function() {
        loadResponses(fila);
    });

    handleTooltips();
}

function loadRecordData(record) {
    $('#id_pregunta').val(record.id_pregunta);
    $('#categoria').val(record.id_categoria);
    $('#enunciado').val(record.enunciado);
    $('#orden').val(record.orden);
}

function editRecord(fila) {
    var id = $(fila).attr("id");
    var record = JSON.parse(tabla.row('#'+id).data()[column_data]);

    $('#title-operacion').html("Editar");
    cleanForm(formulario);
    $('#operacion').val("editar");
    loadRecordData(record);
    enableForm(formulario);

    $('#modal-pregunta').modal();
}

function viewRecord(fila) {
    var id = $(fila).attr("id");
    var record = JSON.parse(tabla.row('#'+id).data()[column_data]);

    $('#title-operacion').html("Consultar");
    cleanForm(formulario);
    $('#operacion').val("consultar");
    loadRecordData(record);
    disableForm(formulario);

    $('#modal-pregunta').modal();
}

function lockRecord(fila) {
    var id = $(fila).attr("id");
    var operacion = "desactivar";
    var params = {"operacion": operacion, "id_pregunta": id};
    //console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log(JSON.stringify(respuesta));
                tabla.row('#'+id).remove();
                loadRecord(respuesta.data);
                swal('La pregunta ha sido desactivada', '', 'success');
            }
            else {
                console.log(JSON.stringify(respuesta));
            }
        },
        error: function(respuesta) {
            console.log(JSON.stringify(respuesta));
        }
    });
}

function unlockRecord(fila) {
    var id = $(fila).attr("id");
    var operacion = "activar";
    var params = {"operacion": operacion, "id_pregunta": id};
    //console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log(JSON.stringify(respuesta));
                tabla.row('#'+id).remove();
                loadRecord(respuesta.data);
                swal('La pregunta ha sido activada', '', 'success');
            }
            else {
                console.log(JSON.stringify(respuesta));
            }
        },
        error: function(respuesta) {
            console.log(JSON.stringify(respuesta));
        }
    });
}

function updateRecord() {
    var operacion = $('#operacion').val();
    var id = $("#id_pregunta").val();
    var pregunta = JSON.parse(tabla.row('#'+id).data()[column_data]);
    pregunta.id_categoria = $("#categoria").val();
    pregunta.enunciado = $('#enunciado').val();
    pregunta.orden = $('#orden').val();
    var params = {"operacion": operacion, "pregunta": pregunta};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log(JSON.stringify(respuesta));
                tabla.row('#'+id).remove();
                loadRecord(respuesta.data);
                $('#modal-pregunta .close').click();
                swal('La pregunta ha sido actualizada', '', 'success');
            }
            else {
                console.log(JSON.stringify(respuesta));
            }
        },
        error: function(respuesta) {
            console.log(JSON.stringify(respuesta));
        }
    });
}

function addRecord() {
    var operacion = $('#operacion').val();
    var pregunta = {};
    pregunta.id_categoria = $("#categoria").val();
    pregunta.enunciado = $('#enunciado').val();
    pregunta.orden = $('#orden').val();
    var params = {"operacion": operacion, "pregunta": pregunta};
    //console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log(JSON.stringify(respuesta));
                loadRecord(respuesta.data);
                $('#modal-pregunta .close').click();
                swal('La pregunta ha sido creada', '', 'success');
            }
            else {
                console.log(JSON.stringify(respuesta));
            }
        },
        error: function(respuesta) {
            console.log(JSON.stringify(respuesta));
        }
    });
}

function processSave() {
    var operacion = $('#operacion').val();
    if (operacion != "") {
        if (operacion == "consultar") {
            $('#modal-pregunta .close').click();
        }
        else if (operacion == "editar") {
            updateRecord();
        }
        else if (operacion == "nuevo") {
            addRecord();
        }
    }
}

$('#btn-nuevo').click(function() {
    $('#title-operacion').html("Nueva");
    cleanForm(formulario);
    $('#operacion').val("nuevo");
    enableForm(formulario);

    $('#modal-pregunta').modal();
});

function loadResponses(fila) {
    var id = $(fila).attr("id");
    $('#id_pregunta_respuesta').val(id);
    tabla_respuestas.clear().draw();

    var params = {"operacion": "consultar_respuestas", "id_pregunta": id};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log("EXITO TRUE: "+JSON.stringify(respuesta));
                for (i=0; i<respuesta.data.length; i++) {
                    var response = respuesta.data[i];
                    loadResponse(response);
                }
            }
            else {
                // console.log("EXITO FALSE: "+JSON.stringify(respuesta));
                swal(respuesta.mensaje, '', 'error');
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });

    $('#modal-respuestas').modal();
}

function loadResponse(response) {
    var acciones = '';
    // Editar
    acciones +=
        '<button class="btn green btn-outline tooltips btn-action edit" type="button" data-original-title="Editar">'+
        '<i class="ion-edit"></i>'+
        '</button>';
    // Consultar
    acciones +=
        '<button class="btn yellow-gold btn-outline tooltips btn-action view" type="button" data-original-title="Consultar">'+
        '<i class="ion-eye"></i>'+
        '</button>';
    if (response.estado == $REGISTRO_ACTIVO) {
        // Desactivar
        acciones +=
            '<button class="btn red btn-outline tooltips btn-action lock" type="button" data-original-title="Desactivar">'+
            '<i class="ion-locked"></i>'+
            '</button>';
    }
    else if (response.estado == $REGISTRO_INACTIVO) {
        // Activar
        acciones +=
            '<button class="btn green-jungle btn-outline tooltips btn-action unlock" type="button" data-original-title="Activar">'+
            '<i class="ion-unlocked"></i>'+
            '</button>';
    }

    // Dibujar la fila
    var fila = tabla_respuestas.row.add([
        response.orden,
        response.enunciado,
        acciones,
        JSON.stringify(response)
    ]).draw( false ).node();

    // Personalizar acciones sobre la fila
    $(fila).attr("id", response.id_respuesta);
    $(fila).find('td:nth-child(1)').addClass('text-center');
    $(fila).find('td:nth-child(3)').addClass('text-center');
    $(fila).find('.edit').click(function() {
        editResponse(fila);
    });
    $(fila).find('.view').click(function() {
        viewResponse(fila);
    });
    $(fila).find('.lock').click(function() {
        lockResponse(fila);
    });
    $(fila).find('.unlock').click(function() {
        unlockResponse(fila);
    });

    handleTooltips();
}

function loadResponseData(response) {
    $('#id_respuesta').val(response.id_respuesta);
    $('#enunciado_respuesta').val(response.enunciado);
    $('#orden_respuesta').val(response.orden);
}

function editResponse(fila) {
    var id = $(fila).attr("id");
    var response = JSON.parse(tabla_respuestas.row('#'+id).data()[column_data_respuesta]);

    $('#title-operacion-respuesta').html("Editar");
    cleanForm(formulario_respuesta);
    $('#operacion_respuesta').val("editar_respuesta");
    loadResponseData(response);
    enableForm(formulario_respuesta);

    $('#modal-respuesta').modal();
}

function viewResponse(fila) {
    var id = $(fila).attr("id");
    var response = JSON.parse(tabla_respuestas.row('#'+id).data()[column_data_respuesta]);

    $('#title-operacion-respuesta').html("Consultar");
    cleanForm(formulario_respuesta);
    $('#operacion_respuesta').val("consultar_respuesta");
    loadResponseData(response);
    disableForm(formulario_respuesta);

    $('#modal-respuesta').modal();
}

function lockResponse(fila) {
    var id = $(fila).attr("id");
    var operacion = "desactivar_respuesta";
    var params = {"operacion": operacion, "id_respuesta": id};
    //console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log(JSON.stringify(respuesta));
                tabla_respuestas.row('#'+id).remove();
                loadResponse(respuesta.data);
                swal('La respuesta ha sido desactivada', '', 'success');
            }
            else {
                console.log(JSON.stringify(respuesta));
            }
        },
        error: function(respuesta) {
            console.log(JSON.stringify(respuesta));
        }
    });
}

function unlockResponse(fila) {
    var id = $(fila).attr("id");
    var operacion = "activar_respuesta";
    var params = {"operacion": operacion, "id_respuesta": id};
    //console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log(JSON.stringify(respuesta));
                tabla_respuestas.row('#'+id).remove();
                loadResponse(respuesta.data);
                swal('La respuesta ha sido activada', '', 'success');
            }
            else {
                console.log(JSON.stringify(respuesta));
            }
        },
        error: function(respuesta) {
            console.log(JSON.stringify(respuesta));
        }
    });
}

function updateResponse() {
    var operacion = $('#operacion_respuesta').val();
    var id = $("#id_respuesta").val();
    var respuesta = JSON.parse(tabla_respuestas.row('#'+id).data()[column_data_respuesta]);
    respuesta.enunciado = $('#enunciado_respuesta').val();
    respuesta.orden = $('#orden_respuesta').val();
    var params = {"operacion": operacion, "respuesta": respuesta};
    //console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log(JSON.stringify(respuesta));
                tabla_respuestas.row('#'+id).remove();
                loadResponse(respuesta.data);
                $('#modal-respuesta .close').click();
                swal('La respuesta ha sido actualizada', '', 'success');
            }
            else {
                console.log(JSON.stringify(respuesta));
            }
        },
        error: function(respuesta) {
            console.log(JSON.stringify(respuesta));
        }
    });
}

function addResponse() {
    var operacion = $('#operacion_respuesta').val();
    var respuesta = {};
    respuesta.id_pregunta = $("#id_pregunta_respuesta").val();
    respuesta.enunciado = $('#enunciado_respuesta').val();
    var params = {"operacion": operacion, "respuesta": respuesta};
    console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                console.log(JSON.stringify(respuesta));
                loadResponse(respuesta.data);
                $('#modal-respuesta .close').click();
                swal('La respuesta ha sido creada', '', 'success');
            }
            else {
                console.log(JSON.stringify(respuesta));
            }
        },
        error: function(respuesta) {
            console.log(JSON.stringify(respuesta));
        }
    });
}

function processSaveResponse() {
    var operacion = $('#operacion_respuesta').val();
    if (operacion != "") {
        if (operacion == "consultar_respuesta") {
            $('#modal-respuesta .close').click();
        }
        else if (operacion == "editar_respuesta") {
            updateResponse();
        }
        else if (operacion == "nueva_respuesta") {
            addResponse();
        }
    }
}

function nuevaRespuesta() {
    $('#title-operacion-respuesta').html("Nueva");
    cleanForm(formulario_respuesta);
    $('#operacion_respuesta').val("nueva_respuesta");
    enableForm(formulario_respuesta);
    $('#orden_respuesta').prop("disabled", true);

    $('#modal-respuesta').modal();
}
