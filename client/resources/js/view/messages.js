var urlServerPage = urlServer.concat("messages.php");

function init() {
    loadMessages();
    $('#btn_load_more').click(function() {
        loadMessages();
    });
}

function loadMessages() {
    var ultimo_mensaje = null;
    if ($('#table_messages > tbody > tr').length > 0) {
        ultimo_mensaje = $('#table_messages > tbody > tr:last-child').find('.message_id').html();
    }
    var usuario = getUserData();
    var params = {"operacion": "cargar_mensajes", "usuario": JSON.stringify(usuario), "ultimo_mensaje": ultimo_mensaje};
    //console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log("EXITO TRUE: "+JSON.stringify(respuesta));
                for (i=0; i<respuesta.mensajes.length; i++) {
                    var mensaje = respuesta.mensajes[i]
                    loadMessage(mensaje);
                    if (mensaje.id_mensaje <= respuesta.primer_mensaje) {
                        $('#btn_load_more').hide();
                    }
                }
                checkParameters();
            }
            else {
                console.log("EXITO FALSE: "+JSON.stringify(respuesta));
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}

function loadMessage(message) {
    if ($('#table_messages .'+message.id_mensaje).length <= 0) {
        var row =
            '<tr class="msg-display clickable '+((message.leido == $NO)?'mensaje-no-leido ':'')+message.id_mensaje+'">' +
                '<td class="wd-xxs">' +
                    '<text class="message_id hidden">'+message.id_mensaje+'</text>' +
                    '<text class="message_url hidden">'+((message.url_apertura!=null)?urlClient.concat(message.url_apertura):'')+'</text>' +
                    '<text class="message_id_consultation hidden">'+((message.id_consulta!=null)?message.id_consulta:'')+'</text>' +
                    '<text class="message_consultation_condition hidden">'+((message._consulta_condicion!=null)?message._consulta_condicion:'')+'</text>' +
                    '<img class="rounded-circle thumb32 message_user_from_image" src="'+message.usuario_imagen+'" alt="user" title="'+message.usuario_imagen+'">' +
                '</td>' +
                '<td class="text-ellipsis wd-sm">' +
                    '<text class="message_user_from_name">'+message.usuario_nombre_completo+'</text>' +
                    '<text class="hidden message_user_from_email">'+message.id_usuario_remitente+'</text>' +
                '</td>' +
                '<td class="text-ellipsis">' +
                    '<text class="message_title">'+message.titulo+'</text>' +
                    '<text> - </text>' +
                    '<text class="message_text">'+message.texto+'</text>' +
                '</td>' +
                '<td class="text-right wd-sm message_date">' +
                    '<text>'+message.fecha_format+'</text>' +
                '</td>' +
            '</tr>'
        if ($('#table_messages > tbody > tr').length > 0) {
            $('#table_messages > tbody:last-child').append(row);
        }
        else {
            $('#table_messages > tbody').html(row);
        }
        $('#table_messages .msg-display.'+message.id_mensaje).on('click', function(e){
            openMessage($(this));
        });
    }
}

function openMessage(row) {
    // Ignores drodown click to avoid opening modal at the same time
    // if( $(row).is('.dropdown') ||
    //     $(row).parents('.dropdown').length > 0  ) {
    //     return;
    // }

    // Open modal
    $('#message_user_from_image').attr("src", $(row).find('.message_user_from_image').attr("src"));
    $('#message_user_from_name').html($(row).find('.message_user_from_name').html());
    // $('#message_user_from_email').html($(row).find('.message_user_from_email').html());
    $('#message_date').html($(row).find('.message_date').html());
    $('#message_title').html($(row).find('.message_title').html());
    $('#message_text').html($(row).find('.message_text').html());
    $('#message_id_consultation').val($(row).find('.message_id_consultation').html());
    var url = $(row).find('.message_url').html();
    var condition = $(row).find('.message_consultation_condition').html();
    $('.modal-message').find('.modal-footer').hide();
    if (url != '') {
        if (condition != $CONSULTA_CONDICION_SIN_CONTESTAR) {
            $('#btn-aceptar-consulta').hide();
            $('#btn-rechazar-consulta').hide();
        }
        else {
            $('#btn-aceptar-consulta').click(function() {
                document.location.href = url+'&condition=accepted';
            });
            $('#btn-rechazar-consulta').click(function() {
                rejectConsultation();
            });
        }
        $('#btn_message_details').click(function() {
            document.location.href = url;
        });
        $('.modal-message').find('.modal-footer').show();
    }
    $('.modal-message').modal();

    if ($(row).hasClass('mensaje-no-leido')) {
        var usuario = getUserData();
        var id_mensaje = $(row).find('.message_id').html();
        var params = {"operacion": "marcar_mensaje_leido", "usuario": JSON.stringify(usuario), "id_mensaje": id_mensaje};
        //console.log("DATA TO SEND: "+JSON.stringify(params));
        $.ajax({
            type: "POST",
            url: urlServerPage,
            data: params,
            success: function(respuesta) {
                if (respuesta.exito == true) {
                    $(row).removeClass('mensaje-no-leido');
                    // Actualizar los tooltips que muestran la cantidad de mensajes sin leer
                    $('.messages_total').html(respuesta.total_mensajes);
                    $('.messages_total').show();
                    if (respuesta.total_mensajes <= 0) {
                        $('.messages_total').hide();
                    }
                }
                else {
                    console.log("EXITO FALSE: "+JSON.stringify(respuesta));
                }
            },
            error: function(respuesta) {
                console.log("ERROR: "+JSON.stringify(respuesta));
            }
        });
    }
}

function rejectConsultation() {
    var id_consulta = $('#message_id_consultation').val();
    if (id_consulta != '') {
        var usuario = getUserData();
        var params = {"operacion": "rechazar_consulta", "usuario": JSON.stringify(usuario), "id_consulta": id_consulta};
        //console.log("DATA TO SEND: "+JSON.stringify(params));
        $.ajax({
            type: "POST",
            url: urlServerPage,
            data: params,
            success: function(respuesta) {
                if (respuesta.exito == true) {
                    // console.log("EXITO TRUE: "+JSON.stringify(respuesta));
                    swal({
                        title: 'Consulta Rechazada',
                        text: '',
                        type: 'success',
                        confirmButtonText: 'Ok',
                        closeOnConfirm: false
                    },
                    function () {
                        var usuario = getUserData();
                        document.location.href = urlClient.concat(usuario.directorio_vistas+"index.html");
                    });
                }
                else {
                    console.log("EXITO FALSE: "+JSON.stringify(respuesta));
                }
            },
            error: function(respuesta) {
                console.log("ERROR: "+JSON.stringify(respuesta));
            }
        });
    }
}

function checkParameters() {
    var id_message = getUrlParameter('id_message');
    if (id_message != '') {
        if ($('#table_messages .msg-display.'+id_message).length > 0) {
            //console.log("CHECK PARAMETERS: "+id_message);
            $('#table_messages .msg-display.'+id_message).click();
        }
    }
}