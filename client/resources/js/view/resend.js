var urlServerPage = urlServer.concat("resend.php");

(function() {
    'use strict';
    $(userResend);
})();

/**************************************************************************
 * VALIDACION DE FORMULARIO DE REENVIO DE EMAIL DE VERIFICACION - INICIO
 **************************************************************************/
function userResend() {

    if (!$.fn.validate) return;

    var $form = $('#user-resend');
    $form.validate({
        errorPlacement: errorPlacementInput,
        // Form rules
        rules: {
            username: {
                required: true,
                email: true,
                rangelength: [6, 50]
            }
        },
        submitHandler: function() {
            resendEmail();
        }
    });
}

function resendEmail() {
    var email = $('#username').val();
    var params = {"operacion": "reenviar_correo", "email": email};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log("EXITO TRUE: "+JSON.stringify(respuesta));
                swal({
                    title: 'El correo ha sido reenviado',
                    text: 'Te hemos enviado un correo con un nuevo enlace de verificacion',
                    type: 'success',
                    confirmButtonText: 'Ok',
                    closeOnConfirm: true
                },
                function () {
                    document.location.href = urlClient.concat("login.html");
                });
            }
            else {
                // console.log("EXITO FALSE: "+JSON.stringify(respuesta));
                swal(respuesta.mensaje, '', 'error');
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}
/**************************************************************************
 * VALIDACION DE FORMULARIO DE REENVIO DE EMAIL DE VERIFICACION - FIN
 **************************************************************************/