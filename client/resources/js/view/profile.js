var urlServerPage = urlServer.concat("profile.php");

/**************************************************************************
 * INICIALIZACION DE VALIDACIONES - INICIO
 **************************************************************************/
function init() {
    $(profileBasicData);
    $(profileChangePassword);
}
/**************************************************************************
 * INICIALIZACION DE VALIDACIONES - FIN
 **************************************************************************/

/**************************************************************************
 * VALIDACION DE FORMULARIO DE DATOS BASICOS - INICIO
 **************************************************************************/
function profileBasicData() {
    if (!$.fn.validate) return;

    var $form = $('#profile-basic-data');
    $form.validate({
        errorPlacement: errorPlacementInput,
        // Form rules
        rules: {
            user_email: {
                required: true,
                email: true
            },
            user_firstname: {
                required: true,
                rangelength: [2, 50]
            },
            user_lastname: {
                required: true,
                rangelength: [2, 50]
            },
            user_phone: {
                required: false,
                digits: true,
                rangelength: [7, 20]
            }
        },
        submitHandler: function() {
            updateBasicData();
        }
    });
}

function updateBasicData() {
    var usuario = {};
    usuario.id_usuario = $('#user_email').val();
    usuario.nombres = $('#user_firstname').val();
    usuario.apellidos = $('#user_lastname').val();
    usuario.telefono = $('#user_phone').val();
    var params = {"operacion": "actualizar_datos_basicos", "usuario": JSON.stringify(usuario)};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log("EXITO TRUE: "+JSON.stringify(respuesta));
                var usuario = getUserData();
                if (usuario != null) {
                    usuario.nombres = respuesta.usuario.nombres;
                    usuario.apellidos = respuesta.usuario.apellidos;
                    usuario.nombre_completo = respuesta.usuario.nombre_completo;
                    usuario.telefono = respuesta.usuario.telefono;
                    setUserData(usuario);
                }
                swal({
                    title: 'Sus datos basicos han sido actualizados',
                    text: '',
                    type: 'success',
                    confirmButtonText: 'Ok',
                    closeOnConfirm: true
                },
                function () {
                    document.location.reload();
                });
            }
            else {
                // console.log("EXITO FALSE: "+JSON.stringify(respuesta));
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}
/**************************************************************************
 * VALIDACION DE FORMULARIO DE DATOS BASICOS - FIN
 **************************************************************************/

/**************************************************************************
 * VALIDACION DE FORMULARIO DE CAMBIO DE CLAVE - INICIO
 **************************************************************************/
function profileChangePassword() {
    if (!$.fn.validate) return;

    var $form = $('#profile-change-password');
    $form.validate({
        errorPlacement: errorPlacementInput,
        // Form rules
        rules: {
            user_password_current: {
                required: true,
                rangelength: [6, 20]

            },
            user_password_new: {
                required: true,
                rangelength: [6, 20]

            },
            user_password_new_confirm: {
                required: true,
                rangelength: [6, 20],
                equalTo: '#user_password_new'
            }
        },
        submitHandler: function() {
            changePassword();
        }
    });
}

function changePassword() {
    var usuario = {};
    usuario.id_usuario = $('#user_email').val();
    usuario.clave = $('#user_password_current').val();
    usuario.clave_nueva = $('#user_password_new').val();
    var params = {"operacion": "cambiar_clave", "usuario": JSON.stringify(usuario)};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                //console.log("EXITO TRUE: "+JSON.stringify(respuesta));
                $('#user_password_current').val("");
                $('#user_password_new').val("");
                $('#user_password_new_confirm').val("");
                swal('Su clave ha sido cambiada', '', 'success');
            }
            else {
                // console.log("EXITO FALSE: "+JSON.stringify(respuesta));
                swal(respuesta.mensaje, '', 'error');
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}
/**************************************************************************
 * VALIDACION DE FORMULARIO DE CAMBIO DE CLAVE - FIN
 **************************************************************************/

$(document).on('click', '#btn_remover_imagen', function(e) {
    e.preventDefault();
    var usuario = {};
    usuario.id_usuario = $('#user_email').val();
    var params = {"operacion": "remover_imagen", "usuario": JSON.stringify(usuario)};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log("EXITO TRUE: "+JSON.stringify(respuesta));
                var usuario = getUserData();
                if (usuario != null) {
                    usuario.imagen = respuesta.usuario.imagen;
                    setUserData(usuario);
                }
                swal({
                    title: 'Su imagen ha sido removida',
                    text: '',
                    type: 'success',
                    confirmButtonText: 'Ok',
                    closeOnConfirm: true
                },
                function () {
                    document.location.reload();
                });
            }
            else {
                console.log("EXITO FALSE: "+JSON.stringify(respuesta));
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
});

$(document).on('click', '#btn_cambiar_imagen', function(e) {
    e.preventDefault();
    var params = new FormData();
    params.append('operacion', 'cambiar_imagen');
    params.append('id_usuario', $('#user_email').val());
    params.append("imagen", $("#user_image_new")[0]["files"][0]);
    // console.log(params);
    $.ajax({
        type: "POST",
        cache: false,
        url: urlServerPage,
        data: params,
        contentType: false,
        processData: false,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log("EXITO TRUE: "+JSON.stringify(respuesta));
                var usuario = getUserData();
                if (usuario != null) {
                    usuario.imagen = respuesta.usuario.imagen;
                    setUserData(usuario);
                }
                swal({
                        title: 'Su imagen ha sido cambiada',
                        text: '',
                        type: 'success',
                        confirmButtonText: 'Ok',
                        closeOnConfirm: true
                    },
                    function () {
                        document.location.reload();
                    });
            }
            else {
                console.log("EXITO FALSE: "+JSON.stringify(respuesta));
                swal(respuesta.mensaje, '', 'error');
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
});
