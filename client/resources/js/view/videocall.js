var urlVideoCallPage = urlServer.concat("videocall.php");
var session = null;
var connectionCount = 0;

$(function() {
    checkParameters();
});

function checkParameters() {
    var id_consultation = getUrlParameter('id_consultation');
    if (id_consultation != '') {
        checkVideoCall(id_consultation);
    }
    else {
        document.location.href = urlClient.concat(usuario.directorio_vistas + "index.html");
    }
}

function checkVideoCall(id_consultation) {
    var usuario = getUserData();
    var params = {"operacion": "consultar", "usuario": JSON.stringify(usuario), "id_consulta": id_consultation};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlVideoCallPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log("EXITO TRUE: "+JSON.stringify(respuesta));
                $('#id_consulta').val(id_consultation);
                var apiKey = respuesta.data.apiKey;
                var sessionId = respuesta.data.sessionId;
                var token = respuesta.data.token;
                initializeSession(apiKey, sessionId, token);
                var seconds = parseInt(respuesta.data.segundos);
                initializeTimer(seconds);
            }
            else {
                // console.log("EXITO FALSE: "+JSON.stringify(respuesta));
                swal(respuesta.mensaje, '', 'error');
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}

// Handling all of our errors here by alerting them
function handleError(error) {
    if (error) {
        swal(error.message, '', 'error');
    }
}

function initializeSession(apiKey, sessionId, token) {
    session = OT.initSession(apiKey, sessionId);

    // Subscribe to a newly created stream
    session.on('streamCreated', function(event) {
        session.subscribe(event.stream, 'subscriber'/*ID del elemento donde aparecera el video del subscriber*/, {
            insertMode: 'append',
            width: '100%',
            height: '100%'
        }, handleError);
    });

    // Create a publisher
    var publisher = OT.initPublisher('publisher'/*ID del elemento donde aparecera el video del publisher*/, {
        insertMode: 'append',
        width: '100%',
        height: '100%'
    }, handleError);

    // Know when a new partner is connected
    session.on("connectionCreated", function(event) {
        connectionCount++;
        if (connectionCount > 1) {
            startTimer();
        }
    });

    // Connect to the session
    session.connect(token, function(error) {
        // If the connection is successful, publish to the session
        if (error) {
            handleError(error);
        }
        else {
            session.publish(publisher, handleError);
        }
    });
}

function initializeTimer(seconds) {
    $('#max_time').val(seconds);
    $('#time_remaining').val(seconds + 1);
}

function startTimer() {
    var id_interval_timer = setInterval(updateTimer, 1000);
    $('#id_interval_timer').val(id_interval_timer);
    if (usuario.directorio_vistas.indexOf('customer') >= 0) {
        parent.saveStartVideoCall();
    }
}

function updateTimer() {
    var time = parseInt($('#time_remaining').val()) -1;
    $('#time_remaining').val(time);
    if (time > 0) {
        var minutes = parseInt(time/60)
        var seconds = parseInt(time%60);
        var time_format = ((minutes > 9)?minutes:'0'+minutes)+':'+((seconds>9)?seconds:'0'+seconds);
        $('#timer').html(time_format);
    }
    else {
        closeVideoCall();
    }
}

function closeVideoCall() {
    $('#timer').html('00:00');
    // Limpiar los intervalos
    clearIntervals();
    saveEndVideoCall();
}

function clearIntervals() {
    // Limpiar el intervalo que lleva el conteo regresivo
    var id_interval_timer = $('#id_interval_timer').val();
    clearInterval(id_interval_timer);
}

function saveEndVideoCall() {
    var usuario = getUserData();
    var id_consulta = $('#id_consulta').val();
    var params = {"operacion": "guardar", "usuario": JSON.stringify(usuario), "id_consulta": id_consulta};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlVideoCallPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log("EXITO TRUE: "+JSON.stringify(respuesta.data));
                if (session.currentState == "connected") {
                    session.disconnect();
                }
                parent.onCloseVideoCall();
            }
            else {
                // console.log("EXITO FALSE: "+JSON.stringify(respuesta));
                swal(respuesta.mensaje, '', 'error');
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}

$('#btn-finalizar-videollamada').click(function (e) {
    e.preventDefault();
    swal({
        title: 'Estas seguro?',
        text: 'Si finalizas la llamada no podras ingresar nuevamente!',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'No, me arrepenti',
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Si, finalizarla',
        closeOnConfirm: false
    },
    function () {
        closeVideoCall();
    });
});
