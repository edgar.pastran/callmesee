var urlServerPage = urlServer.concat("recover.php");

(function() {
    'use strict';
    $(userRecover);
})();

/**************************************************************************
 * VALIDACION DE FORMULARIO DE RECUPERACION DE CLAVE - INICIO
 **************************************************************************/
function userRecover() {

    if (!$.fn.validate) return;

    var $form = $('#user-recover');
    $form.validate({
        errorPlacement: errorPlacementInput,
        // Form rules
        rules: {
            username: {
                required: true,
                email: true,
                rangelength: [6, 50]
            }
        },
        submitHandler: function() {
            resetPassword();
        }
    });
}

function resetPassword() {
    var email = $('#username').val();
    var params = {"operacion": "resetear_clave", "email": email};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log("EXITO TRUE: "+JSON.stringify(respuesta));
                swal({
                    title: 'Su clave ha sido reseteada',
                    text: 'Te hemos enviado un correo con la informacion de tu nueva clave',
                    type: 'success',
                    confirmButtonText: 'Ok',
                    closeOnConfirm: true
                },
                function () {
                    document.location.href = urlClient.concat("login.html");
                });
            }
            else {
                // console.log("EXITO FALSE: "+JSON.stringify(respuesta));
                swal(respuesta.mensaje, '', 'error');
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}
/**************************************************************************
 * VALIDACION DE FORMULARIO DE RECUPERACION DE CLAVE - FIN
 **************************************************************************/