var urlServerPage = urlServer.concat("adviser/consultation.php");

$(function() {
    $('#videocall').hide();
    checkParameters();
});

function checkParameters() {
    var id_consultation = getUrlParameter('id_consultation');
    if (id_consultation != '') {
        $('#id_consulta').val(id_consultation);
        loadData();
        var condition = getUrlParameter('condition');
        if (condition == 'accepted') {
            acceptConsultation();
        }
    }
    else {
        var usuario = getUserData();
        document.location.href = urlClient.concat(usuario.directorio_vistas+"index.html");
    }
}

function loadData() {
    var usuario = getUserData();
    var params = {"operacion": "consultar", "usuario": JSON.stringify(usuario), "id_consulta": $('#id_consulta').val()};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log("EXITO TRUE: "+JSON.stringify(respuesta));
                if (respuesta.data != null) {
                    $('#consulta_data').val(JSON.stringify(respuesta.data));
                    showCustomerData(respuesta.data.cliente);
                    showConsultationData(respuesta.data.consulta);
                    showRating();
                }
                else {
                    var usuario = getUserData();
                    document.location.href = urlClient.concat(usuario.directorio_vistas+"index.html");
                }
            }
            else {
                // console.log("EXITO FALSE: "+JSON.stringify(respuesta));
                swal(respuesta.mensaje, '', 'error');
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}

function showCustomerData(customer) {
    var html =
        '<div class="cardbox">' +
            '<div class="cardbox-body text-center">' +
                '<div class="mb-3">' +
                    '<img class="wd-sm rounded-circle img-thumbnail" src="'+customer._imagen+'" alt="'+customer._nombres+' '+customer._apellidos+'">' +
                '</div>' +
                '<div class="h3">'+customer._nombres+' '+customer._apellidos+'</div>' +
                //'<p>'+customer._correo+'</p>' +
            '</div>' +
        '</div>';
    $('#container-cliente').html(html);
}

function showConsultationData(consultation) {
    showConsultationMotive(consultation);
    showConsultationQuestions(consultation.preguntas);
}

function showConsultationMotive(consultation) {
    var html =
        '<div class="cardbox">' +
            '<div class="cardbox-body text-center">' +
                '<div class="h3">Consulta de '+consultation._categoria_nombre+'</div>' +
            '</div>' +
            '<div class="cardbox-body bb">' +
                '<p class="text-left">'+consultation.motivo+'</p>' +
            '</div>';
    if (consultation.condicion == $CONSULTA_CONDICION_SIN_CONTESTAR) {
        html +=
            '<div id="container-botones" class="cardbox-body">' +
                '<button class="btn btn-success btn-block" id="btn-aceptar-consulta" type="button">' +
                    '<em class="ion-checkmark-round mr-2"></em>Iniciar Video Llamada' +
                '</button>' +
                '<button class="btn btn-danger btn-block" id="btn-rechazar-consulta" type="button">' +
                    '<em class="ion-close-round mr-2"></em>Rechazar Consulta' +
                '</button>' +
            '</div>';
    }
    html +=
        '</div>';
    $('#container-motivo').html(html);

    if (consultation.condicion == $CONSULTA_CONDICION_SIN_CONTESTAR) {
        $('#btn-aceptar-consulta').click(function () {
            acceptConsultation();
        });

        $('#btn-rechazar-consulta').click(function () {
            rejectConsultation();
        });
    }
}

function showConsultationQuestions(questions) {
    var html =
        '<div class="cardbox">' +
            '<div class="cardbox-body text-center">' +
                '<div class="h3">Preguntas</div>' +
            '</div>';
    for (q=0; q<questions.length; q++) {
        var question = questions[q];
        html +=
            '<div class="cardbox-body bb">' +
                '<p><strong>'+(q+1)+'/'+questions.length+'. '+question.enunciado+'</strong></p>';
        for (r=0; r<question.respuestas.length; r++) {
            var respuesta = question.respuestas[r];
            html +=
                '<div class="row mb-3">'+
                    '<div class="col-1"> '+
                        '<em class="ion-checkmark-round mx-3"></em>' +
                    '</div>' +
                    '<div class="col-11">' +
                        '<span>'+respuesta.enunciado+'</span>' +
                    '</div>' +
                '</div>';
        }
        html +=
            '</div>';
    }
    html +=
        '</div>';
    $('#container-preguntas').html(html);
}

function showRating() {
    $('.awesomeRating').awesomeRating({
        values: [ 1, 2, 3, 4, 5 ], // custom rating values
        valueInitial: null, // initial value
        targetSelector: "input.awesomeRatingValue", // target selector
    });
}

function rejectConsultation() {
    var id_consulta = $('#id_consulta').val();
    if (id_consulta != '') {
        var usuario = getUserData();
        var params = {"operacion": "rechazar_consulta", "usuario": JSON.stringify(usuario), "id_consulta": id_consulta};
        //console.log("DATA TO SEND: "+JSON.stringify(params));
        $.ajax({
            type: "POST",
            url: urlServerPage,
            data: params,
            success: function(respuesta) {
                if (respuesta.exito == true) {
                    // console.log("EXITO TRUE: "+JSON.stringify(respuesta));
                    swal({
                            title: 'Consulta Rechazada',
                            text: '',
                            type: 'success',
                            confirmButtonText: 'Ok',
                            closeOnConfirm: false
                        },
                        function () {
                            var usuario = getUserData();
                            document.location.href = urlClient.concat(usuario.directorio_vistas+"index.html");
                        });
                }
                else {
                    console.log("EXITO FALSE: "+JSON.stringify(respuesta));
                }
            },
            error: function(respuesta) {
                console.log("ERROR: "+JSON.stringify(respuesta));
            }
        });
    }
}

function acceptConsultation() {
    var id_consulta = $('#id_consulta').val();
    if (id_consulta != '') {
        var usuario = getUserData();
        var params = {"operacion": "aceptar_consulta", "usuario": JSON.stringify(usuario), "id_consulta": id_consulta};
        //console.log("DATA TO SEND: "+JSON.stringify(params));
        $.ajax({
            type: "POST",
            url: urlServerPage,
            data: params,
            success: function(respuesta) {
                if (respuesta.exito == true) {
                    // console.log("EXITO TRUE: "+JSON.stringify(respuesta));
                    $('#container-botones').hide();
                    startVideoCall();
                }
                else {
                    console.log("EXITO FALSE: "+JSON.stringify(respuesta));
                }
            },
            error: function(respuesta) {
                console.log("ERROR: "+JSON.stringify(respuesta));
            }
        });
    }
}

function startVideoCall() {
    var id_consulta = $('#id_consulta').val();
    // window.open(urlClient.concat(usuario.directorio_vistas+"videocall.html?id_consultation="+id_consulta));
    var url = urlClient.concat(usuario.directorio_vistas+"videocall.html?id_consultation="+id_consulta);
    $('.layout-container').hide();
    $('#videocall').show();
    $('#videocall').attr('src', url);
}

function onCloseVideoCall() {
    $('#videocall').hide();
    $('.layout-container').show();
    validateForm();
    $('#modal-comentarios-llamada').modal();
}

function validateForm() {
    if (!$.fn.validate) return;

    var form = $('#formulario-comentarios-llamada');
    form.validate({
        errorPlacement: errorPlacementInput,
        // Form rules
        rules: {
            comentarios: {
                required: true,
                rangelength: [1, 140]
            },
            calificacion: {
                required: true,
                number: true,
                range: [1, 5]
            }
        },
        submitHandler: function() {
            saveCommentsVideoCall();
        }
    });
    // Preguntas
    $('.question').each(function () {
        var id_question = $(this).find('.id_question').val();
        $('.answer-question-'+id_question).each(function () {
            $(this).rules("add", {
                required: function (element) {
                    return ($('.answer-question-'+id_question).filter(':checked').length == 0);
                },
                messages: {
                    required: 'Por favor seleccione al menos una opcion'
                }
            });
        });
    });
    // Asesores
    $('.adviser').each(function () {
        $(this).rules("add", {
            required: function (element) {
                return ($('.adviser').filter(':checked').length == 0);
            },
            messages: {
                required: 'Por favor seleccione al menos una opcion'
            }
        });
    });
}

function saveCommentsVideoCall () {
    var consulta = {};
    consulta.id_consulta = $('#id_consulta').val();
    consulta.comentario_asesor = $('#comentarios').val();
    consulta.calificacion_asesor = $('#calificacion').val();
    var params = {"operacion": "guardar_comentarios_llamada", "consulta": consulta};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log("EXITO TRUE: "+JSON.stringify(respuesta.data));
                swal({
                    title: 'Hemos registrado tus comentarios, gracias!!!',
                    text: '',
                    type: 'success',
                    confirmButtonText: 'Ok',
                    closeOnConfirm: true
                },
                function () {
                    var usuario = getUserData();
                    document.location.href = urlClient.concat(usuario.directorio_vistas+"index.html");
                });
            }
            else {
                // console.log("EXITO FALSE: "+JSON.stringify(respuesta));
                swal(respuesta.mensaje, '', 'error');
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}