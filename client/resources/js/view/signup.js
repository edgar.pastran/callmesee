var urlServerPage = urlServer.concat("signup.php");

(function() {
    'use strict';
    $(userSignup);
})();

/**************************************************************************
 * VALIDACION DE FORMULARIO DE REGISTRO - INICIO
 **************************************************************************/
function userSignup() {

    if (!$.fn.validate) return;

    var $form = $('#user-signup');
    $form.validate({
        errorPlacement: errorPlacementInput,
        // Form rules
        rules: {
            username: {
                required: true,
                email: true,
                rangelength: [6, 50]
            },
            firstname: {
                required: true,
                rangelength: [2, 50]
            },
            lastname: {
                required: true,
                rangelength: [2, 50]
            },
            password: {
                required: true,
                rangelength: [6, 20]

            },
            password_confirm: {
                required: true,
                rangelength: [6, 20],
                equalTo: '#password'
            }
        },
        submitHandler: function() {
            signup();
        }
    });
}

function signup() {
    var usuario = {};
    usuario.id_usuario = $('#username').val();
    usuario.clave = $('#password').val();
    usuario.correo = $('#username').val();
    usuario.nombres = $('#firstname').val();
    usuario.apellidos = $('#lastname').val();
    var params = {"operacion": "crear_usuario", "usuario": JSON.stringify(usuario)};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log("EXITO TRUE: "+JSON.stringify(respuesta));
                swal({
                    title: 'Usuario creado y por verificar',
                    text: 'Te hemos enviado un correo con un enlace de verificacion',
                    type: 'success',
                    confirmButtonText: 'Ok',
                    closeOnConfirm: false
                },
                function () {
                    document.location.href = urlClient.concat("login.html");
                });
            }
            else {
                // console.log("EXITO FALSE: "+JSON.stringify(respuesta));
                swal(respuesta.mensaje, '', 'error');
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}
/**************************************************************************
 * VALIDACION DE FORMULARIO DE REGISTRO - FIN
 **************************************************************************/
