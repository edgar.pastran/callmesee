var urlServerPage = urlServer.concat("customer/consultation.php");
var formulario = $('#formulario-consulta');

$(function() {
    $('#videocall').hide();
    checkParameters();
});

function checkParameters() {
    var id_purchase = getUrlParameter('id_purchase');
    if (id_purchase != '') {
        $('#id_compra').val(id_purchase);
        loadData();
    }
    else {
        var usuario = getUserData();
        document.location.href = urlClient.concat(usuario.directorio_vistas+"index.html");
    }
}

function loadData() {
    var usuario = getUserData();
    var params = {"operacion": "consultar", "usuario": JSON.stringify(usuario), "id_compra": $('#id_compra').val()};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                //console.log("EXITO TRUE: "+JSON.stringify(respuesta.data));
                if (respuesta.data != null) {
                    $('#compra_data').val(JSON.stringify(respuesta.data));
                    showQuestions(respuesta.data.preguntas);
                    showAdvisers(respuesta.data.asesores);
                    validateForm();
                    createWizard();
                    showRating();
                }
                else {
                    var usuario = getUserData();
                    document.location.href = urlClient.concat(usuario.directorio_vistas+"index.html");
                }
            }
            else {
                // console.log("EXITO FALSE: "+JSON.stringify(respuesta));
                swal({
                    title: respuesta.mensaje,
                    text: '',
                    type: 'error',
                    confirmButtonText: 'Ok',
                    closeOnConfirm: true
                },
                function () {
                    var usuario = getUserData();
                    document.location.href = urlClient.concat(usuario.directorio_vistas+"index.html");
                });
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}

function showQuestions(questions) {
    var total_questions = questions.length;
    for (i=0; i<total_questions; i++) {
        var question = questions[i];
        var number = (i+1)+"/"+total_questions;
        showQuestion(question, number);
    }
}

function showQuestion(question, number) {
    var html =
        '<div class="cardbox-body bb question">' +
            '<input class="id_question" type="hidden" value="'+question.id_pregunta+'">' +
            '<p><strong>'+number+'. '+'<label class="title_question">'+question.enunciado+'</label></strong></p>';
    for (j=0; j<question.respuestas.length; j++) {
        var respuesta = question.respuestas[j];
        html +=
            '<div class="form-group">' +
                '<label class="custom-control custom-checkbox">' +
                    '<input class="custom-control-input answer-question-'+question.id_pregunta+'" type="checkbox" id="answer-'+respuesta.id_respuesta+'" name="answer-'+respuesta.id_respuesta+'">' +
                    '<span class="custom-control-indicator"></span>' +
                    '<span class="custom-control-description">'+respuesta.enunciado+'</span>' +
                '</label>' +
            '</div>';
    }
    html +=
        '</div>';
    $('#questions').append(html);
}

function showAdvisers(advisers) {
    if (advisers.length > 0) {
        for (j = 0; j < advisers.length; j++) {
            var adviser = advisers[j];
            showAdviser(adviser);
        }
    }
    else {
        var html =
            '<p class="lead text-danger text-bold pl-3">En este momento no hay algun operador disponible</p>' +
            '<p class="lead text-danger text-bold pl-3">Por favor intenta dentro de un momento nuevamente.</p>';
        $('#advisers').html(html);
    }
}

function showAdviser(adviser) {
    var html =
        '<div class="col-md-3 col-lg-3" title="'+adviser._nombres+' '+adviser._apellidos+'">' +
            '<div id="'+adviser.id_asesor+'" class="custom-button">' +
                '<img src="'+adviser._imagen+'">' +
                '<p>'+adviser._nombres+' '+adviser._apellidos+'</p>' +
                '<label class="custom-control custom-checkbox">' +
                    '<input class="custom-control-input adviser" id="adviser-'+adviser.id_asesor+'" name="adviser-'+adviser.id_asesor+'" type="checkbox">' +
                    '<span class="custom-control-indicator"></span>' +
                '</label>' +
            '</div>' +
        '</div>';
    $('#advisers').append(html);
}

function showRating() {
    $('.awesomeRating').awesomeRating({
        values: [ 1, 2, 3, 4, 5 ], // custom rating values
        valueInitial: null, // initial value
        targetSelector: "#calificacion", // target selector
    });
}

function validateForm() {
    if (!$.fn.validate) return;

    var form = formulario;
    form.validate({
        errorPlacement: errorPlacementInput,
        // Form rules
        rules: {
            motivo: {
                required: true,
                rangelength: [1, 140]
            },
            comentarios: {
                required: true,
                rangelength: [1, 140]
            },
            calificacion: {
                required: true,
                number: true,
                range: [1, 5]
            }
        },
        submitHandler: function() {
            saveCommentsVideoCall();
        }
    });
    // Preguntas
    $('.question').each(function () {
        var id_question = $(this).find('.id_question').val();
        $('.answer-question-'+id_question).each(function () {
            $(this).rules("add", {
                required: function (element) {
                    return ($('.answer-question-'+id_question).filter(':checked').length == 0);
                },
                messages: {
                    required: 'Por favor seleccione al menos una opcion'
                }
            });
        });
    });
    // Asesores
    $('.adviser').each(function () {
        $(this).rules("add", {
            required: function (element) {
                return ($('.adviser').filter(':checked').length == 0);
            },
            messages: {
                required: 'Por favor seleccione al menos una opcion'
            }
        });
    });
}

function createWizard() {
    if (!$.fn.steps) return;

    var form = formulario;
    form.children('div').steps({
        headerTag: 'h4',
        bodyTag: 'fieldset',
        transitionEffect: 'slideLeft',
        stepsOrientation: 'vertical',
        labels: {
            current: "Paso Actual",
            pagination: "Paginacion",
            finish: "Finalizar",
            next: "Proximo",
            previous: "Anterior",
            loading: "Cargando ..."
        },
        onStepChanging: function(event, currentIndex, newIndex) {
            form.validate().settings.ignore = ':disabled,:hidden';
            return form.valid();
        },
        onStepChanged: function(event, currentIndex) {
            if (currentIndex == 2) {
                if ($('#advisers').find('.custom-button').length <= 0) {
                    // Ocultar los botones Anterior y Proximo
                    $('#steps-uid-0 > div.actions.clearfix > ul > li').hide();
                }
            }
            else if (currentIndex == 3) {
                contactAdviser();
            }
        },
        onFinishing: function(/*event, currentIndex*/) {
            form.validate().settings.ignore = ':disabled';
            return form.valid();
        },
        onFinished: function(/*event, currentIndex*/) {
            // Submit form
            $(this).submit();
        }
    });
}

function contactAdviser() {
    // Ocultar los botones Anterior y Proximo
    $('#steps-uid-0 > div.actions.clearfix > ul > li').hide();
    // Ocultar el DIV que muestra el tiempo de espera por la respuesta del asesor
    $('#container-espera-respuesta-asesor').hide();

    var consulta = {};
    consulta._id_compra = $('#id_compra').val();
    consulta.motivo = $('#motivo').val();
    consulta.id_asesor = $('#advisers').find('.selected').attr('id');
    consulta.preguntas = [];
    $('.question').each(function () {
        var pregunta = {};
        var id_question = $(this).find('.id_question').val();
        var title_question = $(this).find('.title_question').html();
        pregunta.id_pregunta = id_question;
        pregunta.enunciado = title_question;
        pregunta.respuestas = [];
        $('.answer-question-'+id_question+':checked').each(function () {
            var id_response = $(this).attr('id').split('-')[1];
            var title_response = $(this).siblings('.custom-control-description').html();
            var respuesta = {};
            respuesta.id_respuesta = id_response;
            respuesta.enunciado = title_response;
            pregunta.respuestas.push(respuesta);
        });
        consulta.preguntas.push(pregunta);
    });
    var usuario = getUserData();
    var params = {"operacion": "guardar", "usuario": JSON.stringify(usuario), "consulta": consulta};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log("EXITO TRUE: "+JSON.stringify(respuesta.data));
                // Se guarda el ID de la consulta creada
                $('#id_consulta').val(respuesta.data.id_consulta);
                // Se asigna el tiempo maximo de espera
                $('#time').val(maxTimeWaitForAdviser + 1);
                // Se muestra el div que contiene el cronometro
                $('#container-espera-respuesta-asesor').show();
                // Se inicia el conteo regresivo de la espera
                var id_interval_time = setInterval(manageWaitForAdviser, 1000);
                // Se guarda el ID que identifica el hilo del intervalo de conteo regresivo
                $('#id_interval_time').val(id_interval_time);
                // Se inicia el chequeo en el servidor de la respuesta del asesor
                var id_interval_adviser_response = setInterval(checkAdviserResponse, timeoutCheckAdviserResponse);
                // Se guarda el ID que identifica el hilo del intervalo de chequeo en el servidor
                $('#id_interval_adviser_response').val(id_interval_adviser_response);
            }
            else {
                console.log("EXITO FALSE: "+JSON.stringify(respuesta));
                swal(respuesta.mensaje, '', 'error');
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}

function manageWaitForAdviser() {
    var time = parseInt($('#time').val()) -1;
    $('#time').val(time);
    if (time > 0) {
        var minutes = parseInt(time/60)
        var seconds = parseInt(time%60);
        var time_format = ((minutes > 9)?minutes:'0'+minutes)+':'+((seconds>9)?seconds:'0'+seconds);
        $('#tiempo-espera-videollamada').html(time_format);
    }
    else {
        $('#tiempo-espera-videollamada').html('00:00');
        // Limpiar los intervalos
        clearIntervals();
        var id_consulta = $('#id_consulta').val();
        var params = {"operacion": "expirar_consulta", "id_consulta": id_consulta};
        // console.log("DATA TO SEND: "+JSON.stringify(params));
        $.ajax({
            type: "POST",
            url: urlServerPage,
            data: params,
            success: function(respuesta) {
                if (respuesta.exito == true) {
                    // console.log("EXITO TRUE: "+JSON.stringify(respuesta.data));
                    swal({
                        title: 'Operador no respondió',
                        text: 'Al parecer el operador actualmente no se encuentra disponible para atender tu consulta. Prueba con otro operador',
                        type: 'warning',
                        confirmButtonText: 'Ok',
                        closeOnConfirm: true
                    },
                    function () {
                        changeAdviser();
                    });
                }
                else {
                    // console.log("EXITO FALSE: "+JSON.stringify(respuesta));
                    swal(respuesta.mensaje, '', 'error');
                }
            },
            error: function(respuesta) {
                console.log("ERROR: "+JSON.stringify(respuesta));
            }
        });
    }
}

function clearIntervals() {
    // Limpiar el intervalo que lleva el conteo regresivo
    var id_interval_time = $('#id_interval_time').val();
    clearInterval(id_interval_time);
    // Limpiar el intervalo que realiza la consulta al servidor
    var id_interval_adviser_response = $('#id_interval_adviser_response').val();
    clearInterval(id_interval_adviser_response);
}

function checkAdviserResponse() {
    var id_consulta = $('#id_consulta').val();
    var params = {"operacion": "consultar_respuesta_asesor", "id_consulta": id_consulta};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log("EXITO TRUE: "+JSON.stringify(respuesta.data));
                if (respuesta.data.condicion != $CONSULTA_CONDICION_SIN_CONTESTAR) {
                    // Limpiar los intervalos
                    clearIntervals();
                    // Ocultar el DIV que muestra el tiempo de espera por la respuesta del asesor
                    $('#container-espera-respuesta-asesor').hide();
                    if (respuesta.data.condicion == $CONSULTA_CONDICION_ACEPTADA) {
                        swal({
                            title: 'Operador disponible',
                            text: 'El operador ya confirmó su disponibilidad para atender tu consulta y te esta esperando en una video llamada',
                            type: 'success',
                            confirmButtonText: 'Iniciar Video LLamada',
                            closeOnConfirm: true
                        },
                        function () {
                            startVideoCall();
                        });
                    }
                    else if (respuesta.data.condicion == $CONSULTA_CONDICION_RECHAZADA) {
                        swal({
                            title: 'Operador no disponible',
                            text: 'El operador actualmente no se encuentra disponible para atender tu consulta. Prueba con otro operador',
                            type: 'warning',
                            confirmButtonText: 'Ok',
                            closeOnConfirm: true
                        },
                        function () {
                            changeAdviser();
                        });
                    }
                }
            }
            else {
                // console.log("EXITO FALSE: "+JSON.stringify(respuesta));
                swal(respuesta.mensaje, '', 'error');
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}

function changeAdviser() {
    // Ir al paso previo (seleccion de operador)
    $('#steps-uid-0 > div.actions.clearfix > ul > li:nth-child(1) > a').click();
    // Deseleccionar el asesor
    var btn_asesor = $('#advisers').find('.selected');
    btn_asesor.removeClass('selected');
    btn_asesor.find('input:checkbox').prop('checked',false);
}

function startVideoCall() {
    var id_consulta = $('#id_consulta').val();

    // Ir al ultimo paso de comentarios
    $('#steps-uid-0 > div.actions.clearfix > ul > li:nth-child(2) > a').click();

    // Mostrar video llamada
    var url = urlClient.concat(usuario.directorio_vistas+"videocall.html?id_consultation="+id_consulta);
    $('.layout-container').hide();
    $('#videocall').show();
    $('#videocall').attr('src', url);
    // $('#videocall').reload();
    // $('#videocall').contentWindow.location.reload(true);
}

function saveStartVideoCall() {
    var id_compra = $('#id_compra').val();
    var id_consulta = $('#id_consulta').val();

    var params = {"operacion": "guardar_inicio_llamada", "id_compra": id_compra, "id_consulta": id_consulta};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log("EXITO TRUE: "+JSON.stringify(respuesta.data));
            }
            else {
                // console.log("EXITO FALSE: "+JSON.stringify(respuesta));
                swal(respuesta.mensaje, '', 'error');
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}

function onCloseVideoCall() {
    $('#videocall').hide();
    $('.layout-container').show();
}

function saveCommentsVideoCall () {
    var consulta = {};
    consulta.id_consulta = $('#id_consulta').val();
    consulta.comentario_cliente = $('#comentarios').val();
    consulta.calificacion_cliente = $('#calificacion').val();
    var params = {"operacion": "guardar_comentarios_llamada", "consulta": consulta};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log("EXITO TRUE: "+JSON.stringify(respuesta.data));
                swal({
                    title: 'Hemos registrado tus comentarios, gracias!!!',
                    text: '',
                    type: 'success',
                    confirmButtonText: 'Ok',
                    closeOnConfirm: true
                },
                function () {
                    var usuario = getUserData();
                    document.location.href = urlClient.concat(usuario.directorio_vistas+"index.html");
                });
            }
            else {
                // console.log("EXITO FALSE: "+JSON.stringify(respuesta));
                swal(respuesta.mensaje, '', 'error');
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}
