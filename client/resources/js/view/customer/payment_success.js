$(function() {
    swal({
        title: 'Pago Exitoso',
        text: 'Tu pago ha sido procesado',
        type: 'success',
        confirmButtonText: 'Ok',
        closeOnConfirm: false
    },
    function () {
        var usuario = getUserData();
        document.location.href = urlClient.concat(usuario.directorio_vistas+"index.html");
    });
});
