var urlServerPage = urlServer.concat("customer/index.php");

$(function() {
    loadData();
});

function loadData() {
    var usuario = getUserData();
    var params = {"operacion": "consultar", "usuario": JSON.stringify(usuario)};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log("EXITO TRUE: "+JSON.stringify(respuesta));
                if (respuesta.data.categorias.length > 0) {
                    $('#no-calls').hide();
                    showCategories(respuesta.data.categorias);
                    showCalls(respuesta.data.llamadas);
                    initIsotope();
                }
            }
            else {
                // console.log("EXITO FALSE: "+JSON.stringify(respuesta));
                swal(respuesta.mensaje, '', 'error');
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}

function showCategories(categories) {
    for (i=0; i<categories.length; i++) {
        var category = categories[i];
        showCategory(category);
    }
}

function showCategory(category) {
    var html =
        // '<div class="filter card b clickable" data-filter='+category.id_categoria+'>' +
        //     '<div class="card-body text-center bb text-info">' +
        //         '<h1 class="my-0 text-bold">' +
        //             '<span style="font-size: 1.0rem">'+category.nombre+'</span>' +
        //         '</h1>' +
        //     '</div>' +
        // '</div>';
        '<button class="btn btn-oval btn-secondary mr-2 mt-2 filter clickable" data-filter='+category.id_categoria+'>' +
            category.nombre +
        '</button>';
    $('#categories').append(html);
}

function showCalls(calls) {
    for (i=0; i<calls.length; i++) {
        var call = calls[i];
        showCall(call);
    }
}

function showCall(call) {
    var class_tipo = '';
    if (call._tipo_paquete == $TIPO_PAQUETE_GRATIS) {
        class_tipo = $CLASS_TIPO_PAQUETE_GRATIS;
    }
    else if (call._tipo_paquete == $TIPO_PAQUETE_POR_LLAMADAS) {
        class_tipo = $CLASS_TIPO_PAQUETE_POR_LLAMADAS;
    }
    else if (call._tipo_paquete == $TIPO_PAQUETE_POR_SUSCRIPCION) {
        class_tipo = $CLASS_TIPO_PAQUETE_POR_SUSCRIPCION;
    }
    var html =
        '<div class="element initially-hidden col-sm-3 '+call._id_categoria_paquete+'" data-category="'+call._id_categoria_paquete+'">' +
            '<div class="cardbox b">' +
                '<div class="p-2">' +
                    '<div class="cardbox-body bg-gradient-'+class_tipo+' text-center text-white rounded">' +
                        '<div class="text-bold">'+call._nombre_paquete+'</div>' +
                    '</div>' +
                '</div>' +
                '<div class="cardbox-body">' +
                    '<div class="row mb-3">'+
                        '<div class="col-2"> '+
                            '<em class="ion-checkmark-round mx-3"></em>' +
                        '</div>' +
                        '<div class="col-10">' +
                            '<span>'+call._minutos_paquete+' minutos</span>' +
                        '</div>' +
                    '</div>' +
                    '<div class="row mb-3">' +
                        '<div class="col-2">' +
                            '<em class="ion-checkmark-round mx-3"></em>' +
                        '</div>' +
                        '<div class="col-10">' +
                            '<span>Valido hasta '+((call._fecha_vencimiento_format == null)?'su uso':'el '+call._fecha_vencimiento_format)+'</span>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
                '<div class="cardbox-body text-center">' +
                    '<a class="btn btn-outline-'+class_tipo+' btn-usar" data-id-purchase="'+call.id_compra+'" href="#">USAR</a>' +
                '</div>' +
            '</div>' +
        '</div>';
    $('#calls').append(html);

    $('.btn-usar').click(function() {
        if ($(this).attr('data-id-purchase')) {
            var id_purchase = $(this).attr('data-id-purchase');
            var usuario = getUserData();
            document.location.href = urlClient.concat(usuario.directorio_vistas+"consultation.html?id_purchase="+id_purchase);
        }
    });
}

function initIsotope() {
    var $container = $('.elements');

    // init Isotope
    $container.isotope({
        itemSelector: '.element',
        layoutMode: 'fitRows',
        filter: ':not(.initially-hidden)'
    });

    var PageLoadFilter = ':not(.initially-hidden)';
    $container.isotope({filter: PageLoadFilter});

    // filter functions
    $('.filters').on('click', '.filter', function () {
        var filterValue = $(this).attr('data-filter');
        $container.isotope({filter: '.'+filterValue});
    });

    // change is-checked class on buttons
    $('.filter').each(function (i, button) {
        var $button = $(button);
        $button.on('click', function () {
            // $button.parent().find('.filter-selected').removeClass('filter-selected');
            // $(this).addClass('filter-selected');
            $button.parent().find('.btn-primary').removeClass('btn-primary btn-gradient');
            $(this).addClass('btn-primary btn-gradient');
        });
    });
}
