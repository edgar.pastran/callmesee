var urlServerPage = urlServer.concat("customer/package.php");

$(function() {
    checkParameters();
});

function checkParameters() {
    var id_package = getUrlParameter('id_package');
    if (id_package != '') {
        $('#id_paquete').val(id_package);
        loadData();
    }
    else {
        var usuario = getUserData();
        document.location.href = urlClient.concat(usuario.directorio_vistas+"packages.html");
    }
}

function loadData() {
    var params = {"operacion": "consultar", "id_paquete": $('#id_paquete').val()};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log("EXITO TRUE: "+JSON.stringify(respuesta));
                if (respuesta.data != null) {
                    $('#paquete_data').val(JSON.stringify(respuesta.data));
                    showPackage(respuesta.data);
                    showForm(respuesta.data);
                }
                else {
                    var usuario = getUserData();
                    document.location.href = urlClient.concat(usuario.directorio_vistas+"packages.html");
                }
            }
            else {
                // console.log("EXITO FALSE: "+JSON.stringify(respuesta));
                swal(respuesta.mensaje, '', 'error');
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}

function showPackage(package) {
    var class_tipo = '';
    var div_precio = ''
    if (package.tipo == $TIPO_PAQUETE_GRATIS) {
        class_tipo = $CLASS_TIPO_PAQUETE_GRATIS;
        div_precio =
            '<div class="h1 mb-4 text-'+class_tipo+' text-bold">GRATIS</div>';
    }
    else if (package.tipo == $TIPO_PAQUETE_POR_LLAMADAS) {
        class_tipo = $CLASS_TIPO_PAQUETE_POR_LLAMADAS;
        div_precio =
            '<div class="mb-4 text-'+class_tipo+'">' +
                '<span class="h1">' +
                    '<small>$ </small>' +
                    '<span class="text-bold">'+$.number(package.precio, 2, $PUNTO_DECIMAL, $PUNTO_MILES)+'</span>' +
                '</span>' +
            '</div>';
    }
    else if (package.tipo == $TIPO_PAQUETE_POR_SUSCRIPCION) {
        class_tipo = $CLASS_TIPO_PAQUETE_POR_SUSCRIPCION;
        div_precio =
            '<div class="mb-4 text-'+class_tipo+'">' +
                '<span class="h1">' +
                    '<small>$ </small>' +
                    '<span class="text-bold">'+$.number(package.precio, 2, $PUNTO_DECIMAL, $PUNTO_MILES)+'</span>' +
                '</span>' +
            '</div>';
    }
    var html =
        '<div class="cardbox b">' +
            '<div class="p-2">' +
                '<div class="cardbox-body bg-gradient-'+class_tipo+' text-center text-white rounded">' +
                    '<div class="text-bold">'+package.nombre+'</div>' +
                '</div>' +
            '</div>' +
            '<div class="cardbox-body">' +
                '<p class="mb-3">' +
                    '<em class="ion-checkmark-round mx-3"></em>' +
                    '<span>'+package.llamadas+' llamadas</span>' +
                '</p>' +
                '<p class="mb-3">' +
                    '<em class="ion-checkmark-round mx-3"></em>' +
                    '<span>'+package.minutos+' minutos por llamada</span>' +
                '</p>' +
                '<p class="mb-3">' +
                    '<em class="ion-checkmark-round mx-3"></em>' +
                    '<span>Valido '+((package.dias == null)?'hasta su uso':'por '+package.dias+' dias')+'</span>' +
                '</p>' +
            '</div>' +
            '<div class="cardbox-body text-center">' +
                div_precio+
            '</div>' +
        '</div>';
    $('#container-paquete').html(html);
}

function showForm(package) {
    var html =
        '<div class="cardbox">' +
            '<div class="cardbox-heading text-bold">Comprar Paquete</div>' +
            '<div class="cardbox-body bt">' +
                '<form class="cardbox cardbox-flat form-validate" action="" id="formulario-paquete" novalidate="">' +
                    '<div class="row">' +
                        '<div class="col-md-6 col-lg-6">' +
                            '<div class="form-group">' +
                                '<label>Categoria</label>' +
                                '<p id="categoria" class="text-primary text-bold">'+package._categoria_nombre+'</p>' +
                            '</div>' +
                        '</div>' +
                        '<div class="col-md-6 col-lg-6">' +
                            '<div class="form-group">' +
                                '<label>Nombre</label>' +
                                '<p id="nombre" class="text-primary text-bold">'+package.nombre+'</p>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                    '<div class="row">' +
                        '<div class="col-md-6 col-lg-6">' +
                            '<div class="form-group">' +
                                '<label>Precio</label>' +
                                '<p class="text-primary text-bold">$ '+$.number(package.precio, 2, $PUNTO_DECIMAL, $PUNTO_MILES)+'</p>' +
                            '</div>' +
                        '</div>' +
                        '<div class="col-md-6 col-lg-6">' +
                            '<div class="form-group">' +
                                '<label>Cantidad</label>' +
                                '<input id="cantidad" name="cantidad" class="form-control" type="number" min="1" required>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                    '<button id="btn_comprar" class="btn btn-info" type="submit">Comprar Paquete</button>' +
                '</form>' +
            '</div>' +
        '</div>';
    $('#container-formulario').html(html);

    // Validar Formulario
    $(validateForm);
}

function validateForm() {

    if (!$.fn.validate) return;

    var $form = $('#formulario-paquete');
    $form.validate({
        errorPlacement: errorPlacementInput,
        // Form rules
        rules: {
            cantidad: {
                required: true,
                number: true,
                min: 1
            }
        },
        submitHandler: function() {
            processSave();
        }
    });
}

function processSave() {
    var id_paquete = $('#id_paquete').val();
    var paquete_data = JSON.parse($('#paquete_data').val());
    var cantidad = $('#cantidad').val();
    addItemIntoCart(id_paquete, cantidad, paquete_data);
    var usuario = getUserData();
    document.location.href = urlClient.concat(usuario.directorio_vistas+"cart.html");
}