$(function() {
    swal({
        title: 'Pago Pendiente',
        text: 'Tu pago aun esta en proceso',
        type: 'warning',
        confirmButtonText: 'Ok',
        closeOnConfirm: false
    },
    function () {
        var usuario = getUserData();
        document.location.href = urlClient.concat(usuario.directorio_vistas+"index.html");
    });
});
