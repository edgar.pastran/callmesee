$(function() {
    swal({
        title: 'Pago Fallido',
        text: 'Tu pago no ha sido procesado',
        type: 'error',
        confirmButtonText: 'Ok',
        closeOnConfirm: false
    },
    function () {
        var usuario = getUserData();
        document.location.href = urlClient.concat(usuario.directorio_vistas+"index.html");
    });
});
