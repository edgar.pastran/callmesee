var urlServerPage = urlServer.concat("customer/packages.php");

$(function() {
    loadData();
});

function loadData() {
    var params = {"operacion": "consultar"};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log("EXITO TRUE: "+JSON.stringify(respuesta));
                showCategories(respuesta.data.categorias);
                showPackages(respuesta.data.paquetes);
                initIsotope();
            }
            else {
                // console.log("EXITO FALSE: "+JSON.stringify(respuesta));
                swal(respuesta.mensaje, '', 'error');
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}

function showCategories(categories) {
    for (i=0; i<categories.length; i++) {
        var category = categories[i];
        showCategory(category);
    }
}

function showCategory(category) {
    var html =
        // '<div class="filter card b clickable" data-filter='+category.id_categoria+'>' +
        //     '<div class="card-body text-center bb text-info">' +
        //         '<h1 class="my-0 text-bold">' +
        //             '<span style="font-size: 1.0rem">'+category.nombre+'</span>' +
        //         '</h1>' +
        //     '</div>' +
        // '</div>';
        '<button class="btn btn-oval btn-secondary mr-2 mt-2 filter clickable" data-filter='+category.id_categoria+'>' +
            category.nombre +
        '</button>';
    $('#categories').append(html);
}

function showPackages(packages) {
    for (i=0; i<packages.length; i++) {
        var package = packages[i];
        showPackage(package);
    }
}

function showPackage(package) {
    var class_tipo = '';
    var div_precio = ''
    if (package.tipo == $TIPO_PAQUETE_GRATIS) {
        class_tipo = $CLASS_TIPO_PAQUETE_GRATIS;
        div_precio =
            '<div class="h1 mb-4 text-'+class_tipo+' text-bold">GRATIS</div>';
    }
    else if (package.tipo == $TIPO_PAQUETE_POR_LLAMADAS) {
        class_tipo = $CLASS_TIPO_PAQUETE_POR_LLAMADAS;
        div_precio =
            '<div class="mb-4 text-'+class_tipo+'">' +
                '<span class="h1">' +
                    '<small>$ </small>' +
                    '<span class="text-bold">'+$.number(package.precio, 2, $PUNTO_DECIMAL, $PUNTO_MILES)+'</span>' +
                '</span>' +
            '</div>';
    }
    else if (package.tipo == $TIPO_PAQUETE_POR_SUSCRIPCION) {
        class_tipo = $CLASS_TIPO_PAQUETE_POR_SUSCRIPCION;
        div_precio =
            '<div class="mb-4 text-'+class_tipo+'">' +
                '<span class="h1">' +
                    '<small>$ </small>' +
                    '<span class="text-bold">'+$.number(package.precio, 2, $PUNTO_DECIMAL, $PUNTO_MILES)+'</span>' +
                '</span>' +
            '</div>';
    }
    var html =
        '<div class="element initially-hidden col-sm-3 '+package.id_categoria+'" data-category="'+package.id_categoria+'">' +
            '<div class="cardbox b">' +
                '<div class="p-2">' +
                    '<div class="cardbox-body bg-gradient-'+class_tipo+' text-center text-white rounded">' +
                        '<div class="text-bold">'+package.nombre+'</div>' +
                    '</div>' +
                '</div>' +
                '<div class="cardbox-body">' +
                    '<p class="mb-3">' +
                        '<em class="ion-checkmark-round mx-3"></em>' +
                        '<span>'+package.llamadas+' llamadas</span>' +
                    '</p>' +
                    '<p class="mb-3">' +
                        '<em class="ion-checkmark-round mx-3"></em>' +
                        '<span>'+package.minutos+' minutos por llamada</span>' +
                    '</p>' +
                    '<p class="mb-3">' +
                        '<em class="ion-checkmark-round mx-3"></em>' +
                        '<span>Valido '+((package.dias == null)?'hasta su uso':'por '+package.dias+' dias')+'</span>' +
                    '</p>' +
                '</div>' +
                '<div class="cardbox-body text-center">' +
                    div_precio+
                    '<a class="btn btn-outline-'+class_tipo+' btn-comprar" data-id-package="'+package.id_paquete+'" href="#">COMPRAR</a>' +
                '</div>' +
            '</div>' +
        '</div>';
    $('#packages').append(html);

    $('.btn-comprar').click(function() {
        if ($(this).attr('data-id-package')) {
            var id_package = $(this).attr('data-id-package');
            var usuario = getUserData();
            document.location.href = urlClient.concat(usuario.directorio_vistas+"package.html?id_package="+id_package);
        }
    });
}

function initIsotope() {
    var $container = $('.elements');

    // init Isotope
    $container.isotope({
        itemSelector: '.element',
        layoutMode: 'fitRows',
        filter: ':not(.initially-hidden)'
    });

    var PageLoadFilter = ':not(.initially-hidden)';
    $container.isotope({filter: PageLoadFilter});

    // filter functions
    $('.filters').on('click', '.filter', function () {
        var filterValue = $(this).attr('data-filter');
        $container.isotope({filter: '.'+filterValue});
    });

    // change is-checked class on buttons
    $('.filter').each(function (i, button) {
        var $button = $(button);
        $button.on('click', function () {
            // $button.parent().find('.filter-selected').removeClass('filter-selected');
            // $(this).addClass('filter-selected');
            $button.parent().find('.btn-primary').removeClass('btn-primary btn-gradient');
            $(this).addClass('btn-primary btn-gradient');
        });
    });
}
