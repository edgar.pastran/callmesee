var urlServerPage = urlServer.concat("customer/cart.php");

$(function() {
    loadPaymentWays();
    checkUserCart()
});

function loadPaymentWays() {
    var params = {"operacion": "consultar"};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log("EXITO TRUE: "+JSON.stringify(respuesta));
                for (i=0; i<respuesta.data.length; i++) {
                    var payment_way = respuesta.data[i];
                    showPaymentWay(payment_way);
                }
            }
            else {
                // console.log("EXITO FALSE: "+JSON.stringify(respuesta));
                swal(respuesta.mensaje, '', 'error');
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}

function showPaymentWay(payment_way) {
    var html =
        '<div class="col-md-2 col-lg-2" title="'+payment_way.nombre+'">' +
            '<div id="'+payment_way.id+'" class="custom-button">' +
                '<img src="'+payment_way.imagen+'">' +
                '<p>'+payment_way.nombre+'</p>' +
                '<label class="custom-control custom-checkbox">' +
                    '<input class="custom-control-input adviser-radio" id="payment_way-'+payment_way.id+'" name="payment_way-'+payment_way.id+'" type="checkbox">' +
                    '<span class="custom-control-indicator"></span>' +
                '</label>' +
            '</div>' +
        '</div>';
    $('#medios-pago').append(html);
}

function checkUserCart() {
    var cart = getCartData();
    if (cart != null && Object.keys(cart).length > 0) {
        clearTable();
        showItemsCart();
        calculateTotal();
    }
    else {
        redirectToPage()
    }
}

function clearTable() {
    $.each($('#table-cart > tbody > tr'), function(i, row) {
        $(row).remove();
    });
}

function showItemsCart() {
    var cart = getCartData();
    var keys = Object.keys(cart);
    for (i=0; i<keys.length; i++) {
        var id = keys[i];
        var data = cart[id];
        addItemCart(id, data);
    }
}

function addItemCart(id, data) {
    var cantidad = data.quantity;
    var paquete = data.item;
    var precio = parseFloat(paquete.precio)
    var subtotal = precio * cantidad;
    var row =
        '<tr id="'+id+'">' +
            '<td class="text-left">'+paquete.nombre+'</td>' +
            '<td class="text-left d-none d-md-table-cell">'+paquete._categoria_nombre+'</td>' +
            '<td class="text-right d-none d-md-table-cell price">' +
                '$ '+$.number(precio, 2, $PUNTO_DECIMAL, $PUNTO_MILES) +
                '<input type="hidden" class="precio" value="'+precio+'">' +
            '</td>' +
            '<td class="text-center quantity">' +
                $.number(cantidad, 0, $PUNTO_DECIMAL, $PUNTO_MILES) +
                '<input type="hidden" class="cantidad" value="'+cantidad+'">' +
            '</td>' +
            '<td class="text-right subtotal">' +
                '$ '+$.number(subtotal, 2, $PUNTO_DECIMAL, $PUNTO_MILES) +
                '<input type="hidden" class="sub-total" value="'+subtotal+'">' +
            '</td>' +
            '<td class="text-center">' +
                '<button class="btn red btn-outline tooltips btn-action delete-item-cart" type="button" data-original-title="Eliminar">' +
                    '<i class="ion-close-round"></i>' +
                '</button>' +
            '</td>' +
        '</tr>';

    if ($('#table-cart > tbody > tr').length > 0) {
        $('#table-cart > tbody:last-child').append(row);
    }
    else {
        $('#table-cart > tbody').html(row);
    }

    $('.delete-item-cart').click(function() {
        var id = $(this).closest('tr').attr('id');
        deleteItemCart(id);
    });

    handleTooltips();
}

function deleteItemCart(id) {
    deleteItemIntoCart(id);
    checkUserCart();
    showQuantityItemsCart();
    calculateTotal();
}

function calculateTotal() {
    var subtotal = 0;
    var total_impuesto = 0;
    $.each($('#table-cart > tbody > tr'), function(i, row) {
        subtotal += parseFloat($(this).find('.subtotal > .sub-total').val());
    });
    var total = subtotal + total_impuesto;
    $('#cart_subtotal').html("$ "+$.number( subtotal, 2, $PUNTO_DECIMAL, $PUNTO_MILES));
    $('#cart_tax').html("$ "+$.number( total_impuesto, 2, $PUNTO_DECIMAL, $PUNTO_MILES));
    $('#cart_total_format').html("$ "+$.number( total, 2, $PUNTO_DECIMAL, $PUNTO_MILES));
    $('#cart_total').val(total);
}

function redirectToPage() {
    var usuario = getUserData();
    document.location.href = urlClient.concat(usuario.directorio_vistas+"packages.html");
}

$('#btn-seguir-comprando').click(function() {
    redirectToPage();
});

$('#btn-pagar').click(function() {
    if (!($('#btn-pagar').hasClass("btn-secondary"))) {
        $('#btn-pagar').addClass("btn-secondary");
        var items = $('#table-cart > tbody > tr').length;
        if (items <= 0) {
            swal('Debe incluir paquetes al carro', '', 'error');
            $('#btn-pagar').removeClass("btn-secondary");
            return;
        }
        var medio_pago = $('#medios-pago').find('.selected').length;
        if (medio_pago <= 0) {
            swal('Debe seleccionar un medio de pago', '', 'error');
            $('#btn-pagar').removeClass("btn-secondary");
            return;
        }
        processPayment();
    }
});

function processPayment() {
    var medio_pago = $('#medios-pago').find('.selected').attr('id');
    var usuario = getUserData();
    var params = {"operacion": "pagar", "medio_pago": medio_pago, "usuario": JSON.stringify(usuario)};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServerPage,
        data: params,
        success: function(respuesta) {
            // console.log("RESPUESTA: "+JSON.stringify(respuesta));
            if (respuesta.exito == true) {
                // Limpiar el carro de compras
                clearCart();
                $('#medios-pago').append(respuesta.data);
                if (medio_pago == $MERCADO_PAGO_ID) {
                    $('#MP-Checkout')[0].click();
                }
            }
            else {
                console.log("EXITO FALSE: "+JSON.stringify(respuesta));
                swal(respuesta.mensaje, '', 'error');
            }
            $('#btn-pagar').removeClass("btn-secondary");
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
            $('#btn-pagar').removeClass("btn-secondary");
        }
    });
}
