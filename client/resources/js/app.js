/*!
 *
 * Dasha - Bootstrap Admin Template
 *
 * Version: 1.3
 * Author: @themicon_co
 * Website: http://themicon.co
 * License: https://wrapbootstrap.com/help/licenses
 *
 */

/**************************************************************************
 * GESTION DE PETICIONES AJAX - INICIO
 **************************************************************************/
$(document).on({
    ajaxStart: function () {
        $("body").addClass("loading");
    },
    ajaxStop: function () {
        $("body").removeClass("loading");
    }
});

(function() {
    'use strict';

    // Disable warning "Synchronous XMLHttpRequest on the main thread is deprecated.."
    $.ajaxPrefilter(function(options) {
        options.async = true;
    });

    // used for the preloader
    $(function() { document.body.style.opacity = 1; });

})();
/**************************************************************************
 * GESTION DE PETICIONES AJAX - FIN
 **************************************************************************/

/**************************************************************************
 * INICIALIZACION DE DATOS AL CULMINAR LA CARGA DE LAS PAGINAS - INICIO
 **************************************************************************/
$(function() {
    // Agregar el div usado para aplicar la clase loading
    addLoadingDiv();

    // Colocar el nombre de aplicacion donde este configurado
    loadApplicationName();

    // Agregar elementos especificos de un usuario
    var usuario = getUserData();
    if (usuario != null) {
        // Aplicar estilo segun el rol
        addBodyStyles(usuario);

        // Cargar los archivos HTML a incluir (Inclusion Interna)
        loadInnerHTML();

        // Buscar nuevos mensajes pendientes
        if (usuario.directorio_vistas.indexOf('adviser') >= 0) {
            searchNewMessages();
            setInterval(searchNewMessages, timeoutLoadNewMessages);
        }
    }

    // Aplicacion del plugin Select2
    loadSelect2();

    // Correcciones generales al mostrar un modal
    fixDetailsOnShowModal();
});

function addLoadingDiv() {
    if ($('body .modal-loading').length == 0) {
        // Para la animacion de carga usando GIF
        // $('body').append('<div class="modal-loading"></div>');

        // Para la animacion de carga usando CSS
        var bars = (loadingAnimationBars != undefined)?loadingAnimationBars:8;
        var modal = '<div class="modal-loading"><div class="spinner">';
        for (i=0; i<bars; i++) {
            modal += '<i></i>';
        }
        modal += '</div></div>';
        $('body').append(modal);
    }
}

function loadApplicationName() {
    $.each($('.application-name'), function(i, element) {
        $(element).html(applicationName+$(element).html());
    });
}

function addBodyStyles(usuario) {
    $('body').addClass('footer-fixed');
    if (usuario.directorio_vistas.indexOf('admin') >= 0 ) {
        //$('body').addClass('theme-2');
        $('body').addClass('theme-gradient-header-6');
    }
    else if (usuario.directorio_vistas.indexOf('adviser') >= 0) {
        //$('body').addClass('theme-4');
        $('body').addClass('theme-gradient-header-1');
    }
    else if (usuario.directorio_vistas.indexOf('customer') >= 0) {
        //$('body').addClass('theme-3');
        $('body').addClass('theme-gradient-header-3');
    }
}

function loadInnerHTML() {
    $.each($('.includeHTML'), function(i, element) {
        if (element.hasAttribute('data-html')) {
            $(element).load($(element).attr('data-html'), function() {
                // Cargar el ID del usuario en algunos elementos
                if ($(element).find('.user_email').length > 0) {
                    $.each($(element).find('.user_email'), function(i, element2) {
                        $(element2).html(usuario.id_usuario);
                        $(element2).val(usuario.id_usuario);
                    });
                }

                // Cargar el nombre del usuario en algunos elementos
                if ($(element).find('.user_firstname').length > 0) {
                    $.each($(element).find('.user_firstname'), function(i, element2) {
                        $(element2).html(usuario.nombres);
                        $(element2).val(usuario.nombres);
                    });
                }

                // Cargar el apellido del usuario en algunos elementos
                if ($(element).find('.user_lastname').length > 0) {
                    $.each($(element).find('.user_lastname'), function(i, element2) {
                        $(element2).html(usuario.apellidos);
                        $(element2).val(usuario.apellidos);
                    });
                }

                // Cargar el nombre completo del usuario en algunos elementos
                if ($(element).find('.user_fullname').length > 0) {
                    $.each($(element).find('.user_fullname'), function(i, element2) {
                        $(element2).html(usuario.nombre_completo);
                        $(element2).val(usuario.nombre_completo);
                    });
                }

                // Cargar el telefono del usuario en algunos elementos
                if ($(element).find('.user_phone').length > 0) {
                    $.each($(element).find('.user_phone'), function(i, element2) {
                        $(element2).html(usuario.telefono);
                        $(element2).val(usuario.telefono);
                    });
                }

                // Cargar la imagen del usuario en algunos elementos
                if ($(element).find('.user_image').length > 0) {
                    $.each($(element).find('.user_image'), function(i, element2) {
                        $(element2).attr("src", usuario.imagen);
                    });
                }

                // Cargar el titulo de la ventana en algunos elementos
                if ($(element).find('.view_title').length > 0 && element.hasAttribute('data-title')) {
                    $.each($(element).find('.view_title'), function(i, element2) {
                        $(element2).html($(element2).html()+$(element).attr('data-title'));
                    });
                }

                // Actualizar cantidad de items en el carro de compras
                if ($(element).find('.cart_items_total').length > 0) {
                    $.each($(element).find('.cart_items_total'), function(i, element2) {
                        var cart = getCartData();
                        var items = (cart != null)?Object.keys(cart).length:0;
                        $(element2).html(items);
                        $(element2).show();
                        if (items <= 0) {
                            $('.cart_items_total').hide();
                        }
                    });
                }

                // Cargar la funcionalidad del Menu (SideBar)
                if ($(element).find('.sidebar-nav').length > 0) {
                    (function () {
                        'use strict';

                        $(sidebarNav);

                        function sidebarNav() {
                            var $sidebarNav = $('.sidebar-nav');
                            var $sidebarContent = $('.sidebar-content');

                            activate($sidebarNav);

                            $sidebarNav.on('click', function (event) {
                                var item = getItemElement(event);
                                // check click is on a tag
                                if (!item) return;

                                var ele = $(item),
                                    liparent = ele.parent()[0];

                                var lis = ele.parent().parent().children(); // markup: ul > li > a
                                // remove .active from childs
                                lis.find('li').removeClass('active');
                                // remove .active from siblings ()
                                $.each(lis, function (idx, li) {
                                    if (li !== liparent)
                                        $(li).removeClass('active');
                                });

                                var next = ele.next();
                                if (next.length && next[0].tagName === 'UL') {
                                    ele.parent().toggleClass('active');
                                    event.preventDefault();
                                }
                            });

                            // find the a element in click context
                            // doesn't check deeply, asumens two levels only
                            function getItemElement(event) {
                                var element = event.target,
                                    parent = element.parentNode;
                                if (element.tagName.toLowerCase() === 'a') return element;
                                if (parent.tagName.toLowerCase() === 'a') return parent;
                                if (parent.parentNode.tagName.toLowerCase() === 'a') return parent.parentNode;
                            }

                            function activate(sidebar) {
                                sidebar.find('a').each(function () {
                                    var $this = $(this),
                                        href = $this.attr('href').replace('#', '');
                                    if (href !== '' && window.location.href.indexOf('/' + href) >= 0) {
                                        var item = $this.parents('li').addClass('active');
                                        // Animate scrolling to focus active item
                                        $sidebarContent.animate({
                                            scrollTop: $sidebarContent.scrollTop() + item.position().top - (window.innerHeight / 2)
                                        }, 100);
                                        return false; // exit foreach
                                    }
                                });
                            }

                            var layoutContainer = $('.layout-container');
                            var $body = $('body');
                            // Handler to toggle sidebar visibility on mobile
                            $('.sidebar-toggler').click(function (e) {
                                e.preventDefault();
                                layoutContainer.toggleClass('sidebar-visible');
                                // toggle icon state
                                $(this).parent().toggleClass('active');
                            });
                            // Close sidebar when click on backdrop
                            $('.sidebar-layout-obfuscator').click(function (e) {
                                e.preventDefault();
                                $body.removeClass('sidebar-cover-visible'); // for use with cover mode
                                layoutContainer.removeClass('sidebar-visible'); // for use on mobiles
                                // restore icon
                                $('.sidebar-toggler').parent().removeClass('active');
                            });

                            // escape key closes sidebar on desktops
                            $(document).keyup(function (e) {
                                if (e.keyCode === 27) {
                                    $body.removeClass('sidebar-cover-visible');
                                }
                            });

                            // Handler to toggle sidebar visibility on desktop
                            $('.covermode-toggler').click(function (e) {
                                e.preventDefault();
                                $body.addClass('sidebar-cover-visible');
                            });

                            $('.sidebar-close').click(function (e) {
                                e.preventDefault();
                                $body.removeClass('sidebar-cover-visible');
                            });

                            // remove desktop offcanvas when app changes to mobile
                            // so when it returns, the sidebar is shown again
                            window.addEventListener('resize', function () {
                                if (window.innerWidth < 768) {
                                    $body.removeClass('sidebar-cover-visible');
                                }
                            });

                        }

                    })();
                }

                // Colocar el nombre de aplicacion donde este configurado
                if ($(element).find('.application-name').length > 0) {
                    $.each($(element).find('.application-name'), function(i, element2) {
                        $(element2).html(applicationName+$(element2).html());
                    });
                }

                // Ejecutar la funcion init() si esta definida
                if ($(element).hasClass('executeInitFunction')) {
                    if( typeof init !== 'undefined' && jQuery.isFunction( init ) ) {
                        //Es seguro ejecutara la función
                        init();
                    }
                }
            });
        }
    });
}

function showQuantityItemsCart() {
    var cart = getCartData();
    var items = (cart != null)?Object.keys(cart).length:0;
    // Actualizar los tooltips que muestran la cantidad de mensajes sin leer
    $('.cart_items_total').html(items);
    $('.cart_items_total').show();
    if (items <= 0) {
        $('.cart_items_total').hide();
    }
}

function searchNewMessages() {
    var usuario = getUserData();
    var params = {"operacion": "consultar_mensajes_pendientes", "usuario": JSON.stringify(usuario)};
    // console.log("DATA TO SEND: "+JSON.stringify(params));
    $.ajax({
        type: "POST",
        url: urlServer.concat("messages.php"),
        data: params,
        success: function(respuesta) {
            if (respuesta.exito == true) {
                // console.log("EXITO TRUE: "+JSON.stringify(respuesta));
                // Actualizar los tooltips que muestran la cantidad de mensajes sin leer
                $('.messages_total').html(respuesta.total_mensajes);
                $('.messages_total').show();
                if (respuesta.total_mensajes <= 0) {
                    $('.messages_total').hide();
                }
                // Mostrar notificaciones push por cada mensaje reciente
                $.each(respuesta.mensajes_recientes, function(i, mensaje) {
                    Push.create(applicationName, {
                        body: 'Tienes una nueva '+mensaje.titulo+' por '+mensaje.usuario_nombre_completo,
                        icon: mensaje.usuario_imagen,
                        timeout: timeoutLoadNewMessages,
                        onClick: function() {
                            // window.open(mensaje.url_apertura);
                            window.open(urlClient.concat(usuario.directorio_vistas+"messages.html?id_message="+mensaje.id_mensaje));
                            this.close();
                            return false;
                        }
                    });
                });
            }
            else {
                console.log("EXITO FALSE: "+JSON.stringify(respuesta));
            }
        },
        error: function(respuesta) {
            console.log("ERROR: "+JSON.stringify(respuesta));
        }
    });
}

function loadSelect2() {
    if (!$.fn.select2) {
        return;
    }
    $('.select2').select2();
}

function fixDetailsOnShowModal() {
    $('.modal').on('shown.bs.modal', function (e) {
        // Correcciones en los DataTables
        if ($.fn.dataTable) {
            $.fn.dataTable.tables({visible: true, api: true}).columns.adjust();
            fixScrollableDataTable();
        }

        // Quitar las clases error y valid de los inputs
        $.each($(this).find('input, textarea, select'), function (i, element) {
            $(element).removeClass('error');
            $(element).removeClass('valid');
        });
        // Quitar las etiquetas de error que acompana a los inputs
        $.each($(this).find('label.error'), function (i, element) {
            $(element).remove();
        });
    });
}
/**************************************************************************
 * INICIALIZACION DE DATOS AL CULMINAR LA CARGA DE LAS PAGINAS - FIN
 **************************************************************************/

/**************************************************************************
 * GESTION DE EVENTOS DE ELEMENTOS GENERICOS EN LAS PAGINAS - INICIO
 **************************************************************************/
$(document).on('click', '.close_session', function(e) {
    e.preventDefault();
    closeSession();
});

$(document).on('click', '.user_dashboard', function(e) {
    e.preventDefault();
    document.location.href = urlClient.concat(usuario.directorio_vistas+"index.html");
});

$(document).on('click', '.user_messages', function(e) {
    e.preventDefault();
    document.location.href = urlClient.concat(usuario.directorio_vistas+"messages.html");
});

$(document).on('click', '.user_profile', function(e) {
    e.preventDefault();
    document.location.href = urlClient.concat(usuario.directorio_vistas+"profile.html");
});

$(document).on('click', '.user_cart', function(e) {
    e.preventDefault();
    document.location.href = urlClient.concat(usuario.directorio_vistas+"cart.html");
});

$(document).on('click', '.customer_packages', function(e) {
    e.preventDefault();
    document.location.href = urlClient.concat(usuario.directorio_vistas+"packages.html");
});

$(document).on('click', '.custom-button', function(e) {
    e.preventDefault();
    var others = $(this).closest('.custom-button-group').find('.selected');
    if (others.length > 0) {
        others.removeClass('selected');
        if ($(others).find('input:checkbox').length > 0) {
            $(others).find('input:checkbox').prop('checked',false);
        }
    }
    $(this).addClass('selected');
    if ($(this).find('input:checkbox').length > 0) {
        $(this).find('input:checkbox').prop('checked',true);
    }
});
/**************************************************************************
 * GESTION DE EVENTOS DE ELEMENTOS GENERICOS EN LAS PAGINAS - FIN
 **************************************************************************/