// Funcion que permite obtener el valor de un parametro
// presente en la URL (Query String)
function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
};

// Funcion usada en la validacion de formularios
// con JQuery Validation
// Necessary to place dyncamic error messages
// without breaking the expected markup for custom input
function errorPlacementInput(error, element) {
    if (element.is(':radio') || element.is(':checkbox')) {
        if (element.closest('.custom-button').length) {
            error.insertAfter(element.closest('.custom-button'));
        }
        else {
            error.insertAfter(element.parent());
        }
    }
    else if(element.parent('.input-group').length) {
        error.insertAfter(element.parent());
    }
    else {
        error.insertAfter(element);
    }
}

// Funcion que devuelve un boolean indicando si
// el cliente esta usando Microsof Internet Explorer
function msie() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf('MSIE ');
    return (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./));
}

// Funcion usada para resolver un conflicto que se presenta
// entre el plugin X-Editable y DatePicker
function fix_xeditable_conflict() {
    $('.datepicker > div:first-of-type').css('display', 'initial');
}

// Funcion para crear un DataTable a partir de una tabla
function createDataTable(table, withPaging) {
    withPaging = (withPaging != undefined)?withPaging:true;
    // Setup - add a text input to each footer cell
    $(table).find('tfoot th').each( function () {
        if ($(this).hasClass('search-individual')) {
            var title = $(this).text();
            $(this).html( '<input type="text" class="form-control search-individual" placeholder="Buscar por '+title+'" />' );
        }
    } );

    var options = {
        'paging': withPaging, // Table pagination
        'ordering': true, // Column ordering
        'info': true, // Bottom left status text
        // Text translation options
        // Note the required keywords between underscores (e.g _MENU_)
        oLanguage: {
            sSearch: '<em class="ion-search"></em>',
            sLengthMenu: '_MENU_ registros por pagina',
            sInfo: 'Mostrando pagina _PAGE_ de _PAGES_',
            sZeroRecords: 'No hay registros',
            sInfoEmpty: 'No se encontraron datos',
            sInfoFiltered: '(filtrado de _MAX_ registros totales)',
            oPaginate: {
                sNext: '<em class="ion-ios-arrow-right"></em>',
                sPrevious: '<em class="ion-ios-arrow-left"></em>'
            }
        }
    };
    if (withPaging == false) {
        options.scrollY = '50vh';
        options.scrollCollapse = true;
        options.dom = '<"toolbar">frtip';
        options.drawCallback = function () {
            fixScrollableDataTable();
        };
        options.initComplete = function(settings, json) {
            fixScrollableDataTable();
        };
        options.oLanguage.sInfo = 'Mostrando _MAX_ registros';
    }
    var dataTable  = $(table).DataTable(options);

    // Hiiden columns
    $.each($(table).find('thead th'), function(i, element) {
        if ($(element).hasClass('hidden-column')) {
            dataTable.column(i).visible(false);
        }
    });

    // Apply the search
    dataTable.columns().every( function () {
        var that = this;
        $( 'input.search-individual', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );

    return dataTable;
}

// Funcion para corregir detalles de un DataTable scrollable
function fixScrollableDataTable() {
    $('.dataTables_scrollBody thead tr').css({visibility:'collapse'});
    $('.dataTables_scrollBody').css({'margin-top':'-30px'});
}

// Funcion para crear los ToolTips
function handleTooltips() {
    $('.tooltip.fade.bs-tooltip-top.show').remove();

    // global tooltips
    $('.tooltips').tooltip();
};

// Funcion para limpiar un formulario
function cleanForm(form) {
    $.each($(form).find('input, textarea, select'), function(i, element) {
        $(element).val("");
    });
}

// Funcion para habilitar un formulario
function enableForm(form) {
    $.each($(form).find('input, textarea, select'), function (i, element) {
        enableElement(element);
    });
}

// Funcion para deshabilitar un formulario
function disableForm(form) {
    $.each($(form).find('input, textarea, select'), function(i, element) {
        disableElement(element);
    });
}

// Funcion para habilitar un elemento
function enableElement(element) {
    $(element).prop("disabled", false);
}

// Funcion para deshabilitar un elemento
function disableElement(element) {
    $(element).prop("disabled", true);
}
