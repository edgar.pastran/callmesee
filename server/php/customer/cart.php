<?php //
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

$respuesta = array( 'exito' => false, 'mensaje' => '');
try {
    if (isset($_POST)) {
        if (isset($_POST['operacion'])) {
            require_once("../service/customer_cart.php");

            $service = new Customer_Cart();
            $operacion = $_POST['operacion'];

            if ($operacion == "consultar") {
                $respuesta = $service->consultar();
            }
            else if ($operacion == "pagar" && isset($_POST['medio_pago'])  && isset($_POST['usuario'])) {
                $medio_pago = $_POST['medio_pago'];
                $usuario = json_decode($_POST['usuario']);
                $respuesta = $service->pagar($medio_pago, $usuario);
            }
            else {
                $respuesta['mensaje'] = 'No se indicaron todos los parametros necesarios.';
            }
        }
        else {
            $respuesta['mensaje'] = 'Es necesario indicar la operacion.';
        }
    }
    else {
        $respuesta['mensaje'] = 'Solo se permiten parametros mediante el metodo POST.';
    }
}
catch(Exception $e) {
    $respuesta['mensaje'] = $e->getMessage();
}
echo json_encode($respuesta, true);
?>
