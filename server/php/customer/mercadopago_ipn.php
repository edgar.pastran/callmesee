<?php
// Motrar todos los errores de PHP
error_reporting(E_ALL);
ini_set('display_errors', '1');

//Get the semaphore
$semaphore = sem_get(ftok(__FILE__, chr(1)), 1);
//Block the semaphore
sem_acquire($semaphore);

if (!isset($_GET["id"], $_GET["topic"]) || !ctype_digit($_GET["id"])) {
    http_response_code(400);
}
else {
    require_once ("../service/customer_ipn.php");

    $customer_ipn = new Customer_IPN();
    $respuesta = $customer_ipn->revisar_mercadopago_notificacion($_GET);
    if ($respuesta['exito']) {
        http_response_code(200);
    }
}

// Release the semaphore
sem_release($semaphore);
?>