<?php //
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

$respuesta = array( 'exito' => false, 'mensaje' => '');
try {
    if (isset($_POST)) {
        if (isset($_POST['operacion'])) {
            require_once ("../service/customer_consultation.php");

            $servicio = new Customer_Consultation();
            $operacion = $_POST['operacion'];

            if ($operacion == "consultar" && isset($_POST['usuario']) && isset($_POST['id_compra'])) {
                $usuario = json_decode($_POST['usuario']);
                $id_compra = $_POST['id_compra'];
                $respuesta = $servicio->consultar($usuario, $id_compra);
            }
            else if ($operacion == "guardar" && isset($_POST['usuario']) && isset($_POST['consulta'])) {
                $usuario = json_decode($_POST['usuario']);
                $consulta = $_POST['consulta'];
                $respuesta = $servicio->guardar($usuario, $consulta);
            }
            else if ($operacion == "consultar_respuesta_asesor" && isset($_POST['id_consulta'])) {
                $id_consulta = $_POST['id_consulta'];
                $respuesta = $servicio->consultar_respuesta_asesor($id_consulta);
            }
            else if ($operacion == "guardar_inicio_llamada" && isset($_POST['id_compra']) && isset($_POST['id_consulta'])) {
                $id_compra = $_POST['id_compra'];
                $id_consulta = $_POST['id_consulta'];
                $respuesta = $servicio->guardar_inicio_llamada($id_compra, $id_consulta);
            }
            else if ($operacion == "guardar_comentarios_llamada" && isset($_POST['consulta'])) {
                $consulta = $_POST['consulta'];
                $respuesta = $servicio->guardar_comentarios_llamada($consulta);
            }
            else if ($operacion == "expirar_consulta" && isset($_POST['id_consulta'])) {
                $id_consulta = $_POST['id_consulta'];
                $respuesta = $servicio->expirar_consulta($id_consulta);
            }
            else {
                $respuesta['mensaje'] = 'No se indicaron todos los parametros necesarios.';
            }
        }
        else {
            $respuesta['mensaje'] = 'Es necesario indicar la operacion.';
        }
    }
    else {
        $respuesta['mensaje'] = 'Solo se permiten parametros mediante el metodo POST.';
    }
}
catch(Exception $e) {
    $respuesta['mensaje'] = $e->getMessage();
}
echo json_encode($respuesta, true);
?>
