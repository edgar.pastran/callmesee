<?php
/*
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 04/07/2018
 * Time: 04:47 PM
 */

require_once(__DIR__."/../config/connection.php");
require_once(__DIR__."/../config/constant.php");
class Configuracion {

    private static $_CAMPOS_CLAVE = array("id_configuracion");

    public static function consultar_configuraciones($like="") {
        $conexion = new Connection();
        $sql =
            "SELECT * ".
            "FROM ".Constant::$TABLA_CONFIGURACION." ".
            "WHERE nombre LIKE '%".$like."%'".
            "ORDER BY nombre";
        $data = $conexion->consulta($sql);
        return $data;
    }

    public static function consultar_configuracion_por_id($id) {
        $conexion = new Connection();
        $sql =
            "SELECT * ".
            "FROM ".Constant::$TABLA_CONFIGURACION." ".
            "WHERE id_configuracion = ".$id;
        $data = $conexion->consulta($sql);
        return (count($data))?$data[0]:null;
    }

    public static function consultar_configuracion_por_nombre($nombre) {
        $conexion = new Connection();
        $sql =
            "SELECT * ".
            "FROM ".Constant::$TABLA_CONFIGURACION." ".
            "WHERE nombre = '".$nombre."'";
        $data = $conexion->consulta($sql);
        return (count($data))?$data[0]:null;
    }

    private static function registrar_configuracion($configuracion) {
        $conexion = new Connection();
        $respuesta = $conexion->insertar_desde_objeto($configuracion, Constant::$TABLA_CONFIGURACION);
        if ($respuesta["exito"]) {
            $respuesta["data"] = self::consultar_configuracion_por_id($respuesta["codigo"]);
        }
        return $respuesta;
    }

    private static function actualizar_configuracion($configuracion) {
        $conexion = new Connection();
        $respuesta = $conexion->actualizar_desde_objeto($configuracion, self::$_CAMPOS_CLAVE, Constant::$TABLA_CONFIGURACION);
        if ($respuesta["exito"]) {
            $respuesta["data"] = self::consultar_configuracion_por_id($respuesta["codigo"]);
        }
        return $respuesta;
    }

    public static function guardar_configuracion($configuracion) {
        $otro_registro = self::consultar_configuracion_por_nombre($configuracion["nombre"]);
        if ($otro_registro == null) {
            $respuesta = self::registrar_configuracion($configuracion);
        }
        else {
            $configuracion['id_configuracion'] = $otro_registro['id_configuracion'];
            $respuesta = self::actualizar_configuracion($configuracion);
        }
        return $respuesta;
    }

}