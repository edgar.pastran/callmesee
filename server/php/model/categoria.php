<?php
/*
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 04/07/2018
 * Time: 04:47 PM
 */

require_once(__DIR__."/../config/connection.php");
require_once(__DIR__."/../config/constant.php");
class Categoria {

    private static $_CAMPOS_CLAVE = array("id_categoria");

    public static function consultar_categorias($estado=null) {
        $conexion = new Connection();
        $sql =
            "SELECT * ".
            "FROM ".Constant::$TABLA_CATEGORIA." ";
        $sql .=
            (($estado!=null)?"WHERE estado = '".$estado."' ":"");
        $sql .=
            "ORDER BY nombre";
        $data = $conexion->consulta($sql);
        return $data;
    }

    public static function consultar_categoria_por_id($id) {
        $conexion = new Connection();
        $sql =
            "SELECT * ".
            "FROM ".Constant::$TABLA_CATEGORIA." ".
            "WHERE id_categoria = ".$id;
        $data = $conexion->consulta($sql);
        return (count($data))?$data[0]:null;
    }

    public static function registrar_categoria($categoria) {
        $conexion = new Connection();
        $respuesta = $conexion->insertar_desde_objeto($categoria, Constant::$TABLA_CATEGORIA);
        if ($respuesta["exito"]) {
            $respuesta["data"] = self::consultar_categoria_por_id($respuesta["codigo"]);
        }
        return $respuesta;
    }

    public static function actualizar_categoria($categoria) {
        $conexion = new Connection();
        $respuesta = $conexion->actualizar_desde_objeto($categoria, self::$_CAMPOS_CLAVE, Constant::$TABLA_CATEGORIA);
        if ($respuesta["exito"]) {
            $respuesta["data"] = self::consultar_categoria_por_id($respuesta["codigo"]);
        }
        return $respuesta;
    }

    private static function actualizar_estado_categoria($id_categoria, $estado) {
        $conexion = new Connection();
        $categoria = array(
            "id_categoria" => $id_categoria,
            "estado" => $estado
        );
        $respuesta = $conexion->actualizar_desde_objeto($categoria, self::$_CAMPOS_CLAVE, Constant::$TABLA_CATEGORIA);
        if ($respuesta["exito"]) {
            $respuesta["data"] = self::consultar_categoria_por_id($respuesta["codigo"]);
        }
        return $respuesta;
    }

    public static function desactivar_categoria($id_categoria) {
        return self::actualizar_estado_categoria($id_categoria, Constant::$REGISTRO_INACTIVO);
    }

    public static function activar_categoria($id_categoria) {
        return self::actualizar_estado_categoria($id_categoria, Constant::$REGISTRO_ACTIVO);
    }
}