<?php
/*
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 04/07/2018
 * Time: 04:47 PM
 */

require_once(__DIR__."/../config/connection.php");
require_once(__DIR__."/../config/constant.php");
class Compra {

    private static $_CAMPOS_CLAVE = array("id_compra");

    public static function consultar_compras($estado=null) {
        $conexion = new Connection();
        $sql =
            "SELECT * ".
            "FROM ".Constant::$TABLA_COMPRA." ";
        $sql .=
            (($estado!=null)?"WHERE estado = '".$estado."' ":"");
        $sql .=
            "ORDER BY id_compra DESC";
        $data = $conexion->consulta($sql);
        return $data;
    }

    private static function consultar_compra_por_id($id) {
        $conexion = new Connection();
        $sql =
            "SELECT * ".
            "FROM ".Constant::$TABLA_COMPRA." ".
            "WHERE id_compra = ".$id;
        $data = $conexion->consulta($sql);
        return (count($data))?$data[0]:null;
    }

    public static function consultar_compras_por_cliente($id_cliente) {
        $conexion = new Connection();
        $sql =
            "SELECT * ".
            "FROM ".Constant::$TABLA_COMPRA." ".
            "WHERE id_cliente = ".$id_cliente." ".
            "ORDER BY id_compra DESC";
        $data = $conexion->consulta($sql);
        return $data;
    }

    public static function consultar_categorias_compras_disponibles_por_cliente($id_cliente) {
        $conexion = new Connection();
        $sql =
            "SELECT * ".
            "FROM ".Constant::$TABLA_CATEGORIA." ".
            "WHERE id_categoria IN (".
                "SELECT PA.id_categoria ".
                "FROM ".Constant::$TABLA_COMPRA." CO, ".Constant::$TABLA_PAQUETE." PA ".
                "WHERE CO.id_paquete = PA.id_paquete ".
                "AND CO.id_cliente = ".$id_cliente." ".
                "AND CO.consumido = '".Constant::$NO."' ".
                "AND (CO.fecha_vencimiento IS NULL OR CO.fecha_vencimiento > NOW())".
            ") ".
            "ORDER BY nombre";
        $data = $conexion->consulta($sql);
        return $data;
    }

    public static function consultar_compras_disponibles_por_cliente($id_cliente) {
        $conexion = new Connection();
        $sql =
            "SELECT CO.*, ".
            "PA.id_categoria AS _id_categoria_paquete, PA.nombre AS _nombre_paquete,  PA.tipo AS _tipo_paquete, ".
            "PP.minutos AS _minutos_paquete ".
            "FROM ".Constant::$TABLA_COMPRA." CO, ".Constant::$TABLA_PAQUETE." PA, ".Constant::$TABLA_PAGO_PAQUETES." PP ".
            "WHERE CO.id_paquete = PA.id_paquete ".
            "AND CO.id_pago = PP.id_pago ".
            "AND CO.id_paquete = PP.id_paquete ".
            "AND CO.id_cliente = ".$id_cliente." ".
            "AND CO.consumido = '".Constant::$NO."' ".
            "AND (CO.fecha_vencimiento IS NULL OR CO.fecha_vencimiento > NOW()) ".
            "ORDER BY CO.fecha_vencimiento DESC";
        return $conexion->consulta($sql);
    }

    public static function consultar_compra_disponible_por_cliente_id($id_cliente, $id) {
        $conexion = new Connection();
        $sql =
            "SELECT CO.*, PA.id_categoria AS _id_categoria, PP.minutos AS _minutos ".
            "FROM ".Constant::$TABLA_COMPRA." CO, ".Constant::$TABLA_PAQUETE." PA, ".Constant::$TABLA_PAGO_PAQUETES." PP ".
            "WHERE CO.id_paquete = PA.id_paquete ".
            "AND CO.id_pago = PP.id_pago ".
            "AND CO.id_paquete = PP.id_paquete ".
            "AND CO.consumido = '".Constant::$NO."' ".
            "AND (CO.fecha_vencimiento IS NULL OR CO.fecha_vencimiento > NOW()) ".
            "AND CO.id_cliente = ".$id_cliente." ".
            "AND CO.id_compra = ".$id;
        $data = $conexion->consulta($sql);
        return (count($data))?$data[0]:null;
    }

    public static function contar_compras_por_pago_paquete($id_pago, $id_paquete) {
        $conexion = new Connection();
        $sql =
            "SELECT COUNT(*) AS counter ".
            "FROM ".Constant::$TABLA_COMPRA." ".
            "WHERE id_pago = ".$id_pago." ".
            "AND id_paquete = ".$id_paquete."";
        $data = $conexion->consulta($sql);
        return (count($data))?$data[0]['counter']:0;
    }

    public static function registrar_compra($compra) {
        $conexion = new Connection();
        $respuesta = $conexion->insertar_desde_objeto($compra, Constant::$TABLA_COMPRA);
        if ($respuesta["exito"]) {
            $respuesta["data"] = self::consultar_compra_por_id($respuesta["codigo"]);
        }
        return $respuesta;
    }

    private static function actualizar_compra($compra) {
        $conexion = new Connection();
        $respuesta = $conexion->actualizar_desde_objeto($compra, self::$_CAMPOS_CLAVE, Constant::$TABLA_COMPRA);
        if ($respuesta["exito"]) {
            $respuesta["data"] = self::consultar_compra_por_id($respuesta["codigo"]);
        }
        return $respuesta;
    }

    public static function consumir_compra($id_compra, $id_consulta) {
        $compra = array(
            "id_compra" => $id_compra,
            "consumido" => Constant::$SI,
            "fecha_consumo" => "NOW()",
            "id_consulta" => $id_consulta
        );
        return self::actualizar_compra($compra);
    }

}