<?php
/*
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 04/07/2018
 * Time: 04:47 PM
 */

require_once(__DIR__."/../config/connection.php");
require_once(__DIR__."/../config/constant.php");
class Pregunta {

    private static $_CAMPOS_CLAVE = array("id_pregunta");

    public static function consultar_preguntas($estado=null) {
        $conexion = new Connection();
        $sql =
            "SELECT PR.*, CA.nombre AS _categoria_nombre ".
            "FROM ".Constant::$TABLA_PREGUNTA." PR, ".Constant::$TABLA_CATEGORIA." CA ".
            "WHERE PR.id_categoria = CA.id_categoria ";
        $sql .=
            (($estado!=null)?"AND PR.estado = '".$estado."'":"");
        $sql .=
            "ORDER BY PR.id_categoria ASC, PR.orden ASC";
        $data = $conexion->consulta($sql);
        return $data;
    }

    public static function consultar_preguntas_activas_por_categoria($id_categoria) {
        $conexion = new Connection();
        $sql =
            "SELECT * ".
            "FROM ".Constant::$TABLA_PREGUNTA." ".
            "WHERE id_categoria = ".$id_categoria." ".
            "AND estado = '".Constant::$REGISTRO_ACTIVO."'".
            "ORDER BY orden ASC";
        $data = $conexion->consulta($sql);
        return $data;
    }

    public static function consultar_pregunta_por_id($id) {
        $conexion = new Connection();
        $sql =
            "SELECT PR.*, CA.nombre AS _categoria_nombre ".
            "FROM ".Constant::$TABLA_PREGUNTA." PR, ".Constant::$TABLA_CATEGORIA." CA ".
            "WHERE PR.id_categoria = CA.id_categoria ".
            "AND PR.id_pregunta = ".$id;
        $data = $conexion->consulta($sql);
        return (count($data))?$data[0]:null;
    }

    public static function registrar_pregunta($pregunta) {
        $conexion = new Connection();
        $respuesta = $conexion->insertar_desde_objeto($pregunta, Constant::$TABLA_PREGUNTA);
        if ($respuesta["exito"]) {
            $respuesta["data"] = self::consultar_pregunta_por_id($respuesta["codigo"]);
        }
        return $respuesta;
    }

    public static function actualizar_pregunta($pregunta) {
        $conexion = new Connection();
        $respuesta = $conexion->actualizar_desde_objeto($pregunta, self::$_CAMPOS_CLAVE, Constant::$TABLA_PREGUNTA);
        if ($respuesta["exito"]) {
            $respuesta["data"] = self::consultar_pregunta_por_id($respuesta["codigo"]);
        }
        return $respuesta;
    }

    private static function actualizar_estado_pregunta($id_pregunta, $estado) {
        $conexion = new Connection();
        $pregunta = array(
            "id_pregunta" => $id_pregunta,
            "estado" => $estado
        );
        $respuesta = $conexion->actualizar_desde_objeto($pregunta, self::$_CAMPOS_CLAVE, Constant::$TABLA_PREGUNTA);
        if ($respuesta["exito"]) {
            $respuesta["data"] = self::consultar_pregunta_por_id($respuesta["codigo"]);
        }
        return $respuesta;
    }

    public static function desactivar_pregunta($id_pregunta) {
        return self::actualizar_estado_pregunta($id_pregunta, Constant::$REGISTRO_INACTIVO);
    }

    public static function activar_pregunta($id_pregunta) {
        return self::actualizar_estado_pregunta($id_pregunta, Constant::$REGISTRO_ACTIVO);
    }
}