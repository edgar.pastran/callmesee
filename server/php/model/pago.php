<?php
/*
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 04/07/2018
 * Time: 04:47 PM
 */

require_once(__DIR__."/../config/connection.php");
require_once(__DIR__."/../config/constant.php");
class Pago {

    private static $_CAMPOS_CLAVE = array("id_pago");

    public static function consultar_pagos($estado=null) {
        $conexion = new Connection();
        $sql =
            "SELECT * ".
            "FROM ".Constant::$TABLA_PAGO." ";
        $sql .=
            (($estado!=null)?"WHERE estado = '".$estado."' ":"");
        $sql .=
            "ORDER BY id_pago DESC";
        $data = $conexion->consulta($sql);
        return $data;
    }

    public static function consultar_pago_por_id($id) {
        $conexion = new Connection();
        $sql =
            "SELECT * ".
            "FROM ".Constant::$TABLA_PAGO." ".
            "WHERE id_pago = ".$id;
        $data = $conexion->consulta($sql);
        return (count($data))?$data[0]:null;
    }

    public static function consultar_pago_por_referencia($id_referencia) {
        $conexion = new Connection();
        $sql =
            "SELECT * ".
            "FROM ".Constant::$TABLA_PAGO." ".
            "WHERE id_referencia = '".$id_referencia."'";
        $data = $conexion->consulta($sql);
        return (count($data))?$data[0]:null;
    }

    public static function consultar_pago_por_referencia_medio_pago($id_referencia, $medio_pago) {
        $conexion = new Connection();
        $sql =
            "SELECT * ".
            "FROM ".Constant::$TABLA_PAGO." ".
            "WHERE id_referencia = '".$id_referencia."' ".
            "AND medio_pago = '".$medio_pago."'";
        $data = $conexion->consulta($sql);
        return (count($data))?$data[0]:null;
    }

    public static function registrar_pago($pago) {
        $conexion = new Connection();
        $respuesta = $conexion->insertar_desde_objeto($pago, Constant::$TABLA_PAGO);
        if ($respuesta["exito"]) {
            $respuesta["data"] = self::consultar_pago_por_id($respuesta["codigo"]);
        }
        return $respuesta;
    }

    private static function actualizar_pago($pago) {
        $conexion = new Connection();
        $respuesta = $conexion->actualizar_desde_objeto($pago, self::$_CAMPOS_CLAVE, Constant::$TABLA_PAGO);
        if ($respuesta["exito"]) {
            $respuesta["data"] = self::consultar_pago_por_id($respuesta["codigo"]);
        }
        return $respuesta;
    }

    public static function confirmar_pago($id_pago, $email_pagador, $monto_pagado) {
        $pago= array(
            "id_pago" => $id_pago,
            "confirmado" => Constant::$SI,
            "fecha_confirmacion" => "NOW()",
            "monto_pagado" => $monto_pagado,
            "email_pagador" => $email_pagador,
        );
        return self::actualizar_pago($pago);
    }

}