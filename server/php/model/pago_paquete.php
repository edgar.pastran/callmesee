<?php
/*
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 04/07/2018
 * Time: 04:47 PM
 */

require_once(__DIR__."/../config/connection.php");
require_once(__DIR__."/../config/constant.php");
class Pago_Paquete {

    private static $_CAMPOS_CLAVE = array("id_pago", "id_paquete");

    public static function consultar_pago_paquetes_por_pago($id_pago, $estado=null) {
        $conexion = new Connection();
        $sql =
            "SELECT PP.*, PA.nombre AS _paquete_nombre ".
            "FROM ".Constant::$TABLA_PAGO_PAQUETES." PP, ".Constant::$TABLA_PAQUETE." PA ".
            "WHERE PP.id_paquete = PA.id_paquete ".
            "AND PP.id_pago = ".$id_pago." ";
        $sql .=
            (($estado!=null)?"AND PP.estado = '".$estado."' ":"");
        $sql .=
            "ORDER BY PP.id_paquete ASC";
        $data = $conexion->consulta($sql);
        return $data;
    }

    public static function consultar_pago_paquete_por_id($id_pago, $id_paquete) {
        $conexion = new Connection();
        $sql =
            "SELECT * ".
            "FROM ".Constant::$TABLA_PAGO_PAQUETES." ".
            "WHERE id_pago = '".$id_pago." ".
            "AND id_paquete = '".$id_paquete."'";
        $data = $conexion->consulta($sql);
        return (count($data))?$data[0]:null;
    }

    public static function registrar_pago_paquete($pago_paquete) {
        $conexion = new Connection();
        $response = $conexion->insertar_desde_objeto($pago_paquete, Constant::$TABLA_PAGO_PAQUETES);
        if ($response["exito"]) {
            $response["data"] = self::consultar_pago_paquete_por_id($pago_paquete['id_pago'], $pago_paquete['id_paquete']);
        }
        return $response;
    }

}