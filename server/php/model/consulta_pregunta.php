<?php
/**
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 28/07/2018
 * Time: 08:31 AM
 */

require_once(__DIR__."/../config/connection.php");
require_once(__DIR__."/../config/constant.php");
class Consulta_Pregunta {

    private static $_CAMPOS_CLAVE = array("id_consulta_pregunta");

    public static function consultar_consulta_preguntas_por_consulta($id_consulta) {
        $conexion = new Connection();
        $sql =
            "SELECT * ".
            "FROM ".Constant::$TABLA_CONSULTA_PREGUNTA." ".
            "WHERE id_consulta = ".$id_consulta." ".
            "ORDER BY id_consulta_pregunta ASC";
        return $conexion->consulta($sql);
    }

    private static function consultar_consulta_pregunta_por_id($id) {
        $conexion = new Connection();
        $sql =
            "SELECT * ".
            "FROM ".Constant::$TABLA_CONSULTA_PREGUNTA." ".
            "WHERE id_consulta_pregunta = ".$id;
        $data = $conexion->consulta($sql);
        return (count($data))?$data[0]:null;
    }

    public static function registrar_consulta_pregunta($consulta_pregunta) {
        $conexion = new Connection();
        $respuesta = $conexion->insertar_desde_objeto($consulta_pregunta, Constant::$TABLA_CONSULTA_PREGUNTA);
        if ($respuesta["exito"]) {
            $respuesta["data"] = self::consultar_consulta_pregunta_por_id($respuesta["codigo"]);
        }
        return $respuesta;
    }

}
?>