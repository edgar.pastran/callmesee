<?php
/*
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 04/07/2018
 * Time: 04:47 PM
 */

require_once(__DIR__."/../config/connection.php");
require_once(__DIR__."/../config/constant.php");
class Asesor {

    private static $_CAMPOS_CLAVE = array("id_asesor");

    public static function consultar_asesores($estado=null) {
        $conexion = new Connection();
        $sql =
            "SELECT ASE.*, US.nombres AS _nombres, US.apellidos AS _apellidos, US.telefono AS _telefono, US.imagen AS _imagen ".
            "FROM ".Constant::$TABLA_ASESOR." ASE, ".Constant::$TABLA_USUARIO." US ".
            "WHERE ASE.id_usuario = US.id_usuario ";
        $sql .=
            (($estado!=null)?"AND estado = '".$estado."' ":"");
        $sql .=
            "ORDER BY ASE.id_usuario";

        $data = $conexion->consulta($sql);
        for ($i=0; $i<count($data); $i++) {
            $data[$i]['categorias'] = self::consultar_asesor_categorias($data[$i]['id_asesor']);
        }
        return $data;
    }

    public static function consultar_asesores_disponibles_por_categoria($id_categoria) {
        $conexion = new Connection();
        $sql =
            "SELECT ASE.*, US.nombres AS _nombres, US.apellidos AS _apellidos, US.imagen AS _imagen ".
            "FROM ".Constant::$TABLA_ASESOR." ASE, ".Constant::$TABLA_ASESOR_CATEGORIAS." AC, ".Constant::$TABLA_USUARIO." US ".
            "WHERE ASE.id_asesor = AC.id_asesor ".
            "AND ASE.id_usuario = US.id_usuario ".
            "AND US.estado = '".Constant::$REGISTRO_ACTIVO."' ".
            "AND AC.id_categoria = ".$id_categoria." ".
//            "AND US.fecha_ultima_actividad > DATE_SUB(NOW(), INTERVAL ".Constant::$TIEMPO_MENSAJES_RECIENTES.") ".
            "AND ASE.disponible = '".Constant::$SI."' ".
            "AND ASE.estado = '".Constant::$REGISTRO_ACTIVO."' ".
            "ORDER BY ASE.id_asesor ASC";
        $data = $conexion->consulta($sql);
//        $data["sql"] = $sql;
        return $data;
    }

    public static function consultar_asesor_por_id($id) {
        $conexion = new Connection();
        $sql =
            "SELECT ASE.*, US.nombres AS _nombres, US.apellidos AS _apellidos, US.telefono AS _telefono, US.imagen AS _imagen ".
            "FROM ".Constant::$TABLA_ASESOR." ASE, ".Constant::$TABLA_USUARIO." US ".
            "WHERE ASE.id_usuario = US.id_usuario ".
            "AND ASE.id_asesor = ".$id;
        $data = $conexion->consulta($sql);
        for ($i=0; $i<count($data); $i++) {
            $data[$i]['categorias'] = self::consultar_asesor_categorias($data[$i]['id_asesor']);
        }
        return (count($data))?$data[0]:null;
    }

    public static function consultar_asesor_por_id_usuario($id_usuario) {
        $conexion = new Connection();
        $sql =
            "SELECT * ".
            "FROM ".Constant::$TABLA_ASESOR." ".
            "WHERE id_usuario = '".$id_usuario."'";
        $data = $conexion->consulta($sql);
        return (count($data))?$data[0]:null;
    }

    private static function consultar_asesor_categorias($id_asesor) {
        $conexion = new Connection();
        $sql =
            "SELECT id_categoria ".
            "FROM ".Constant::$TABLA_ASESOR_CATEGORIAS." ".
            "WHERE id_asesor = ".$id_asesor;
        $data = $conexion->consulta($sql);
        $categorias = array();
        foreach ($data as $categoria) {
            array_push($categorias, $categoria['id_categoria']);
        }
        return $categorias;
    }

    public static function registrar_asesor($usuario) {
        $conexion = new Connection();
        $asesor = array(
            "id_usuario" => $usuario->id_usuario
        );
        return $conexion->insertar_desde_objeto($asesor, Constant::$TABLA_ASESOR);
    }

    private static function eliminar_asesor_categorias($id_asesor) {
        $conexion = new Connection();
        $asesor_categoria = array(
            "id_asesor" => $id_asesor
        );
        return $conexion->eliminar_desde_objeto($asesor_categoria, self::$_CAMPOS_CLAVE, Constant::$TABLA_ASESOR_CATEGORIAS);
    }

    private static function registrar_asesor_categoria($id_asesor, $id_categoria) {
        $conexion = new Connection();
        $asesor_categoria = array(
            "id_asesor" => $id_asesor,
            "id_categoria" => $id_categoria
        );
        return $conexion->insertar_desde_objeto($asesor_categoria, Constant::$TABLA_ASESOR_CATEGORIAS);
    }

    public static function registrar_asesor_categorias($id_asesor, $categorias) {
        $respuesta = self::eliminar_asesor_categorias($id_asesor);
        for ($i=0; $i<count($categorias) && $respuesta["exito"]; $i++) {
            $id_categoria = $categorias[$i];
            $respuesta = self::registrar_asesor_categoria($id_asesor, $id_categoria);
        }
        return $respuesta;
    }

    public static function actualizar_asesor($asesor) {
        $conexion = new Connection();
        $respuesta = $conexion->actualizar_desde_objeto($asesor, self::$_CAMPOS_CLAVE, Constant::$TABLA_ASESOR);
        if ($respuesta["exito"]) {
            $respuesta["data"] = self::consultar_asesor_por_id($respuesta["codigo"]);
        }
        return $respuesta;
    }

    private static function actualizar_estado_asesor($id_asesor, $estado) {
        $conexion = new Connection();
        $asesor = array(
            "id_asesor" => $id_asesor,
            "estado" => $estado
        );
        $respuesta = $conexion->actualizar_desde_objeto($asesor, self::$_CAMPOS_CLAVE, Constant::$TABLA_ASESOR);
        if ($respuesta["exito"]) {
            $respuesta["data"] = self::consultar_asesor_por_id($respuesta["codigo"]);
        }
        return $respuesta;
    }

    public static function desactivar_asesor($id_asesor) {
        return self::actualizar_estado_asesor($id_asesor, Constant::$REGISTRO_INACTIVO);
    }

    public static function activar_asesor($id_asesor) {
        return self::actualizar_estado_asesor($id_asesor, Constant::$REGISTRO_ACTIVO);
    }

    private static function actualizar_disponibilidad_asesor($id_asesor, $disponible) {
        $conexion = new Connection();
        $asesor = array(
            "id_asesor" => $id_asesor,
            "disponible" => $disponible
        );
        $respuesta = $conexion->actualizar_desde_objeto($asesor, self::$_CAMPOS_CLAVE, Constant::$TABLA_ASESOR);
        if ($respuesta["exito"]) {
            $respuesta["data"] = self::consultar_asesor_por_id($respuesta["codigo"]);
        }
        return $respuesta;
    }

    public static function colocar_disponible_asesor($id_asesor) {
        return self::actualizar_disponibilidad_asesor($id_asesor, Constant::$SI);
    }

    public static function colocar_no_disponible_asesor($id_asesor) {
        return self::actualizar_disponibilidad_asesor($id_asesor, Constant::$NO);
    }
}