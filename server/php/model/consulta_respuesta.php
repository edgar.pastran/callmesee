<?php
/**
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 28/07/2018
 * Time: 08:31 AM
 */

require_once(__DIR__."/../config/connection.php");
require_once(__DIR__."/../config/constant.php");
class Consulta_Respuesta {

    private static $_CAMPOS_CLAVE = array("id_consulta_pregunta", "id_respuesta");

    public static function consultar_consulta_respuestas_por_consulta_pregunta($id_consulta_pregunta) {
        $conexion = new Connection();
        $sql =
            "SELECT * ".
            "FROM ".Constant::$TABLA_CONSULTA_RESPUESTA." ".
            "WHERE id_consulta_pregunta = ".$id_consulta_pregunta;
        return $conexion->consulta($sql);
    }

    public static function registrar_consulta_respuesta($consulta_respuesta) {
        $conexion = new Connection();
        return $conexion->insertar_desde_objeto($consulta_respuesta, Constant::$TABLA_CONSULTA_RESPUESTA);
    }

}
?>