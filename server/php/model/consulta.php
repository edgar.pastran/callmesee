<?php
/**
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 28/07/2018
 * Time: 08:24 AM
 */

require_once(__DIR__."/../config/connection.php");
require_once(__DIR__."/../config/constant.php");
class Consulta {

    private static $_CAMPOS_CLAVE = array("id_consulta");

    public static function consultar_consulta_por_id($id) {
        $conexion = new Connection();
        $sql =
            "SELECT CO.*, CA.nombre AS _categoria_nombre ".
            "FROM ".Constant::$TABLA_CONSULTA." CO, ".Constant::$TABLA_CATEGORIA." CA ".
            "WHERE CO.id_categoria = CA.id_categoria ".
            "AND CO.id_consulta = ".$id;
        $data = $conexion->consulta($sql);
        return (count($data))?$data[0]:null;
    }

    public static function registrar_consulta($consulta) {
        $conexion = new Connection();
        $respuesta = $conexion->insertar_desde_objeto($consulta, Constant::$TABLA_CONSULTA);
        if ($respuesta["exito"]) {
            $respuesta["data"] = self::consultar_consulta_por_id($respuesta["codigo"]);
        }
        return $respuesta;
    }

    public static function actualizar_consulta($consulta) {
        $conexion = new Connection();
        $respuesta = $conexion->actualizar_desde_objeto($consulta, self::$_CAMPOS_CLAVE, Constant::$TABLA_CONSULTA);
        if ($respuesta["exito"]) {
            $respuesta["data"] = self::consultar_consulta_por_id($respuesta["codigo"]);
        }
        return $respuesta;
    }

    public static function actualizar_inicio_llamada($id_consulta) {
        $consulta = array(
            "id_consulta" => $id_consulta,
            "fecha_inicio_llamada" => "NOW()"
        );
        return self::actualizar_consulta($consulta);
    }

    public static function actualizar_fin_llamada($id_consulta) {
        $consulta = array(
            "id_consulta" => $id_consulta,
            "fecha_fin_llamada" => "NOW()",
            "segundos" => "TIMESTAMPDIFF(SECOND, fecha_inicio_llamada, NOW())"
        );
        return self::actualizar_consulta($consulta);
    }

    private static function actualizar_condicion_consulta($id_consulta, $condicion, $id_sesion=null) {
        $conexion = new Connection();
        $consulta = array(
            "id_consulta" => $id_consulta,
            "condicion" => $condicion,
            "fecha_cambio_condicion" => "NOW()"
        );
        if ($id_sesion != null) {
            $consulta["id_sesion"] = $id_sesion;
        }
        $respuesta = $conexion->actualizar_desde_objeto($consulta, self::$_CAMPOS_CLAVE, Constant::$TABLA_CONSULTA);
        if ($respuesta["exito"]) {
            $respuesta["data"] = self::consultar_consulta_por_id($respuesta["codigo"]);
        }
        return $respuesta;
    }

    public static function rechazar_consulta($id_consulta) {
        return self::actualizar_condicion_consulta($id_consulta, Constant::$CONSULTA_CONDICION_RECHAZADA);
    }

    public static function aceptar_consulta($id_consulta, $id_sesion) {
        return self::actualizar_condicion_consulta($id_consulta, Constant::$CONSULTA_CONDICION_ACEPTADA, $id_sesion);
    }

    public static function expirar_consulta($id_consulta) {
        return self::actualizar_condicion_consulta($id_consulta, Constant::$CONSULTA_CONDICION_ESPERA_EXCEDIDA);
    }
}
?>