<?php
/**
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 28/06/2018
 * Time: 11:22 AM
 */

require_once(__DIR__."/../config/connection.php");
require_once(__DIR__."/../config/constant.php");
class Cliente {

    private static $_CAMPOS_CLAVE = array("id_cliente");

    public static function consultar_clientes($estado=null) {
        $conexion = new Connection();
        $sql =
            "SELECT CL.*, US.nombres AS _nombres, US.apellidos AS _apellidos, US.telefono AS _telefono, US.imagen AS _imagen ".
            "FROM ".Constant::$TABLA_CLIENTE." CL, ".Constant::$TABLA_USUARIO." US ".
            "WHERE CL.id_usuario = US.id_usuario ";
        $sql .=
            (($estado!=null)?"AND estado = '".$estado."' ":"");
        $sql .=
            "ORDER BY CL.id_usuario";

        $data = $conexion->consulta($sql);
        return $data;
    }

    public static function consultar_cliente_por_id($id) {
        $conexion = new Connection();
        $sql =
            "SELECT CL.*, ".
            "US.nombres AS _nombres, US.apellidos AS _apellidos, US.telefono AS _telefono, US.correo AS _correo, US.imagen AS _imagen ".
            "FROM ".Constant::$TABLA_CLIENTE." CL, ".Constant::$TABLA_USUARIO." US ".
            "WHERE CL.id_usuario = US.id_usuario ".
            "AND CL.id_cliente = ".$id;
        $data = $conexion->consulta($sql);
        return (count($data))?$data[0]:null;
    }

    public static function consultar_cliente_por_id_usuario($id_usuario) {
        $conexion = new Connection();
        $sql =
            "SELECT * ".
            "FROM ".Constant::$TABLA_CLIENTE." ".
            "WHERE id_usuario = '".$id_usuario."'";
        $data = $conexion->consulta($sql);
        return (count($data))?$data[0]:null;
    }

    public static function registrar_cliente($usuario) {
        $conexion = new Connection();
        $cliente = array(
            "id_usuario" => $usuario['id_usuario']
        );
        return $conexion->insertar_desde_objeto($cliente, Constant::$TABLA_CLIENTE);
    }

    private static function actualizar_estado_cliente($id_cliente, $estado) {
        $conexion = new Connection();
        $cliente = array(
            "id_cliente" => $id_cliente,
            "estado" => $estado
        );
        $respuesta = $conexion->actualizar_desde_objeto($cliente, self::$_CAMPOS_CLAVE, Constant::$TABLA_CLIENTE);
        if ($respuesta["exito"]) {
            $respuesta["data"] = self::consultar_cliente_por_id($respuesta["codigo"]);
        }
        return $respuesta;
    }

    public static function desactivar_cliente($id_cliente) {
        return self::actualizar_estado_cliente($id_cliente, Constant::$REGISTRO_INACTIVO);
    }

    public static function activar_cliente($id_cliente) {
        return self::actualizar_estado_cliente($id_cliente, Constant::$REGISTRO_ACTIVO);
    }
}
?>