<?php
/**
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 28/06/2018
 * Time: 10:30 AM
 */

require_once(__DIR__."/../config/connection.php");
require_once(__DIR__."/../config/constant.php");
require_once(__DIR__."/../lib/utilities.php");
class Usuario {

    private static $_CAMPOS_CLAVE = array("id_usuario");

    public static function consultar_usuario_por_id_clave($id_usuario, $clave) {
        $conexion = new Connection();
        $sql =
            "SELECT * ".
            "FROM ".Constant::$TABLA_USUARIO." ".
            "WHERE estado = '".Constant::$REGISTRO_ACTIVO."' ".
            "AND id_usuario = '".$id_usuario."' ".
            "AND clave = '".md5($clave)."'";
        $data = $conexion->consulta($sql);
        return (count($data))?$data[0]:null;
    }

    public static function consultar_usuario_por_id($id_usuario, $estado=null) {
        $conexion = new Connection();
        $sql =
            "SELECT * ".
            "FROM ".Constant::$TABLA_USUARIO." ".
            "WHERE id_usuario = '".$id_usuario."'".
            (($estado!=null)?" AND estado = '".$estado."'":"");
        $data = $conexion->consulta($sql);
        return (count($data))?$data[0]:null;
    }

    public static function consultar_usuario_por_id_codigo_verificacion($id_usuario, $codigo_verificacion) {
        $conexion = new Connection();
        $sql =
            "SELECT id_usuario, clave, correo, nombres, apellidos, id_rol ".
            "FROM ".Constant::$TABLA_USUARIO." ".
            "WHERE id_usuario = '".$id_usuario."' ".
            "AND codigo_verificacion = '".$codigo_verificacion."' ".
            "AND estado = '".Constant::$REGISTRO_POR_VERIFICAR."'";
        $data = $conexion->consulta($sql);
        return (count($data))?$data[0]:null;
    }

    public static function registrar_usuario_rol_cliente($usuario) {
        $conexion = new Connection();
        $usuario->id_rol = Constant::$ROL_CLIENTE;
        $usuario->codigo_verificacion = Utilities::random_word(Constant::$LONGITUD_CODIGO_VERIFICACION);
        $usuario->estado = Constant::$REGISTRO_POR_VERIFICAR;
        // Registro del usuario
        return $conexion->insertar_desde_objeto($usuario, Constant::$TABLA_USUARIO);
    }

    public static function registrar_usuario_rol_asesor($usuario) {
        $conexion = new Connection();
        $usuario->id_rol = Constant::$ROL_ASESOR;
        $usuario->clave = Utilities::random_word(Constant::$LONGITUD_CODIGO_VERIFICACION);
        $usuario->codigo_verificacion = Utilities::random_word(Constant::$LONGITUD_CODIGO_VERIFICACION);
        $usuario->estado = Constant::$REGISTRO_POR_VERIFICAR;
        // Registro del usuario
        return $conexion->insertar_desde_objeto($usuario, Constant::$TABLA_USUARIO);
    }

    public static function verificar_usuario($usuario) {
        $conexion = new Connection();
        // Conversion de la clave
        $usuario['clave'] = md5($usuario['clave']);
        $usuario['codigo_verificacion'] = null;
        $usuario['estado'] = Constant::$REGISTRO_ACTIVO;
        // Actualizacion del usuario
        return $conexion->actualizar_desde_objeto($usuario, self::$_CAMPOS_CLAVE, Constant::$TABLA_USUARIO);
    }

    public static function resetear_clave($usuario) {
        $conexion = new Connection();
        $clave_sin_codificar = Utilities::random_word(6);
        // Conversion de la clave
        $usuario['clave'] = md5($clave_sin_codificar);
        $respuesta = $conexion->actualizar_desde_objeto($usuario, self::$_CAMPOS_CLAVE, Constant::$TABLA_USUARIO);
        if ($respuesta['exito']) {
            $respuesta['data']['clave_sin_codificar'] = $clave_sin_codificar;
        }
        return $respuesta;
    }

    public static function actualizar_codigo_verificacion($id_usuario) {
        $conexion = new Connection();
        $codigo_verificacion = Utilities::random_word(Constant::$LONGITUD_CODIGO_VERIFICACION);
        $usuario = array(
            "id_usuario" => $id_usuario,
            "codigo_verificacion" => $codigo_verificacion
        );
        // Actualizacion del usuario
        return $conexion->actualizar_desde_objeto($usuario, self::$_CAMPOS_CLAVE, Constant::$TABLA_USUARIO);
    }

    public static function actualizar_fecha_ultima_actividad($id_usuario) {
        $conexion = new Connection();
        $sql =
            "UPDATE ".Constant::$TABLA_USUARIO." SET ".
            "fecha_ultima_actividad = NOW() ".
            "WHERE id_usuario = '".$id_usuario."'";
        return $conexion->sentencia($sql);
    }

    public static function actualizar_clave($id_usuario, $clave) {
        $conexion = new Connection();
        $usuario = array(
            "id_usuario" => $id_usuario,
            "clave" => md5($clave)
        );
        // Actualizar el campo clave del usuario
        return $conexion->actualizar_desde_objeto($usuario, self::$_CAMPOS_CLAVE, Constant::$TABLA_USUARIO);
    }

    public static function actualizar_imagen($id_usuario, $nombre_imagen) {
        $conexion = new Connection();
        $usuario = array(
            "id_usuario" => $id_usuario,
            "imagen" => $nombre_imagen
        );
        // Actualizar el campo imagen del usuario
        return $conexion->actualizar_desde_objeto($usuario, self::$_CAMPOS_CLAVE, Constant::$TABLA_USUARIO);
    }

    public static function actualizar_usuario($usuario) {
        $conexion = new Connection();
        return $conexion->actualizar_desde_objeto($usuario, self::$_CAMPOS_CLAVE, Constant::$TABLA_USUARIO);
    }

    private static function actualizar_estado_usuario($id_usuario, $estado) {
        $conexion = new Connection();
        $usuario = array(
            "id_usuario" => $id_usuario,
            "estado" => $estado
        );
        return $conexion->actualizar_desde_objeto($usuario, self::$_CAMPOS_CLAVE, Constant::$TABLA_USUARIO);
    }

    public static function desactivar_usuario($id_usuario) {
        return self::actualizar_estado_usuario($id_usuario, Constant::$REGISTRO_INACTIVO);
    }

    public static function activar_usuario($id_usuario) {
        return self::actualizar_estado_usuario($id_usuario, Constant::$REGISTRO_ACTIVO);
    }
}
?>