<?php
/*
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 04/07/2018
 * Time: 04:47 PM
 */

require_once(__DIR__."/../config/connection.php");
require_once(__DIR__."/../config/constant.php");
class Respuesta {

    private static $_CAMPOS_CLAVE = array("id_respuesta");

    public static function consultar_respuestas_por_pregunta($id_pregunta, $estado=null) {
        $conexion = new Connection();
        $sql =
            "SELECT * ".
            "FROM ".Constant::$TABLA_RESPUESTA." ".
            "WHERE id_pregunta = '".$id_pregunta."' ";
        $sql .=
            (($estado!=null)?"AND estado = '".$estado."' ":"");
        $sql .=
            "ORDER BY orden ASC";
        $data = $conexion->consulta($sql);
        return $data;
    }

    public static function consultar_respuesta_por_id($id) {
        $conexion = new Connection();
        $sql =
            "SELECT * ".
            "FROM ".Constant::$TABLA_RESPUESTA." ".
            "WHERE id_respuesta = ".$id;
        $data = $conexion->consulta($sql);
        return (count($data))?$data[0]:null;
    }

    private static function proximo_orden_por_pregunta($id_pregunta) {
        $conexion = new Connection();
        $sql =
            "SELECT MAX(orden) AS maximo ".
            "FROM ".Constant::$TABLA_RESPUESTA." ".
            "WHERE id_pregunta = ".$id_pregunta;
        $data = $conexion->consulta($sql);
        return (count($data))?($data[0]['maximo']+1):1;
    }

    public static function registrar_respuesta($respuesta) {
        $id_pregunta = $respuesta["id_pregunta"];
        $respuesta["orden"] = self::proximo_orden_por_pregunta($id_pregunta);
        $conexion = new Connection();
        $response = $conexion->insertar_desde_objeto($respuesta, Constant::$TABLA_RESPUESTA);
        if ($response["exito"]) {
            $response["data"] = self::consultar_respuesta_por_id($response["codigo"]);
        }
        return $response;
    }

    public static function actualizar_respuesta($respuesta) {
        $conexion = new Connection();
        $response = $conexion->actualizar_desde_objeto($respuesta, self::$_CAMPOS_CLAVE, Constant::$TABLA_RESPUESTA);
        if ($response["exito"]) {
            $response["data"] = self::consultar_respuesta_por_id($response["codigo"]);
        }
        return $response;
    }

    private static function actualizar_estado_respuesta($id_respuesta, $estado) {
        $conexion = new Connection();
        $respuesta = array(
            "id_respuesta" => $id_respuesta,
            "estado" => $estado
        );
        $response = $conexion->actualizar_desde_objeto($respuesta, self::$_CAMPOS_CLAVE, Constant::$TABLA_RESPUESTA);
        if ($response["exito"]) {
            $response["data"] = self::consultar_respuesta_por_id($response["codigo"]);
        }
        return $response;
    }

    public static function desactivar_respuesta($id_respuesta) {
        return self::actualizar_estado_respuesta($id_respuesta, Constant::$REGISTRO_INACTIVO);
    }

    public static function activar_respuesta($id_respuesta) {
        return self::actualizar_estado_respuesta($id_respuesta, Constant::$REGISTRO_ACTIVO);
    }
}