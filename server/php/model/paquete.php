<?php
/*
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 04/07/2018
 * Time: 04:47 PM
 */

require_once(__DIR__."/../config/connection.php");
require_once(__DIR__."/../config/constant.php");
class Paquete {

    private static $_CAMPOS_CLAVE = array("id_paquete");

    public static function consultar_paquetes($estado=null) {
        $conexion = new Connection();
        $sql =
            "SELECT PA.*, CA.nombre AS _categoria_nombre ".
            "FROM ".Constant::$TABLA_PAQUETE." PA, ".Constant::$TABLA_CATEGORIA." CA ".
            "WHERE PA.id_categoria = CA.id_categoria ";
        $sql .=
            (($estado!=null)?"AND PA.estado = '".$estado."'":"");
        $sql .=
            "ORDER BY PA.id_categoria ASC";
        $data = $conexion->consulta($sql);
        return $data;
    }

    public static function consultar_paquete_por_id($id, $estado=null) {
        $conexion = new Connection();
        $sql =
            "SELECT PA.*, CA.nombre AS _categoria_nombre ".
            "FROM ".Constant::$TABLA_PAQUETE." PA, ".Constant::$TABLA_CATEGORIA." CA ".
            "WHERE PA.id_categoria = CA.id_categoria ".
            "AND PA.id_paquete = ".$id;
        $sql .=
            (($estado!=null)?" AND PA.estado = '".$estado."'":"");
        $data = $conexion->consulta($sql);
        return (count($data))?$data[0]:null;
    }

    public static function registrar_paquete($paquete) {
        $conexion = new Connection();
        $respuesta = $conexion->insertar_desde_objeto($paquete, Constant::$TABLA_PAQUETE);
        if ($respuesta["exito"]) {
            $respuesta["data"] = self::consultar_paquete_por_id($respuesta["codigo"]);
        }
        return $respuesta;
    }

    public static function actualizar_paquete($paquete) {
        $conexion = new Connection();
        $respuesta = $conexion->actualizar_desde_objeto($paquete, self::$_CAMPOS_CLAVE, Constant::$TABLA_PAQUETE);
        if ($respuesta["exito"]) {
            $respuesta["data"] = self::consultar_paquete_por_id($respuesta["codigo"]);
        }
        return $respuesta;
    }

    private static function actualizar_estado_paquete($id_paquete, $estado) {
        $conexion = new Connection();
        $pregunta = array(
            "id_paquete" => $id_paquete,
            "estado" => $estado
        );
        $respuesta = $conexion->actualizar_desde_objeto($pregunta, self::$_CAMPOS_CLAVE, Constant::$TABLA_PAQUETE);
        if ($respuesta["exito"]) {
            $respuesta["data"] = self::consultar_paquete_por_id($respuesta["codigo"]);
        }
        return $respuesta;
    }

    public static function desactivar_paquete($id_paquete) {
        return self::actualizar_estado_paquete($id_paquete, Constant::$REGISTRO_INACTIVO);
    }

    public static function activar_paquete($id_paquete) {
        return self::actualizar_estado_paquete($id_paquete, Constant::$REGISTRO_ACTIVO);
    }

}