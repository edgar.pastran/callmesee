<?php
/**
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 28/06/2018
 * Time: 12:18 PM
 */

require_once(__DIR__."/../config/connection.php");
require_once(__DIR__."/../config/constant.php");
class Mensaje {

    public static function consultar_mensajes_por_usuario($id_usuario, $ultimo_mensaje=null) {
        $conexion = new Connection();
        $sql =
            "SELECT ME.*, US.imagen AS _imagen, US.nombres AS _nombres, US.apellidos AS _apellidos, CO.condicion AS _consulta_condicion ".
            "FROM ".Constant::$TABLA_MENSAJE." ME LEFT JOIN CONSULTA CO ON ME.id_consulta = CO.id_consulta, ".
            Constant::$TABLA_USUARIO." US ".
            "WHERE ME.id_usuario_remitente = US.id_usuario ".
            "AND ME.estado = '".Constant::$REGISTRO_ACTIVO."' ".
            "AND ME.id_usuario_destinatario = '".$id_usuario."' ".
            (($ultimo_mensaje != null)?"AND ME.id_mensaje < ".$ultimo_mensaje." ":"").
            "ORDER BY ME.id_mensaje DESC ".
            "LIMIT ".Constant::$CANTIDAD_MENSAJES_MOSTRADOS;
        return $conexion->consulta($sql);
    }

    public static function consultar_id_primer_mensaje_por_usuario($id_usuario) {
        $conexion = new Connection();
        $sql =
            "SELECT MIN(M.id_mensaje) AS primer_mensaje ".
            "FROM ".Constant::$TABLA_MENSAJE." M ".
            "WHERE M.estado = '".Constant::$REGISTRO_ACTIVO."' ".
            "AND M.id_usuario_destinatario = '".$id_usuario."'";
        $data = $conexion->consulta($sql);
        return (count($data))?$data[0]['primer_mensaje']:-1;
    }

    public static function consultar_mensajes_no_leidos_recientes_por_usuario($id_usuario) {
        $conexion = new Connection();
        $sql =
            "SELECT ME.id_mensaje, ME.titulo, US.imagen AS _imagen, US.nombres AS _nombres, US.apellidos AS _apellidos ".
            "FROM ".Constant::$TABLA_MENSAJE." ME, ".Constant::$TABLA_USUARIO." US ".
            "WHERE ME.id_usuario_remitente = US.id_usuario ".
            "AND ME.leido = '".Constant::$NO."' ".
            "AND ME.estado = '".Constant::$REGISTRO_ACTIVO."' ".
            "AND ME.id_usuario_destinatario = '".$id_usuario."' ".
            "AND ME.fecha_creacion > DATE_SUB(NOW(), INTERVAL ".Constant::$TIEMPO_MENSAJES_RECIENTES.") ".
            "ORDER BY ME.id_mensaje DESC";
        return $mensajes_recientes = $conexion->consulta($sql);
    }

    public static function consultar_total_mensajes_no_leidos_por_usuario($id_usuario) {
        $conexion = new Connection();
        $sql =
            "SELECT COUNT(*) AS total ".
            "FROM ".Constant::$TABLA_MENSAJE." M ".
            "WHERE M.leido = '".Constant::$NO."' ".
            "AND M.estado = '".Constant::$REGISTRO_ACTIVO."' ".
            "AND M.id_usuario_destinatario = '".$id_usuario."'";
        $data = $conexion->consulta($sql);
        return (count($data))?$data[0]['total']:0;
    }

    public static function registrar_mensaje($mensaje) {
        $conexion = new Connection();
        return $conexion->insertar_desde_objeto($mensaje, Constant::$TABLA_MENSAJE);
    }

    public static function marcar_mensaje_leido($id_mensaje) {
        $conexion = new Connection();
        $sql =
            "UPDATE " . Constant::$TABLA_MENSAJE . " SET " .
            "leido = '" . Constant::$SI . "', " .
            "fecha_lectura = NOW() " .
            "WHERE id_mensaje = " . $id_mensaje;
        return $conexion->sentencia($sql);
    }
}
?>