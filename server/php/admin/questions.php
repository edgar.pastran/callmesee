<?php //
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

$respuesta = array( 'exito' => false, 'mensaje' => '');
try {
    if (isset($_POST)) {
        if (isset($_POST['operacion'])) {
            require_once ("../service/admin_questions.php");

            $servicio = new Admin_Questions();
            $operacion = $_POST['operacion'];

            if ($operacion == "consultar") {
                $respuesta = $servicio->consultar();
            }
            else if ($operacion == "consultar_categorias") {
                $respuesta = $servicio->consultar_categorias();
            }
            else if ($operacion == "consultar_respuestas" && isset($_POST['id_pregunta'])) {
                $id_pregunta = $_POST['id_pregunta'];
                $respuesta = $servicio->consultar_respuestas($id_pregunta);
            }
            else if ($operacion == "nuevo" && isset($_POST['pregunta'])) {
                $pregunta = $_POST['pregunta'];
                $respuesta = $servicio->nuevo($pregunta);
            }
            else if ($operacion == "nueva_respuesta" && isset($_POST['respuesta'])) {
                $response = $_POST['respuesta'];
                $respuesta = $servicio->nueva_respuesta($response);
            }
            else if ($operacion == "editar" && isset($_POST['pregunta'])) {
                $pregunta = $_POST['pregunta'];
                $respuesta = $servicio->editar($pregunta);
            }
            else if ($operacion == "editar_respuesta" && isset($_POST['respuesta'])) {
                $response = $_POST['respuesta'];
                $respuesta = $servicio->editar_respuesta($response);
            }
            else if ($operacion == "desactivar" && isset($_POST['id_pregunta'])) {
                $id_pregunta = $_POST['id_pregunta'];
                $respuesta = $servicio->desactivar($id_pregunta);
            }
            else if ($operacion == "desactivar_respuesta" && isset($_POST['id_respuesta'])) {
                $id_respuesta = $_POST['id_respuesta'];
                $respuesta = $servicio->desactivar_respuesta($id_respuesta);
            }
            else if ($operacion == "activar" && isset($_POST['id_pregunta'])) {
                $id_pregunta = $_POST['id_pregunta'];
                $respuesta = $servicio->activar($id_pregunta);
            }
            else if ($operacion == "activar_respuesta" && isset($_POST['id_respuesta'])) {
                $id_respuesta = $_POST['id_respuesta'];
                $respuesta = $servicio->activar_respuesta($id_respuesta);
            }
            else {
                $respuesta['mensaje'] = 'No se indicaron todos los parametros necesarios.';
            }
        }
        else {
            $respuesta['mensaje'] = 'Es necesario indicar la operacion.';
        }
    }
    else {
        $respuesta['mensaje'] = 'Solo se permiten parametros mediante el metodo POST.';
    }
}
catch(Exception $e) {
    $respuesta['mensaje'] = $e->getMessage();
}
echo json_encode($respuesta, true);
?>
