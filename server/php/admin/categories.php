<?php //
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

$respuesta = array( 'exito' => false, 'mensaje' => '');
try {
    if (isset($_POST)) {
        if (isset($_POST['operacion'])) {
            require_once ("../service/admin_categories.php");

            $servicio = new Admin_Categories();
            $operacion = $_POST['operacion'];

            if ($operacion == "consultar") {
                $respuesta = $servicio->consultar();
            }
            else if ($operacion == "nuevo" && isset($_POST['categoria'])) {
                $categoria = $_POST['categoria'];
                $respuesta = $servicio->nuevo($categoria);
            }
            else if ($operacion == "editar" && isset($_POST['categoria'])) {
                $categoria = $_POST['categoria'];
                $respuesta = $servicio->editar($categoria);
            }
            else if ($operacion == "desactivar" && isset($_POST['id_categoria'])) {
                $id_categoria = $_POST['id_categoria'];
                $respuesta = $servicio->desactivar($id_categoria);
            }
            else if ($operacion == "activar" && isset($_POST['id_categoria'])) {
                $id_categoria = $_POST['id_categoria'];
                $respuesta = $servicio->desactivar($id_categoria);
            }
            else {
                $respuesta['mensaje'] = 'No se indicaron todos los parametros necesarios.';
            }
        }
        else {
            $respuesta['mensaje'] = 'Es necesario indicar la operacion.';
        }
    }
    else {
        $respuesta['mensaje'] = 'Solo se permiten parametros mediante el metodo POST.';
    }
}
catch(Exception $e) {
    $respuesta['mensaje'] = $e->getMessage();
}
echo json_encode($respuesta, true);
?>
