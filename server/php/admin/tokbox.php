<?php //
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

$respuesta = array( 'exito' => false, 'mensaje' => '');
try {
    if (isset($_POST)) {
        if (isset($_POST['operacion'])) {
            require_once ("../service/admin_tokbox.php");

            $servicio = new Admin_TokBox();
            $operacion = $_POST['operacion'];

            if ($operacion == "consultar") {
                $respuesta = $servicio->consultar();
            }
            else if ($operacion == "guardar" && isset($_POST['configuraciones'])) {
                $configuraciones = $_POST['configuraciones'];
                $respuesta = $servicio->guardar($configuraciones);
            }
            else {
                $respuesta['mensaje'] = 'No se indicaron todos los parametros necesarios.';
            }
        }
        else {
            $respuesta['mensaje'] = 'Es necesario indicar la operacion.';
        }
    }
    else {
        $respuesta['mensaje'] = 'Solo se permiten parametros mediante el metodo POST.';
    }
}
catch(Exception $e) {
    $respuesta['mensaje'] = $e->getMessage();
}
echo json_encode($respuesta, true);
?>
