<?php //
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

$respuesta = array( 'exito' => false, 'mensaje' => '');
try {
    if (isset($_POST)) {
        if (isset($_POST['operacion'])) {
            require_once ("../service/admin_advisers.php");

            $servicio = new Admin_Advisers();
            $operacion = $_POST['operacion'];

            if ($operacion == "consultar") {
                $respuesta = $servicio->consultar();
            }
            else if ($operacion == "consultar_categorias") {
                $respuesta = $servicio->consultar_categorias();
            }
            else if ($operacion == "nuevo" && isset($_POST['asesor'])) {
                $asesor = json_decode($_POST['asesor']);
                $respuesta = $servicio->nuevo($asesor);
            }
            else if ($operacion == "editar" && isset($_POST['asesor'])) {
                $asesor = json_decode($_POST['asesor']);
                $respuesta = $servicio->editar($asesor);
            }
            else if ($operacion == "remover_imagen"   && isset($_POST['asesor'])) {
                $asesor = json_decode($_POST['asesor']);
                $respuesta = $servicio->remover_imagen($asesor);
            }
            else if ($operacion == "desactivar" && isset($_POST['id_asesor'])) {
                $id_asesor = $_POST['id_asesor'];
                $respuesta = $servicio->desactivar($id_asesor);
            }
            else if ($operacion == "activar" && isset($_POST['id_asesor'])) {
                $id_asesor = $_POST['id_asesor'];
                $respuesta = $servicio->activar($id_asesor);
            }
            else {
                $respuesta['mensaje'] = 'No se indicaron todos los parametros necesarios.';
            }
        }
        else {
            $respuesta['mensaje'] = 'Es necesario indicar la operacion.';
        }
    }
    else {
        $respuesta['mensaje'] = 'Solo se permiten parametros mediante el metodo POST.';
    }
}
catch(Exception $e) {
    $respuesta['mensaje'] = $e->getMessage();
}
echo json_encode($respuesta, true);
?>
