<?php //
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

$respuesta = array( 'exito' => false, 'mensaje' => '');
try {
    if (isset($_POST)) {
        if (isset($_POST['operacion'])) {
            require_once ("../service/admin_customers.php");

            $servicio = new Admin_Customers();
            $operacion = $_POST['operacion'];

            if ($operacion == "consultar") {
                $respuesta = $servicio->consultar();
            }
            /*
            else if ($operacion == "nuevo" && isset($_POST['cliente'])) {
                $cliente = json_decode($_POST['cliente']);
                $respuesta - $servicio->nuevo($cliente);
            }
            else if ($operacion == "editar" && isset($_POST['cliente'])) {
                $cliente = json_decode($_POST['cliente']);
                $respuesta = $servicio->editar($cliente);
            }
            else if ($operacion == "remover_imagen"   && isset($_POST['cliente'])) {
                $cliente = json_decode($_POST['cliente']);
                $respuesta = $servicio->remover_imagen($cliente);
            }
            */
            else if ($operacion == "desactivar" && isset($_POST['id_cliente'])) {
                $id_cliente = $_POST['id_cliente'];
                $respuesta = $servicio->desactivar($id_cliente);
            }
            else if ($operacion == "activar" && isset($_POST['id_cliente'])) {
                $id_cliente = $_POST['id_cliente'];
                $respuesta = $servicio->activar($id_cliente);
            }
            else {
                $respuesta['mensaje'] = 'No se indicaron todos los parametros necesarios.';
            }
        }
        else {
            $respuesta['mensaje'] = 'Es necesario indicar la operacion.';
        }
    }
    else {
        $respuesta['mensaje'] = 'Solo se permiten parametros mediante el metodo POST.';
    }
}
catch(Exception $e) {
    $respuesta['mensaje'] = $e->getMessage();
}
echo json_encode($respuesta, true);
?>
