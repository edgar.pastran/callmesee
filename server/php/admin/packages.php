<?php //
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

$respuesta = array( 'exito' => false, 'mensaje' => '');
try {
    if (isset($_POST)) {
        if (isset($_POST['operacion'])) {
            require_once ("../service/admin_packages.php");

            $servicio = new Admin_Packages();
            $operacion = $_POST['operacion'];

            if ($operacion == "consultar") {
                $respuesta = $servicio->consultar();
            }
            else if ($operacion == "consultar_categorias_tipos") {
                $respuesta = $servicio->consultar_categorias_tipos();
            }
            else if ($operacion == "nuevo" && isset($_POST['paquete'])) {
                $paquete = $_POST['paquete'];
                $respuesta = $servicio->nuevo($paquete);
            }
            else if ($operacion == "editar" && isset($_POST['paquete'])) {
                $paquete = $_POST['paquete'];
                $respuesta = $servicio->editar($paquete);
            }
            else if ($operacion == "desactivar" && isset($_POST['id_paquete'])) {
                $id_paquete = $_POST['id_paquete'];
                $respuesta = $servicio->desactivar($id_paquete);
            }
            else if ($operacion == "activar" && isset($_POST['id_paquete'])) {
                $id_paquete = $_POST['id_paquete'];
                $respuesta = $servicio->activar($id_paquete);
            }
            else {
                $respuesta['mensaje'] = 'No se indicaron todos los parametros necesarios.';
            }
        }
        else {
            $respuesta['mensaje'] = 'Es necesario indicar la operacion.';
        }
    }
    else {
        $respuesta['mensaje'] = 'Solo se permiten parametros mediante el metodo POST.';
    }
}
catch(Exception $e) {
    $respuesta['mensaje'] = $e->getMessage();
}
echo json_encode($respuesta, true);
?>
