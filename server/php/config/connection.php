<?php

class Connection {

    private $DB_HOST  = 'localhost';
    private $DB_NAME  = 'callmesee';
    private $USERNAME = 'callmesee_admin';
    private $PASSWORD = '@F~kqMK[Imx4';
    private $conexion = null;

    private function abrir_conexion() {
        $prefix = ((strpos($_SERVER['HTTP_HOST'], "pablotest") !== false)?"pablorada_":"");
        $this->conexion = new mysqli($this->DB_HOST, $prefix.$this->USERNAME, $this->PASSWORD, $prefix.$this->DB_NAME);
        //chequear conexion
        if ($this->conexion->connect_errno) {
            die ("Connection failed: " .$this->conexion->connect_error);
        }
        $this->conexion->query("SET NAMES 'utf8'");
        return true;
    }

    private function cerrar_conexion() {
        // cerrar conexión de base de datos
        mysqli_close($this->conexion);
    }

    public function consulta($query) {
        $registros = array();
        if ($this->abrir_conexion()) {
            $resultado = $this->conexion->query($query);
            if ($resultado) {
                while($fila = $resultado->fetch_assoc()) {
                    array_push($registros, $fila);
                }
            }
            $this->cerrar_conexion();
        }
        return $registros;
    }

    public function sentencia($sentencia) {
        $respuesta = array(
            "exito" => false,
            "mensaje" => "",
            "codigo" => -1
        );
        if ($this->abrir_conexion()) {
            $resultado = $this->conexion->query($sentencia);
            if ($resultado === TRUE) {
                $respuesta["exito"] = true;
                $respuesta["mensaje"] = "La sentencia se ejecuto satisfactoriamente";
                $tipo = strtoupper(substr(trim($sentencia), 0, 6));
                if ($tipo == "INSERT") {
                    $respuesta["codigo"] = $this->conexion->insert_id;//mysqli_insert_id($this->conexion);
                }
            }
            else {
                $codigo = $this->conexion->error;
                $respuesta["mensaje"] = "Problemas al ejecutar la sentencia (".$codigo.")";
                $respuesta["codigo"] = $codigo;
            }
            $this->cerrar_conexion();
        }
        else {
            $respuesta["mensaje"] = "Problemas al conectarse a la base de datos";
        }
        return $respuesta;
    }

    public function insertar_desde_objeto($objeto, $tabla) {
        $sql = "INSERT INTO ".$tabla;
        $campos = " (";
        $valores = " (";
        foreach($objeto as $campo => $valor) {
            if ((!is_array($valor)) && (substr( $campo, 0, 1 ) !== "_")) {
                $campos .= $campo.",";
                if ($valor == null) {
                    $valores .= "NULL,";
                }
                else if (strpos(strtoupper($valor), "NOW()") !== false) {
                    $valores .= $valor.",";
                }
                else {
                    $valores .= "'".$valor."',";
                }
            }
        }
        $campos = trim($campos, ',').")";
        $valores = trim($valores, ',').")";
        $sql .= $campos." VALUES".$valores;
        $respuesta = $this->sentencia($sql);
        if ($respuesta["exito"]) {
            $respuesta["data"] = $objeto;
        }
        return $respuesta;
    }

    public function actualizar_desde_objeto($objeto, $campos_clave, $tabla) {
        $sql = "UPDATE ".$tabla." SET";
        $campos = "";
        $clave  = " WHERE";
        $codigo = array();
        foreach($objeto as $campo => $valor) {
            if ((!is_array($valor)) && (substr( $campo, 0, 1 ) !== "_")) {
                if (in_array($campo, $campos_clave)) {
                    $clave .= " ".$campo."='".$valor."' AND";
                    array_push($codigo, $valor);
                }
                else if ($valor == null) {
                    $campos .= " ".$campo."=NULL,";
                }
                else if (strpos(strtoupper($valor), "NOW()") !== false) {
                    $campos .= " ".$campo."=".$valor.",";
                }
                else {
                    $campos .= " ".$campo."='".$valor."',";
                }
            }
        }
        $campos = trim($campos, ',');
        $clave = trim($clave, 'AND');
        $sql .= $campos.$clave;
        $respuesta = $this->sentencia($sql);
        $respuesta["codigo"] = (count($codigo) > 1)?$codigo:$codigo[0];
        return $respuesta;
    }

    public function eliminar_desde_objeto($objeto, $campos_clave, $tabla) {
        $sql = "DELETE FROM ".$tabla;
        $clave  = " WHERE";
        foreach($objeto as $campo => $valor) {
            if (!is_array($valor)) {
                if (in_array($campo, $campos_clave)) {
                    $clave .= " ".$campo."='".$valor."' AND";
                }
            }
        }
        $clave = trim($clave, 'AND');
        $sql .= $clave;
        return $this->sentencia($sql);
    }
}
?>