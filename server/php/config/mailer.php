<?php
/**
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 22/06/2018
 * Time: 08:19 PM
 */

require_once(__DIR__."/constant.php");
require_once(__DIR__."/../lib/mailSender.php");
class Mailer {

    public static function send_verification_mail($user_email, $url_verification, $user_verification_code, $user_firstname, $user_lastname) {
        $subject = "Verificacion de Registro en ".Constant::$APLICACION_NOMBRE;
        $url_verification .= "?email=".$user_email."&code=".$user_verification_code;
        $message =
            "<p>Hola ".$user_firstname." ".$user_lastname.".</p>".
            "<p>Hemos recibido una solicitud de registro asociada a este correo en ".Constant::$APLICACION_NOMBRE.".<br/>".
            "Necesitamos verificar que fuiste tu quien hizo esta solictud.</p>".
            "<p>Para verificarlo solo tienes que hacer click en el siguiente enlace : </p>".
            "<hr/>".
            "<p><a href='".$url_verification."' target='_blank'>Verificacion en ".Constant::$APLICACION_NOMBRE."</a><p/>".
            "<hr/>".
            "<p>Luego que realices la verificacion podras ingresar con tu usuario en ".
            "<a href='".Constant::$APLICACION_DOMINIO."' target='_blank'>".Constant::$APLICACION_DOMINIO."</a>".
            "</p>".
            "<p>Gracias, <br/> Equipo de ".Constant::$APLICACION_NOMBRE."</p>";
        $from_name = Constant::$REMITENTE_NOMBRE;
        $from_email = Constant::$REMITENTE_EMAIL;
        MailSender::send_mail($user_email, $subject, $message, $from_name, $from_email);
    }

    public static function send_registration_mail($user_email, $user_password, $user_firstname, $user_lastname) {
        $subject = "Registro Completado en ".Constant::$APLICACION_NOMBRE;
        $message =
            "<p>Hola ".$user_firstname." ".$user_lastname.".</p>".
            "<p>Felicitaciones, te has registrado con exito en ".Constant::$APLICACION_NOMBRE.".<br/>".
            "A continuaci&oacute;n se encuentran los detalles de tu cuenta : </p>".
            "<hr/>".
            "<p>Correo: ".$user_email."<p/>".
            "<p>Clave: ".$user_password."</p>".
            "<hr/>".
            "<p>Cuando lo desees puedes ingresar con tu usuario en ".
            "<a href='".Constant::$APLICACION_DOMINIO."' target='_blank'>".Constant::$APLICACION_DOMINIO."</a>".
            "</p>".
            "<p>Gracias, <br/> Equipo de ".Constant::$APLICACION_NOMBRE."</p>";
        $from_name = Constant::$REMITENTE_NOMBRE;
        $from_email = Constant::$REMITENTE_EMAIL;
        MailSender::send_mail($user_email, $subject, $message, $from_name, $from_email);
    }

    public static function send_recover_password_mail($user_email, $user_password, $user_firstname, $user_lastname) {
        $subject = "Cambio de Clave en ".Constant::$APLICACION_NOMBRE;
        $message =
            "<p>Hola ".$user_firstname." ".$user_lastname.".</p>".
            "<p>Tal como lo solicitaste, hemos cambiado tu clave en ".Constant::$APLICACION_NOMBRE.".<br/>".
            "A continuaci&oacute;n se encuentran los nuevos detalles de tu cuenta : </p>".
            "<hr/>".
            "<p>Correo: ".$user_email."<p/>".
            "<p>Clave: ".$user_password."</p>".
            "<hr/>".
            "<p>Cuando lo desees puedes ingresar con tu usuario en ".
            "<a href='".Constant::$APLICACION_DOMINIO."' target='_blank'>".Constant::$APLICACION_DOMINIO."</a>".
            "</p>".
            "<p>Gracias, <br/> Equipo de ".Constant::$APLICACION_NOMBRE."</p>";
        $from_name = Constant::$REMITENTE_NOMBRE;
        $from_email = Constant::$REMITENTE_EMAIL;
        MailSender::send_mail($user_email, $subject, $message, $from_name, $from_email);
    }

    public static function send_confirmed_purchase_mail($user_email, $user_firstname, $user_lastname, $medio_pago, $id_referencia, $monto, $pago_paquetes) {
        $subject = "Detalles de Compra en ".Constant::$APLICACION_NOMBRE;
        $message =
            "<p>Hola ".$user_firstname." ".$user_lastname.".</p>".
            "<p>Felicitaciones, tu compra en ".Constant::$APLICACION_NOMBRE." ha sido exitosa.<br/>".
            "A continuaci&oacute;n se encuentran los detalles de tu compra : </p>".
            "<hr/>".
            "<p>".
            "<li>Medio de Pago: ".$medio_pago."<br/>".
            "<li>Monto: $ ".number_format($monto, 2, ',', '.')."<br/>".
            "<li>Referencia: ".$id_referencia."<br/>".
            "<li>Paquete(s):";
            foreach ($pago_paquetes as $pago_paquete) {
                $message .=
                    "<ul>".
                    "<b>".$pago_paquete['_paquete_nombre'].":</b> ".$pago_paquete['registrados']." Unidad(es)<br/>".
                    "Cada unidad se caracteriza por:<br/>".
                    "<li>".$pago_paquete['llamadas']." llamadas<br/>".
                    "<li>".$pago_paquete['minutos']." minutos por llamada<br/>".
                    "<li>Valido ".(($pago_paquete['dias'] == null)?"hasta su uso":"por ".$pago_paquete['dias']." dias")."<br/>".
                    "<li>Por un valor de $ ".number_format($pago_paquete['precio'], ',', '.')."<br/>".
                    "</ul>";
            }
        $message .=
            "</p>".
            "<hr/>".
            "<p>Cuando lo desees puedes usar tus paquetes comprados ingresando con tu usuario en ".
            "<a href='".Constant::$APLICACION_DOMINIO."' target='_blank'>".Constant::$APLICACION_DOMINIO."</a>".
            "</p>".
            "<p>Gracias, <br/> Equipo de ".Constant::$APLICACION_NOMBRE."</p>";
        $from_name = Constant::$REMITENTE_NOMBRE;
        $from_email = Constant::$REMITENTE_EMAIL;
        $admin_email = Constant::$ADMINISTRADOR_EMAIL;
        MailSender::send_mail($user_email, $subject, $message, $from_name, $from_email, $admin_email);
    }
}
?>