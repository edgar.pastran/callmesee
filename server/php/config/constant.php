<?php
/**
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 20/06/2018
 * Time: 01:08 PM
 */

class Constant {
    /**************************************************************************
     * CONSTANTES RELACIONADAS CON CONFIGURACION GENERAL - INICIO
     **************************************************************************/
    // DATOS DE CONFIGURACION DE LA APLICACION
    public static $APLICACION_NOMBRE = "Hola Doctor";
    public static $APLICACION_DOMINIO = "http://www.holadoctor.com";
    public static $APLICACION_MEDIO_CREADOR = "APLICACION WEB";

    // DATOS PARA EL ENVIO DE EMAILS
    public static $REMITENTE_NOMBRE = "Soporte Hola Doctor";
    public static $REMITENTE_EMAIL = "soporte@holadoctor.com";
    public static $ADMINISTRADOR_EMAIL = "admin@holadoctor.com"; // Usado para copiar los emails de compra

    // NOMBRE DE LOS DIRECTORIOS DE ARCHIVOS
    public static $DIRECTORIO_IMAGENES_USUARIOS = "server/repo/user/";
    public static $DIRECTORIO_LOGS = "server/log/";

    // NOMBRE DE LOS ARCHIVOS POR DEFECTO
    public static $ARCHIVO_IMAGEN_USUARIO = "client/resources/img/user.jpg";
    public static $PAGINA_VERIFICACION_USUARIO = "client/verification.html";

    // OTRAS CONSTANTES
    public static $LONGITUD_CODIGO_VERIFICACION = 10;
    public static $CANTIDAD_MENSAJES_MOSTRADOS = 10;
    public static $TIEMPO_MENSAJES_RECIENTES = "2 MINUTE";
    /**************************************************************************
     * CONSTANTES RELACIONADAS CON CONFIGURACION GENERAL - FIN
     **************************************************************************/

    /**************************************************************************
     * CONSTANTES RELACIONADAS CON BASE DE DATOS - INICIO
     **************************************************************************/
    // NOMBRE DE LAS TABLAS
    public static $TABLA_ASESOR = "ASESOR";
    public static $TABLA_ASESOR_CATEGORIAS = "ASESOR_CATEGORIAS";
    public static $TABLA_CATEGORIA = "CATEGORIA";
    public static $TABLA_CLIENTE = "CLIENTE";
    public static $TABLA_CONFIGURACION = "CONFIGURACION";
    public static $TABLA_COMPRA = "COMPRA";
    public static $TABLA_CONSULTA = "CONSULTA";
    public static $TABLA_CONSULTA_PREGUNTA = "CONSULTA_PREGUNTA";
    public static $TABLA_CONSULTA_RESPUESTA = "CONSULTA_RESPUESTA";
    public static $TABLA_MENSAJE = "MENSAJE";
    public static $TABLA_PAGO = "PAGO";
    public static $TABLA_PAGO_PAQUETES = "PAGO_PAQUETES";
    public static $TABLA_PAQUETE = "PAQUETE";
    public static $TABLA_PREGUNTA = "PREGUNTA";
    public static $TABLA_RESPUESTA = "RESPUESTA";
    public static $TABLA_USUARIO = "USUARIO";

    // ESTADOS DE REGISTROS
    public static $REGISTRO_ACTIVO = "ACTIVO";
    public static $REGISTRO_INACTIVO = "INACTIVO";
    public static $REGISTRO_POR_VERIFICAR = "POR VERIFICAR";

    // TIPOS DE PAQUETES
    public static $TIPO_PAQUETE_POR_LLAMADAS = "POR LLAMADAS";
    public static $TIPO_PAQUETE_POR_SUSCRIPCION = "POR SUSCRIPCION";
    public static $TIPO_PAQUETE_GRATIS = "GRATIS";

    // VALORES PARA CAMPOS CON ENUMERADO SI/NO
    public static $SI = "SI";
    public static $NO = "NO";

    // CONDICIONES DE CONSULTA
    public static $CONSULTA_CONDICION_SIN_CONTESTAR = "SIN CONTESTAR";
    public static $CONSULTA_CONDICION_ACEPTADA = "ACEPTADA";
    public static $CONSULTA_CONDICION_RECHAZADA = "RECHAZADA";
    public static $CONSULTA_CONDICION_ESPERA_EXCEDIDA = "ESPERA EXCEDIDA";

    // IDS DE LA TABLA ROL
    public static $ROL_ADMINISTRADOR = "ADMIN";
    public static $ROL_ASESOR = "ADVISER";
    public static $ROL_CLIENTE = "CUSTOMER";

    // NOMBRES DE REGISTROS EN LA TABLA CONFIGURACION
    public static $MERCADO_PAGO_ACEPTADO = "mercadopago_accept_payments";
    public static $MERCADO_PAGO_CLIENT_ID = "mercadopago_client_id";
    public static $MERCADO_PAGO_CLIENT_SECRET = "mercadopago_client_secret";
    public static $MERCADO_PAGO_USA_SANDBOX = "mercadopago_use_sandbox";
    public static $TOKBOX_API_KEY = "tokbox_api_key";
    public static $TOKBOX_API_SECRET = "tokbox_api_secret";
    /**************************************************************************
     * CONSTANTES RELACIONADAS CON BASE DE DATOS - FIN
     **************************************************************************/

    /**************************************************************************
     * OTRAS CONSTANTES - INICIO
     **************************************************************************/
    // PAGINAS DE REDIRECCION
    public static $PAGINA_PAGO_EXITOSO = "client/customer/payment_success.html";
    public static $PAGINA_PAGO_FALLIDO = "client/customer/payment_failure.html";
    public static $PAGINA_PAGO_PENDIENTE = "client/customer/payment_pending.html";

    // MERCADO PAGO
    public static $MERCADO_PAGO_ID = "mercadopago";
    public static $MERCADO_PAGO_NOMBRE = "Mercado Pago";
    public static $MERCADO_PAGO_IMAGEN = "client/resources/img/mercadopago.png";
    public static $MERCADO_PAGO_NOTIFICACION_PAGOS = "server/php/customer/mercadopago_ipn.php";
    /**************************************************************************
     * OTRAS CONSTANTES - FIN
     **************************************************************************/
}
?>