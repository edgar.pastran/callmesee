<?php //
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

$respuesta = array( 'exito' => false, 'mensaje' => '');
try {
    if (isset($_POST)) {
        if (isset($_POST['operacion'])) {
            require_once("service/_profile.php");

            $servicio = new _Profile();
            $operacion = $_POST['operacion'];

            if ($operacion == "actualizar_datos_basicos"   && isset($_POST['usuario'])) {
                $usuario = json_decode($_POST['usuario']);
                $respuesta = $servicio->actualizar_datos_basicos($usuario);
            }
            else if ($operacion == "cambiar_clave"   && isset($_POST['usuario'])) {
                $usuario = json_decode($_POST['usuario']);
                $respuesta = $servicio->cambiar_clave($usuario);
            }
            else if ($operacion == "cambiar_imagen"   && isset($_POST['id_usuario'])) {
                $id_usuario = $_POST['id_usuario'];
                $respuesta = $servicio->cambiar_imagen($id_usuario);
            }
            else if ($operacion == "remover_imagen"   && isset($_POST['usuario'])) {
                $usuario = json_decode($_POST['usuario']);
                $respuesta = $servicio->remover_imagen($usuario);
            }
            else {
                $respuesta['mensaje'] = 'No se indicaron todos los parametros necesarios.';
            }
        }
        else {
            $respuesta['mensaje'] = 'Es necesario indicar la operacion.';
        }
    }
    else {
        $respuesta['mensaje'] = 'Solo se permiten parametros mediante el metodo POST.';
    }
}
catch(Exception $e) {
    $respuesta['mensaje'] = $e->getMessage();
}
echo json_encode($respuesta, true);
?>