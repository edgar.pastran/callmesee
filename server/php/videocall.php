<?php //
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

$respuesta = array( 'exito' => false, 'mensaje' => '');
try {
    if (isset($_POST)) {
        if (isset($_POST['operacion'])) {
            require_once("service/_videocall.php");

            $servicio = new _VideoCall();
            $operacion = $_POST['operacion'];

            if ($operacion == "consultar" && isset($_POST['usuario']) && isset($_POST['id_consulta'])) {
                $usuario = json_decode($_POST['usuario']);
                $id_consulta = $_POST['id_consulta'];
                $respuesta = $servicio->consultar($usuario, $id_consulta);
            }
            else if ($operacion == "guardar" && isset($_POST['usuario']) && isset($_POST['id_consulta'])) {
                $usuario = json_decode($_POST['usuario']);
                $id_consulta = $_POST['id_consulta'];
                $respuesta = $servicio->guardar($usuario, $id_consulta);
            }
            else {
                $respuesta['mensaje'] = 'No se indicaron todos los parametros necesarios.';
            }
        }
        else {
            $respuesta['mensaje'] = 'Es necesario indicar la operacion.';
        }
    }
    else {
        $respuesta['mensaje'] = 'Solo se permiten parametros mediante el metodo POST.';
    }
}
catch(Exception $e) {
    $respuesta['mensaje'] = $e->getMessage();
}
echo json_encode($respuesta, true);
?>
