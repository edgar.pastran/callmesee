<?php //
header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

$respuesta = array( 'exito' => false, 'mensaje' => '');
try {
    if (isset($_POST)) {
        if (isset($_POST['operacion'])) {
            require_once ("service/_messages.php");

            $servicio = new _Messages();
            $operacion = $_POST['operacion'];

            if ($operacion == "cargar_mensajes" && isset($_POST['usuario']) && isset($_POST['ultimo_mensaje'])) {
                $usuario = json_decode($_POST['usuario']);
                $ultimo_mensaje = $_POST['ultimo_mensaje'];
                $respuesta = $servicio->cargar_mensajes($usuario, $ultimo_mensaje);
            }
            else if ($operacion == "marcar_mensaje_leido" && isset($_POST['usuario']) && isset($_POST['id_mensaje'])) {
                $usuario = json_decode($_POST['usuario']);
                $id_mensaje = $_POST['id_mensaje'];
                $respuesta = $servicio->marcar_mensaje_leido($usuario, $id_mensaje);
            }
            else if ($operacion == "consultar_mensajes_pendientes" && isset($_POST['usuario'])) {
                $usuario = json_decode($_POST['usuario']);
                $respuesta = $servicio->consultar_mensajes_pendientes($usuario);
            }
            else if ($operacion == "rechazar_consulta" && isset($_POST['usuario']) && isset($_POST['id_consulta'])) {
                $usuario = json_decode($_POST['usuario']);
                $id_consulta = $_POST['id_consulta'];
                $respuesta = $servicio->rechazar_consulta($usuario, $id_consulta);
            }
            else {
                $respuesta['mensaje'] = 'No se indicaron todos los parametros necesarios.';
            }
        }
        else {
            $respuesta['mensaje'] = 'Es necesario indicar la operacion.';
        }
    }
    else {
        $respuesta['mensaje'] = 'Solo se permiten parametros mediante el metodo POST.';
    }
}
catch(Exception $e) {
    $respuesta['mensaje'] = $e->getMessage();
}
echo json_encode($respuesta, true);
?>