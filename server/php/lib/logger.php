<?php
/**
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 20/07/2018
 * Time: 09:03 AM
 */
class Logger {

    private static $LOG_HANDLER = array();

    public static function logOpen($log_dir, $file_name, $file_suffix='') {
        self::logClose($file_name);

        $directory = $log_dir.$file_name.'/';
        if (!file_exists($log_dir)) {
            mkdir($log_dir);
        }
        if (!file_exists($directory)) {
            mkdir($directory);
        }

        self::$LOG_HANDLER[$file_name] = fopen($directory.$file_name.'_'.date('Y-m-d').$file_suffix.'.txt', 'a');
        self::logWrite($file_name, '================================================================');
    }

    private static function logClose($file_name) {
        if (array_key_exists($file_name, self::$LOG_HANDLER)) {
            fclose(self::$LOG_HANDLER[$file_name]);
            self::$LOG_HANDLER[$file_name] = null;
        }
    }

    public static function logWrite($file_name, $txt) {
        fwrite(self::$LOG_HANDLER[$file_name],sprintf("%s\t%s\r\n",date('d/m/Y H:i:s'), $txt));
    }
}
?>