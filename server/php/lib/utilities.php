<?php
/**
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 23/06/2018
 * Time: 11:08 AM
 */

class Utilities {

    public static function random_word($length) {
        return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
    }
}
?>
