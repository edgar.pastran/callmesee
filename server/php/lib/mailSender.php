<?php
/**
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 22/06/2018
 * Time: 07:27 PM
 */

class MailSender {

    public static function send_mail($to_email, $subject, $message, $from_name, $from_email, $bcc_email=null) {
        $header  = "MIME-Version: 1.0" . "\r\n";
        $header .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
        $header .= "From: =?UTF-8?B?". base64_encode($from_name) ."?= <$from_email>\r\n";
        $header .= ($bcc_email != null)?("Bcc: ".$bcc_email . "\r\n"):"";
        $header .= "Reply-To: ".$from_email . "\r\n" .
        $header .= "X-Mailer: PHP/" . phpversion();
        $message =
            "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
            <html xmlns='http://www.w3.org/1999/xhtml'>
            <head>
            <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
            <title></title>
            </head>
            <body>".
            $message.
            "</body></html>";
        mail($to_email, $subject, $message, $header, '-f'.$from_email);
    }
}
?>