<?php
/**
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 22/06/2018
 * Time: 04:39 PM
 */

require_once("utilities.php");
class FileUploader {

    private static $MAXIMUM_FILE_SIZE_IN_KB = 500;
    private static $RANDOM_FILE_NAME_LENGTH = 8;
    private static $IMAGE_EXTENSION_ALLOWED = array("JPG", "PNG", "JPEG", "GIF");

    public static function upload_file($field_name, $target_dir, $file_name, $random_file_name=false) {
        $result = array(
            "exito" => false,
            "mensaje" => "",
            "file_name" => ""
        );
        // Create the directory if it does not exist
        if (!file_exists($target_dir)) {
            mkdir($target_dir, 0777, true);
        }
        // Check file size
        if ($_FILES[$field_name]["size"] > (self::$MAXIMUM_FILE_SIZE_IN_KB * 1024)) {
            $result["mensaje"] = "Lo siento, tu archivo es muy grande. Debe ser menor o igual a ".self::$MAXIMUM_FILE_SIZE_IN_KB." KB";
            return $result;
        }
        $target_file = $target_dir . $file_name;
        // Remove if other file with same name already exists
        if (file_exists($target_file)) {
            unlink( $target_file );
        }
        // Check if the file will have a random name
        if ($random_file_name) {
            $file_type = strtoupper(pathinfo($target_file,PATHINFO_EXTENSION));
            $file_name = Utilities::random_word(self::$RANDOM_FILE_NAME_LENGTH);
            $file_name .= ".".$file_type;
            $target_file = $target_dir . $file_name;
        }
        // If everything is ok, try to upload file
        if (move_uploaded_file($_FILES[$field_name]["tmp_name"], $target_file)) {
            $result["exito"] = true;
            $result["mensaje"] = "El archivo ". basename( $_FILES[$field_name]["name"]). " fue cargado satisfactoriamente.";
            $result["file_name"] = $file_name;
        }
        else {
            $result["mensaje"] = "Lo siento, hubo un error cargando tu archivo.";
        }
        return $result;
    }

    public static function upload_image($field_name, $target_dir, $file_name, $random_file_name=false) {
        $result = array(
            "exito" => false,
            "mensaje" => "",
            "file_name" => ""
        );
        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES[$field_name]["tmp_name"]);
            if($check === false) {
                $result["mensaje"] = "El archivo no es una imagen.";
                return $result;
            }
        }
        $target_file = $target_dir . basename($_FILES[$field_name]["name"]);
        $image_file_extension = strtoupper(pathinfo($target_file,PATHINFO_EXTENSION));
        // Allow certain file formats
        if (!in_array($image_file_extension, self::$IMAGE_EXTENSION_ALLOWED)) {
            $result["mensaje"] = "Lo siento, solo son permitidos archivos con extension ".implode(", ", self::$IMAGE_EXTENSION_ALLOWED).".";
            return $result;
        }
        // if everything is ok, try to upload file
        $file_name = explode(".", $file_name)[0];
        $result = self::upload_file($field_name, $target_dir, $file_name.".".$image_file_extension, $random_file_name);
        if ($result["exito"]) {
            // Remove if other image with same name but another extension still exists
            foreach (self::$IMAGE_EXTENSION_ALLOWED as $extension) {
                if ($image_file_extension !== $extension) {
                    $target_file = $target_dir.$file_name.".".$extension;
                    if (file_exists($target_file)) {
                        unlink( $target_file );
                    }
                }
            }
        }
        return $result;
    }

}
?>