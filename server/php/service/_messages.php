<?php
/**
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 23/07/2018
 * Time: 08:41 AM
 */

require_once("config/constant.php");
require_once("model/asesor.php");
require_once("model/consulta.php");
require_once("model/mensaje.php");
require_once("model/usuario.php");
require_once("service/service.php");
class _Messages extends Service {

    public function cargar_mensajes($usuario, $ultimo_mensaje) {
        $respuesta = array( 'exito' => false, 'mensaje' => '');

        // Buscar los mensajes a cargar
        $mensajes = Mensaje::consultar_mensajes_por_usuario($usuario->id_usuario, $ultimo_mensaje);
        for ($i=0; $i<count($mensajes); $i++) {
            $mensajes[$i]['usuario_imagen'] = $this->get_usuario_imagen($mensajes[$i]['_imagen']);
            $mensajes[$i]['usuario_nombre_completo'] = $mensajes[$i]['_nombres']." ".$mensajes[$i]['_apellidos'];
            $mensajes[$i]['fecha_format'] = date("d/m/Y H:i:s", strtotime($mensajes[$i]['fecha_creacion']));
        }
        // Buscar el ID del primer mensaje recibido
        $primer_mensaje = Mensaje::consultar_id_primer_mensaje_por_usuario($usuario->id_usuario);
        $respuesta['exito'] = true;
        $respuesta['mensajes'] = $mensajes;
        $respuesta['primer_mensaje'] = $primer_mensaje;

        return $respuesta;
    }

    public function marcar_mensaje_leido($usuario, $id_mensaje) {
        $respuesta = Usuario::actualizar_fecha_ultima_actividad($usuario->id_usuario);
        if ($respuesta['exito']) {
            $respuesta = Mensaje::marcar_mensaje_leido($id_mensaje);
            if ($respuesta['exito']) {
                // Consultar el total de mensajes no leidos
                $total_mensajes = Mensaje::consultar_total_mensajes_no_leidos_por_usuario($usuario->id_usuario);
                $respuesta['total_mensajes'] = $total_mensajes;
            }
        }
        return $respuesta;
    }

    public function consultar_mensajes_pendientes($usuario) {
        $respuesta = Usuario::actualizar_fecha_ultima_actividad($usuario->id_usuario);

        // Consultar los mensajes recientes
        $mensajes_recientes = Mensaje::consultar_mensajes_no_leidos_recientes_por_usuario($usuario->id_usuario);
        for ($i=0; $i<count($mensajes_recientes); $i++) {
            $mensajes_recientes[$i]['usuario_imagen'] = $this->get_usuario_imagen($mensajes_recientes[$i]['_imagen']);
            $mensajes_recientes[$i]['usuario_nombre_completo'] = $mensajes_recientes[$i]['_nombres']." ".$mensajes_recientes[$i]['_apellidos'];
        }
        // Consultar el total de mensajes no leidos
        $total_mensajes = Mensaje::consultar_total_mensajes_no_leidos_por_usuario($usuario->id_usuario);
        $respuesta['exito'] = true;
        $respuesta['mensajes_recientes'] = $mensajes_recientes;
        $respuesta['total_mensajes'] = $total_mensajes;

        return $respuesta;
    }

    public function rechazar_consulta($usuario, $id_consulta) {
        $respuesta = array( 'exito' => false, 'mensaje' => '');

        $asesor = Asesor::consultar_asesor_por_id_usuario($usuario->id_usuario);
        if (isset($asesor)) {
            $consulta = Consulta::consultar_consulta_por_id($id_consulta);
            if (isset($consulta)) {
                $id_asesor = $asesor["id_asesor"];
                if ($id_asesor == $consulta["id_asesor"]) {
                    $respuesta = Consulta::rechazar_consulta($id_consulta);
                }
                else {
                    $respuesta['mensaje'] = "Esta consulta no esta dirigida a su usuario";
                }
            }
            else {
                $respuesta['mensaje'] = "Consulta no encontrada";
            }
        }
        else {
            $respuesta['mensaje'] = "Asesor no encontrado";
        }

        return $respuesta;
    }

    private function get_usuario_imagen($nombre_imagen) {
        $url_imagen = $this->URL_BASE_PATH.Constant::$ARCHIVO_IMAGEN_USUARIO;
        if (($nombre_imagen != null) && (file_exists($this->DIR_BASE_PATH.Constant::$DIRECTORIO_IMAGENES_USUARIOS.$nombre_imagen))) {
            $url_imagen = $this->URL_BASE_PATH.Constant::$DIRECTORIO_IMAGENES_USUARIOS.$nombre_imagen;
        }
        return $url_imagen;
    }
}
?>