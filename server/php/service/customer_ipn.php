<?php
/**
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 20/07/2018
 * Time: 09:47 AM
 */

require_once("../config/constant.php");
require_once("../config/mailer.php");
require_once("../model/cliente.php");
require_once("../model/compra.php");
require_once("../model/configuracion.php");
require_once("../model/pago.php");
require_once("../model/pago_paquete.php");
require_once("../lib/logger.php");
require_once("../lib/mercadopago/mercadoPago.php");
require_once("../service/service.php");
class Customer_IPN extends Service {

    /**************************************************************************
     * MERCADO PAGO - INICIO
     **************************************************************************/
    public function revisar_mercadopago_notificacion($parametros) {
        $respuesta = array( 'exito' => false, 'mensaje' => '');

        Logger::logOpen($this->DIR_BASE_PATH.Constant::$DIRECTORIO_LOGS, Constant::$MERCADO_PAGO_ID);
        Logger::logWrite(Constant::$MERCADO_PAGO_ID, __METHOD__.' - parametros: '.json_encode($parametros));

        $mercadopago = $this->get_mercadopago_object();
        $params = ["access_token" => $mercadopago->get_access_token()];
        Logger::logWrite(Constant::$MERCADO_PAGO_ID, __METHOD__.' - id: "'.$parametros["id"].'", topic: "'.$parametros["topic"].'", access_token: "'.$params["access_token"].'"');

        $orden = null;
        if($parametros["topic"] == 'payment'){
            $payment_info = $mercadopago->get("/collections/notifications/" . $parametros["id"], $params, false);
            $orden = $mercadopago->get("/merchant_orders/" . $payment_info["response"]["collection"]["merchant_order_id"], $params, false);
        }
        else if($parametros["topic"] == 'merchant_order'){
            $orden = $mercadopago->get("/merchant_orders/" . $parametros["id"], $params, false);
        }

        if ($orden != null && is_array($orden) && array_key_exists("status", $orden) && $orden["status"] == 200) {
            Logger::logWrite(Constant::$MERCADO_PAGO_ID, __METHOD__.' - status: "'.$orden["status"].'"');

            $orden_info = $orden["response"];
            Logger::logWrite(Constant::$MERCADO_PAGO_ID, __METHOD__.' - orden_info: "'.json_encode($orden_info).'"');
            Logger::logWrite(Constant::$MERCADO_PAGO_ID, __METHOD__.' - external_reference: "'.$orden_info["external_reference"].'"');

            $monto_pagado = 0;
            $monto_orden = $orden_info["total_amount"];
            $pagos = $orden_info["payments"];
            foreach ($pagos as  $pago) {
                if($pago['status'] == 'approved') {
                    $monto_pagado += $pago['transaction_amount'];
                }
            }
            Logger::logWrite(Constant::$MERCADO_PAGO_ID, __METHOD__.' - monto_pagado: "'.$monto_pagado.'" y monto_orden: "'.$monto_orden.'"');

            $isSandBox = $this->is_mercadopago_sandbox();
            $isInDevServer = $this->is_development_version();
            $isSandBoxInDevServer = $isSandBox && $isInDevServer;
            Logger::logWrite(Constant::$MERCADO_PAGO_ID, __METHOD__.' - isSandBoxInDevServer: "'.$isSandBoxInDevServer.'" (isSandBox: "'.$isSandBox.'" && isInDevServer: "'.$isInDevServer.'")');

            if ($isSandBoxInDevServer) {
                $monto_pagado = $monto_orden;
            }
            if ($monto_pagado >= $monto_orden) {
                //Pago aprobado, Liberar productos y actualizar pedido
                $id_referencia = $orden_info["external_reference"];
                $email_pagador = isset($orden_info["payer"]["email"]) ? $orden_info["payer"]["email"] : '';
                Logger::logWrite(Constant::$MERCADO_PAGO_ID, __METHOD__.' - id_referencia: "'.$id_referencia.'"');
                Logger::logWrite(Constant::$MERCADO_PAGO_ID, __METHOD__.' - email_pagador: "'.$email_pagador.'"');
                Logger::logWrite(Constant::$MERCADO_PAGO_ID, __METHOD__.' - monto_pagado: "'.$monto_pagado.'"');
                $respuesta = $this->procesar_pago_confirmado(Constant::$MERCADO_PAGO_ID, $id_referencia, $email_pagador, $monto_pagado);
            }
        }
        return $respuesta;
    }

    private function get_mercadopago_object() {
        $client_id = Configuracion::consultar_configuracion_por_nombre(Constant::$MERCADO_PAGO_CLIENT_ID);
        $client_secret = Configuracion::consultar_configuracion_por_nombre(Constant::$MERCADO_PAGO_CLIENT_SECRET);
        $config = array (
            'client_id'     => $client_id['valor'],
            'client_secret' => $client_secret['valor'],
            'sandbox_mode'  => $this->is_mercadopago_sandbox()
        );
        return new Mercadopago($config);
    }

    private function is_mercadopago_sandbox() {
        $result = true;
        $use_sandbox = Configuracion::consultar_configuracion_por_nombre(Constant::$MERCADO_PAGO_USA_SANDBOX);
        if ($use_sandbox != null) {
            $result = ($use_sandbox['valor'] == Constant::$REGISTRO_ACTIVO);
        }
        return $result;
    }
    /**************************************************************************
     * MERCADO PAGO - FIN
     **************************************************************************/

    private function procesar_pago_confirmado($medio_pago, $id_referencia, $email_pagador, $monto) {
        $respuesta = array( 'exito' => false, 'mensaje' => '');

        $pago = Pago::consultar_pago_por_referencia_medio_pago($id_referencia, $medio_pago);
        if ($pago != null) {
            $monto_pagado = number_format($monto, 2, '.', '');
            $pago_total = $pago['total'];
            Logger::logWrite($medio_pago, __METHOD__ . ' - monto_pagado: "' . $monto_pagado . '"');
            if ($monto_pagado >= $pago_total) {
                if ($pago['confirmado'] == Constant::$NO) {
                    $id_pago = $pago['id_pago'];
                    $id_cliente = $pago['id_cliente'];
                    $pago_paquetes = Pago_Paquete::consultar_pago_paquetes_por_pago($id_pago);
                    Logger::logWrite($medio_pago, __METHOD__ . ' - ----------------------------------------------------------------');
                    Logger::logWrite($medio_pago, __METHOD__ . ' - PROCESAR LA COMPRA DE '.count($pago_paquetes).' PAQUETES');
                    Logger::logWrite($medio_pago, __METHOD__ . ' - ----------------------------------------------------------------');
                    for ($j=0; $j<count($pago_paquetes); $j++) {
                        $pago_paquetes[$j]['registrados'] = 0;

                        $id_paquete = $pago_paquetes[$j]['id_paquete'];
                        $paquete_nombre = $pago_paquetes[$j]['_paquete_nombre'];
                        $dias = $pago_paquetes[$j]['dias'];
                        $cantidad = intval($pago_paquetes[$j]['cantidad']);

                        Logger::logWrite($medio_pago, __METHOD__ . ' - '.($j+1).') PAQUETE: '.$paquete_nombre.'. CANTIDAD: '.$cantidad);
                        for ($i=1; $i<=$cantidad; $i++) {
                            $pago_paquete_cantidad_counter = Compra::contar_compras_por_pago_paquete($id_pago, $id_paquete);
                            Logger::logWrite($medio_pago, __METHOD__ . ' - '.($j+1).') CANTIDAD DE COMPRAS REGISTRADAS CON PAGO ('.$id_pago.') Y PAQUETE ('.$id_paquete.'): '.$pago_paquete_cantidad_counter);
                            if ($pago_paquete_cantidad_counter < $cantidad) {
                                // Registrar la compra
                                $respuesta = $this->registrar_compra($id_pago, $id_paquete, $id_cliente, $dias);
                                if ($respuesta['exito']) {
                                    Logger::logWrite($medio_pago, __METHOD__ . ' - '.($j+1).'.'.$i.') COMPRA REGISTRADA SATISFACTORIAMENTE. ID: '.$respuesta['codigo']);
                                    $pago_paquetes[$j]['registrados'] += 1;
                                }
                                else {
                                    Logger::logWrite($medio_pago, __METHOD__ . ' - '.($j+1).'.'.$i.') PROBLEMAS AL REGISTRAR LA COMPRA: '.$respuesta['mensaje']);
                                }
                            }
                            Logger::logWrite($medio_pago, __METHOD__ . ' - ----------------------------------------------------------------');
                        }
                    }
                    if ($respuesta['exito']) {
                        Logger::logWrite($medio_pago, __METHOD__ . ' - ----------------------------------------------------------------');
                        Logger::logWrite($medio_pago, __METHOD__ . ' - ACTUALIZAR DATOS DE PAGO ('.$id_pago.')');
                        Logger::logWrite($medio_pago, __METHOD__ . ' - ----------------------------------------------------------------');

                        // Confirmar el pago
                        $respuesta = Pago::confirmar_pago($id_pago, $email_pagador, $monto_pagado);
                        if ($respuesta['exito']) {
                            Logger::logWrite($medio_pago, __METHOD__ . ' - PAGO ACTUALIZADO SATISFACTORIAMENTE. ID: '.$respuesta['codigo']);

                            // Enviar email con informacion de la compra
                            $this->enviar_email_compra($id_cliente, $medio_pago, $id_referencia, $monto_pagado, $pago_paquetes);
                        }
                        else {
                            Logger::logWrite($medio_pago, __METHOD__ . ' - PROBLEMAS AL ACTUALIZAR EL PAGO: '.$respuesta['mensaje']);
                        }
                    }
                }
                else {
                    Logger::logWrite($medio_pago, __METHOD__ . ' - YA ESTE PAGO FUE PROCESADO ANTERIORMENTE');
                }
            }
            else {
                Logger::logWrite($medio_pago, __METHOD__ . ' - EL MONTO PAGADO ('.$monto_pagado.') ES MENOR AL TOTAL DEL PAGO ESPERADO ('.$pago_total.')');
            }
        }
        else {
            Logger::logWrite($medio_pago, __METHOD__ . ' - NO SE ENCONTRO UN PAGO CON ESTE ID_REFERENCIA ('.$id_referencia.') Y MEDIO_PAGO ('.$medio_pago.')');
        }
        Logger::logWrite($medio_pago, __METHOD__ . ' - ----------------------------------------------------------------');
        return $respuesta;
    }

    private function registrar_compra($id_pago, $id_paquete, $id_cliente, $dias) {
        $compra = array(
            "id_pago" => $id_pago,
            "id_paquete" => $id_paquete,
            "id_cliente" => $id_cliente,

        );
        if (isset($dias) && $dias > 0) {
            $compra["fecha_vencimiento"] = "DATE_ADD(NOW(), INTERVAL ".$dias." DAY)";
        }
        return Compra::registrar_compra($compra);
    }

    private function enviar_email_compra($id_cliente, $medio_pago, $id_referencia, $monto_pagado, $pago_paquetes) {
        $cliente = Cliente::consultar_cliente_por_id($id_cliente);
        if ($cliente != null) {
            $user_email = $cliente['_correo'];
            $user_firstname = $cliente['_nombres'];
            $user_lastname = $cliente['_apellidos'];
            $medio_pago_nombre = '';
            if ($medio_pago == Constant::$MERCADO_PAGO_ID) {
                $medio_pago_nombre = Constant::$MERCADO_PAGO_NOMBRE;
            }
            Mailer::send_confirmed_purchase_mail($user_email, $user_firstname, $user_lastname, $medio_pago_nombre, $id_referencia, $monto_pagado, $pago_paquetes);
        }
    }
}
?>