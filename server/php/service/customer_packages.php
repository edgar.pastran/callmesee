<?php
/**
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 26/07/2018
 * Time: 03:22 PM
 */

require_once("../config/constant.php");
require_once("../model/categoria.php");
require_once("../model/paquete.php");
require_once('../service/service.php');
class Customer_Packages extends Service {

    public function consultar() {
        $respuesta = array( 'exito' => false, 'mensaje' => '');

        $categorias = Categoria::consultar_categorias(Constant::$REGISTRO_ACTIVO);
        $paquetes = Paquete::consultar_paquetes(Constant::$REGISTRO_ACTIVO);
        $data = array(
            "categorias" => $categorias,
            "paquetes" => $paquetes
        );
        $respuesta['exito'] = true;
        $respuesta['data'] = $data;

        return $respuesta;
    }
}
?>
