<?php
/**
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 23/07/2018
 * Time: 10:57 AM
 */

require_once("../config/constant.php");
require_once("../config/mailer.php");
require_once("../lib/fileUploader.php");
require_once("../model/categoria.php");
require_once("../model/cliente.php");
require_once("../model/usuario.php");
require_once('../service/service.php');
class Admin_Customers extends Service {

    public function consultar() {
        $respuesta = array( 'exito' => false, 'mensaje' => '');

        $data = Cliente::consultar_clientes();
        for ($i=0; $i<count($data); $i++) {
            $data[$i]['_imagen'] = $this->get_cliente_imagen($data[$i]['_imagen']);
        }
        $respuesta['exito'] = true;
        $respuesta['data'] = $data;

        return $respuesta;
    }

    public function nuevo($cliente) {
        $respuesta = array( 'exito' => false, 'mensaje' => '');

        $usuario = $cliente->usuario;

        $otro_usuario = Usuario::consultar_usuario_por_id($usuario->id_usuario);
        if (isset($otro_usuario)) {
            $respuesta['mensaje'] = 'Ya existe un usuario con este correo.';
        }
        else {
            $respuesta = Usuario::registrar_usuario_rol_cliente($usuario);
            if ($respuesta["exito"]) {
                $codigo_verificacion = $respuesta['data']->codigo_verificacion;
                $image_field_name = 'imagen';
                if (isset($_FILES[$image_field_name]['name']) && $_FILES[$image_field_name]['name'] != '') {
                    $id_usuario = $usuario->id_usuario;
                    $image_directory = $this->DIR_BASE_PATH.Constant::$DIRECTORIO_IMAGENES_USUARIOS;
                    $respuesta = FileUploader::upload_image($image_field_name, $image_directory, $id_usuario);
                    if ($respuesta["exito"]) {
                        $imagen = $respuesta["file_name"];
                        $respuesta = Usuario::actualizar_imagen($id_usuario, $imagen);
                    }
                }
                if ($respuesta["exito"]) {
                    $respuesta = Cliente::registrar_cliente($usuario);
                    if ($respuesta["exito"]) {
                        $url_verificacion = $this->URL_BASE_PATH.Constant::$PAGINA_VERIFICACION_USUARIO;
                        Mailer::send_verification_mail($usuario->correo, $url_verificacion, $codigo_verificacion, $usuario->nombres, $usuario->apellidos);

                        $id_cliente = $respuesta["codigo"];
                        $data = Cliente::consultar_cliente_por_id($id_cliente);
                        $data['_imagen'] = $this->get_cliente_imagen($data['_imagen']);
                        $respuesta["data"] = $data;
                    }
                }
            }
        }

        return $respuesta;
    }

    public function editar($cliente) {
        $usuario = $cliente->usuario;
        $respuesta = Usuario::actualizar_usuario($usuario);
        if ($respuesta["exito"]) {
            $image_field_name = 'imagen';
            if (isset($_FILES[$image_field_name]['name']) && $_FILES[$image_field_name]['name'] != '') {
                $id_usuario = $usuario->id_usuario;
                $image_directory = $this->DIR_BASE_PATH.Constant::$DIRECTORIO_IMAGENES_USUARIOS;
                $respuesta = FileUploader::upload_image($image_field_name, $image_directory, $id_usuario);
                if ($respuesta["exito"]) {
                    $imagen = $respuesta["file_name"];
                    $respuesta = Usuario::actualizar_imagen($id_usuario, $imagen);
                }
            }
            if ($respuesta["exito"]) {
                $id_cliente = $cliente->id_cliente;
                $data = Cliente::consultar_cliente_por_id($id_cliente);
                $data['_imagen'] = $this->get_cliente_imagen($data['_imagen']);
                $respuesta["data"] = $data;
            }
        }

        return $respuesta;
    }

    public function remover_imagen($cliente) {
        $respuesta = array( 'exito' => false, 'mensaje' => '');

        $usuario = $cliente->usuario;
        $usuario_consultado = Usuario::consultar_usuario_por_id($usuario->id_usuario);
        if (isset($usuario_consultado)) {
            $imagen_usuario = "";
            if (isset($usuario_consultado["imagen"])) {
                $imagen_usuario = $this->DIR_BASE_PATH.Constant::$DIRECTORIO_IMAGENES_USUARIOS.($usuario_consultado["imagen"]);
            }
            $respuesta = Usuario::actualizar_imagen($usuario->id_usuario, null);
            if ($respuesta["exito"]) {
                if ($imagen_usuario != "" && file_exists($imagen_usuario)) {
                    unlink($imagen_usuario);
                }
                $data = Cliente::consultar_cliente_por_id($cliente->id_cliente);
                $data['_imagen'] = $this->URL_BASE_PATH.Constant::$ARCHIVO_IMAGEN_USUARIO;
                $respuesta["data"] = $data;
            }
        }
        else {
            $respuesta['mensaje'] = 'Usuario no encontrado.';
        }

        return $respuesta;
    }

    public function desactivar($id_cliente) {
        $respuesta = Cliente::desactivar_cliente($id_cliente);
        if ($respuesta['exito']) {
            $data = $respuesta['data'];
            $respuesta = Usuario::desactivar_usuario($data['id_usuario']);
            if ($respuesta['exito']) {
                $data['_imagen'] = $this->get_cliente_imagen($data['_imagen']);
                $respuesta['data'] = $data;
            }
        }

        return $respuesta;
    }

    public function activar($id_cliente) {
        $respuesta = Cliente::activar_cliente($id_cliente);
        if ($respuesta['exito']) {
            $data = $respuesta['data'];
            $respuesta = Usuario::activar_usuario($data['id_usuario']);
            if ($respuesta['exito']) {
                $data['_imagen'] = $this->get_cliente_imagen($data['_imagen']);
                $respuesta['data'] = $data;
            }
        }

        return $respuesta;
    }

    private function get_cliente_imagen($nombre_imagen) {
        $url_imagen = $this->URL_BASE_PATH.Constant::$ARCHIVO_IMAGEN_USUARIO;
        if (($nombre_imagen != null) && (file_exists($this->DIR_BASE_PATH.Constant::$DIRECTORIO_IMAGENES_USUARIOS.$nombre_imagen))) {
            $url_imagen = $this->URL_BASE_PATH.Constant::$DIRECTORIO_IMAGENES_USUARIOS.$nombre_imagen;
        }
        return $url_imagen;
    }
}
?>