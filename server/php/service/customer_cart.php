<?php
/**
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 19/07/2018
 * Time: 04:54 PM
 */

require_once("../config/constant.php");
require_once("../model/cliente.php");
require_once("../model/configuracion.php");
require_once("../model/pago.php");
require_once("../model/paquete.php");
require_once("../model/pago_paquete.php");
require_once("../lib/mercadopago/mercadoPago.php");
require_once("../lib/utilities.php");
require_once('../service/service.php');
class Customer_Cart extends Service {

    public function consultar() {
        $respuesta = array( 'exito' => false, 'mensaje' => '');

        $medios = array();
        // Mercado pago
        $configuracion = Configuracion::consultar_configuracion_por_nombre(Constant::$MERCADO_PAGO_ACEPTADO);
        if ($configuracion != null && $configuracion['valor'] == Constant::$REGISTRO_ACTIVO) {
            $medio = array(
                "id" => Constant::$MERCADO_PAGO_ID,
                "nombre" => Constant::$MERCADO_PAGO_NOMBRE,
                "imagen" => $this->URL_BASE_PATH.Constant::$MERCADO_PAGO_IMAGEN
            );
            array_push($medios, $medio);
        }
        $respuesta['exito'] = true;
        $respuesta['data'] = $medios;
        return $respuesta;
    }

    public function pagar($medio_pago, $usuario) {
        $respuesta = array( 'exito' => false, 'mensaje' => '');

        $cliente = Cliente::consultar_cliente_por_id_usuario($usuario->id_usuario);
        if ($cliente != null) {
            $carro = $usuario->cart;
            if (count($carro) > 0) {
                // Cargar los items a comprar
                $data = $this->cargar_items($carro);
                $paquetes = $data['items'];
                $total = $data['total'];
                if (count($paquetes) > 0) {
                    // Registrar el Pago
                    $respuesta = $this->registrar_pago($cliente, $medio_pago, $total);
                    if ($respuesta["exito"]) {
                        // Registrar los Paquetes del Pago
                        $id_pago = $respuesta["codigo"];
                        $id_referencia = $respuesta["data"]["id_referencia"];
                        $respuesta = $this->registrar_paquetes_pago($id_pago, $paquetes);
                        if ($respuesta['exito']) {
                            if ($total <= 0) {
                                // Procesar compra gratuita
                            }
                            elseif ($medio_pago == Constant::$MERCADO_PAGO_ID) {
                                $mercadopago = $this->get_mercadopago_object();
                                $preferences_data = $this->get_mercadopago_preferences($usuario, $id_referencia, $paquetes);
                                $preferences = $mercadopago->create_preference($preferences_data);
                                $form = $this->get_mercadopago_form($preferences);
                                $respuesta["data"] = $form;
                            }
                        }
                    }
                }
            }
            else {
                $respuesta['mensaje'] = 'Es necesario incluir items al carro de compras.';
            }
        }
        else {
            $respuesta['mensaje'] = 'No se encontro un cliente asociado a este usuario.';
        }
        return $respuesta;
    }

    private function cargar_items($carro) {
        $items = array();
        $total = 0;
        foreach ($carro as $id => $value) {
            $cantidad = $value->quantity;
            $paquete = Paquete::consultar_paquete_por_id($id, Constant::$REGISTRO_ACTIVO);
            if ($paquete != null) {
                $item = array(
                    "id" => $paquete['id_paquete'],
                    "nombre" => $paquete['nombre'],
                    "categoria" => $paquete['_categoria_nombre'],
                    "cantidad" => $cantidad,
                    "precio" => $paquete['precio'],
                    "llamadas" => $paquete['llamadas'],
                    "minutos" => $paquete['minutos'],
                    "dias" => (isset($paquete['dias']) ? $paquete['dias'] : NULL),
                    "subtotal" => ($cantidad * $paquete['precio'])
                );
                array_push($items, $item);
                $total += ($cantidad * $paquete['precio']);
            }
        }
        return array(
            "items" => $items,
            "total" => $total
        );
    }

    private function registrar_pago($cliente, $medio_pago, $total) {
        // Generar un nuevo ID de referencia
        $id_referencia = Utilities::random_word(12);
        while (Pago::consultar_pago_por_referencia($id_referencia) != null) {
            $id_referencia = Utilities::random_word(12);
        }

        $pago = array(
            "id_referencia" => $id_referencia,
            "id_cliente" => $cliente['id_cliente'],
            "total" => $total,
            "medio_creador" => Constant::$APLICACION_MEDIO_CREADOR,
            "medio_pago" => $medio_pago
        );
        return Pago::registrar_pago($pago);
    }

    private function registrar_paquetes_pago($id_pago, $paquetes) {
        $respuesta = array( 'exito' => false, 'mensaje' => '');

        foreach ($paquetes as $paquete) {
            $pago_paquete = array(
                'id_pago' => $id_pago,
                'id_paquete' => $paquete['id'],
                'cantidad' => $paquete['cantidad'],
                'precio' => $paquete['precio'],
                'llamadas' => $paquete['llamadas'],
                'minutos' => $paquete['minutos'],
                'dias' => $paquete['dias'],
                'subtotal' => $paquete['subtotal']
            );
            $respuesta = Pago_Paquete::registrar_pago_paquete($pago_paquete);
            if ($respuesta['exito'] == false) {
                break;
            }
        }
        return $respuesta;
    }

    private function get_mercadopago_object() {
        $client_id = Configuracion::consultar_configuracion_por_nombre(Constant::$MERCADO_PAGO_CLIENT_ID);
        $client_secret = Configuracion::consultar_configuracion_por_nombre(Constant::$MERCADO_PAGO_CLIENT_SECRET);
        $use_sandbox = Configuracion::consultar_configuracion_por_nombre(Constant::$MERCADO_PAGO_USA_SANDBOX);
        $config = array (
            'client_id'     => $client_id['valor'],
            'client_secret' => $client_secret['valor'],
            'sandbox_mode'  => ($use_sandbox['valor'] == Constant::$REGISTRO_ACTIVO)
        );
        return new Mercadopago($config);
    }

    private function get_mercadopago_preferences($usuario, $id_referencia, $paquetes)  {
        $items = array();
        foreach ($paquetes as $paquete) {
            $item = array(
                "id" => $paquete["id"],
                "title" => $paquete["nombre"],
                "currency_id" => "ARS",
                "picture_url" => "https://www.mercadopago.com/org-img/MP3/home/logomp3.gif",
                "description" => $paquete["llamadas"]." llamadas. ".$paquete["minutos"]." minutos por llamada. Valido ".(($paquete["dias"]==NULL)?"hasta su uso.":"por ".$paquete["dias"]." dias."),
                "category_id" => "Paquete de ".$paquete["categoria"],
                "quantity" => intval($paquete["cantidad"]),
                "unit_price" => floatval($paquete["precio"])
            );
            array_push($items, $item);
        }
        $preference_data = array(
            "items" => $items,
            "payer" => array(
                "name" => $usuario->nombres,
                "surname" => $usuario->apellidos,
                "email" => $usuario->correo,
                "date_created" => "",
                "phone" => array(
                    "area_code" => "",
                    "number" => $usuario->telefono
                ),
                "identification" => array(
                    "type" => "",
                    "number" => ""
                ),
                "address" => array(
                    "street_name" => "",
                    "street_number" => "",
                    "zip_code" => ""
                )
            ),
            "back_urls" => array(
                "success" => $this->URL_BASE_PATH.Constant::$PAGINA_PAGO_EXITOSO,
                "failure" => $this->URL_BASE_PATH.Constant::$PAGINA_PAGO_FALLIDO,
                "pending" => $this->URL_BASE_PATH.Constant::$PAGINA_PAGO_PENDIENTE
            ),
            "auto_return" => "approved",
            "payment_methods" => array(
                "excluded_payment_methods" => array(
                ),
                "excluded_payment_types" => array(
                ),
                "installments" => 24,
                "default_payment_method_id" => null,
                "default_installments" => null,
            ),
            "shipments" => array(
                "receiver_address" => array(
                    "zip_code" => "",
                    "street_number"=> null,
                    "street_name"=> "",
                    "floor"=> null,
                    "apartment"=> ""
                )
            ),
            "notification_url" => $this->URL_BASE_PATH.Constant::$MERCADO_PAGO_NOTIFICACION_PAGOS,
            "external_reference" => $id_referencia,
            "expires" => false,
            "expiration_date_from" => null,
            "expiration_date_to" => null
        );
        return $preference_data;
    }

    private function get_mercadopago_form($preferences) {
        $configuracion = Configuracion::consultar_configuracion_por_nombre(Constant::$MERCADO_PAGO_USA_SANDBOX);
        $is_production = ($configuracion != null && $configuracion['valor'] == Constant::$REGISTRO_INACTIVO);

        $formData =
            '<a '.
            'href="'.($is_production?$preferences["response"]["init_point"]:$preferences["response"]["sandbox_init_point"]).'" '.
            'id="MP-Checkout" name="MP-Checkout" class="orange-ar-m-sq-arall" mp-mode="modal">&nbsp;</a>'.
            '<script type="text/javascript" src="//resources.mlstatic.com/mptools/render.js"></script>';
        return $formData;
    }
}
?>