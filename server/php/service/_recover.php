<?php
/**
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 23/07/2018
 * Time: 09:25 AM
 */

require_once("config/constant.php");
require_once("config/mailer.php");
require_once("model/usuario.php");
require_once("service/service.php");
class _Recover extends Service {

    public function resetear_clave($email) {
        $respuesta = array( 'exito' => false, 'mensaje' => '');

        $usuario = Usuario::consultar_usuario_por_id($email);
        if (isset($usuario)) {
            if ($usuario['estado'] == Constant::$REGISTRO_ACTIVO) {
                $respuesta = Usuario::resetear_clave($usuario);
                if ($respuesta["exito"]) {
                    $clave_sin_codificar = $respuesta['data']['clave_sin_codificar'];
                    Mailer::send_recover_password_mail($usuario['id_usuario'], $clave_sin_codificar, $usuario['nombres'], $usuario['apellidos']);
                }
            }
            else if ($usuario['estado'] == Constant::$REGISTRO_POR_VERIFICAR) {
                $respuesta['mensaje'] = 'Su usuario necesita ser verificado.';
            }
            else if ($usuario['estado'] == Constant::$REGISTRO_INACTIVO) {
                $respuesta['mensaje'] = 'Su usuario se encuentra temporalmente bloqueado.';
            }
        }
        else {
            $respuesta['mensaje'] = 'No se encontro un usuario con este correo.';
        }

        return $respuesta;
    }
}
?>