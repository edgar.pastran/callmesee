<?php
/**
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 30/07/2018
 * Time: 12:57 AM
 */

require_once("config/constant.php");
require_once("lib/OpenTok/OpenTokSDK.php");
require_once("lib/OpenTok/OpenTokArchive.php");
require_once("lib/OpenTok/OpenTokSession.php");
require_once("model/asesor.php");
require_once("model/cliente.php");
require_once("model/configuracion.php");
require_once("model/consulta.php");
require_once('service/service.php');
class _VideoCall extends Service {

    public function consultar($usuario, $id_consulta) {
        $respuesta = array( 'exito' => false, 'mensaje' => '');

        $id_usuario = $usuario->id_usuario;
        $id_asesor = null;
        $id_cliente = null;
        $asesor = Asesor::consultar_asesor_por_id_usuario($id_usuario);
        if (isset($asesor)) {
            $id_asesor = $asesor['id_asesor'];
        }
        else {
            $cliente = Cliente::consultar_cliente_por_id_usuario($id_usuario);
            if (isset($cliente)) {
                $id_cliente = $cliente['id_cliente'];
            }
        }

        if ($id_asesor != null || $id_cliente != null) {
            $consulta = Consulta::consultar_consulta_por_id($id_consulta);
            if (isset($consulta)) {
                if ($id_asesor == $consulta['id_asesor'] || $id_cliente = $consulta['id_cliente']) {
                    if ($consulta['condicion'] == Constant::$CONSULTA_CONDICION_ACEPTADA) {
                        if ($consulta['id_sesion'] != null) {
                            $id_sesion = $consulta['id_sesion'];
                            $respuesta = self::get_tokbox_credentials($id_sesion);
                            if ($respuesta['exito']) {
                                $respuesta['data']['segundos'] = $consulta["segundos_maximo"];
                            }
                        }
                        else {
                            $respuesta['mensaje'] = "Esta consulta no tiene un ID de sesion asignado";
                        }
                    }
                    else {
                        $respuesta['mensaje'] = "Esta consulta no ha sido aceptada";
                    }
                }
                else {
                    $respuesta['mensaje'] = "Su usuario no esta invitado a esta videollamada";
                }
            }
            else {
                $respuesta['mensaje'] = "No se encontraron datos de la consulta";
            }
        }
        else {
            $respuesta['mensaje'] = "No se encontraron datos de su usuario";
        }

        return $respuesta;
    }

    public function guardar($usuario, $id_consulta) {
        $respuesta = array( 'exito' => false, 'mensaje' => '');

        $id_usuario = $usuario->id_usuario;
        $id_asesor = null;
        $id_cliente = null;
        $asesor = Asesor::consultar_asesor_por_id_usuario($id_usuario);
        if (isset($asesor)) {
            $id_asesor = $asesor['id_asesor'];
        }
        else {
            $cliente = Cliente::consultar_cliente_por_id_usuario($id_usuario);
            if (isset($cliente)) {
                $id_cliente = $cliente['id_cliente'];
            }
        }

        if ($id_asesor != null || $id_cliente != null) {
            $consulta = Consulta::consultar_consulta_por_id($id_consulta);
            if (isset($consulta)) {
                if ($id_asesor == $consulta['id_asesor'] || $id_cliente = $consulta['id_cliente']) {
                    if ($consulta['condicion'] == Constant::$CONSULTA_CONDICION_ACEPTADA) {
                        $respuesta = Consulta::actualizar_fin_llamada($id_consulta);
                    }
                    else {
                        $respuesta['mensaje'] = "Esta consulta no ha sido aceptada";
                    }
                }
                else {
                    $respuesta['mensaje'] = "Su usuario no esta invitado a esta videollamada";
                }
            }
            else {
                $respuesta['mensaje'] = "No se encontraron datos de la consulta";
            }
        }
        else {
            $respuesta['mensaje'] = "No se encontraron datos de su usuario";
        }

        return $respuesta;
    }

    private function get_tokbox_credentials($session_id) {
        $respuesta = array( 'exito' => false, 'mensaje' => '');

        $configuracion = Configuracion::consultar_configuracion_por_nombre(Constant::$TOKBOX_API_KEY);
        if (isset($configuracion)) {
            $api_key = $configuracion['valor'];
            $configuracion = Configuracion::consultar_configuracion_por_nombre(Constant::$TOKBOX_API_SECRET);
            if (isset($configuracion)) {
                $api_secret = $configuracion['valor'];
                // Creating an OpenTok Object
                $apiObj = new OpenTokSDK($api_key, $api_secret);
                // Creating Simple Session object, passing IP address to determine closest production server
                // Enable p2p connections
                /*
                $session = $apiObj->createSession( $_SERVER["REMOTE_ADDR"], array(SessionPropertyConstants::P2P_PREFERENCE=> "enabled") );
                $session_id = $session->getSessionId();
                */
                // After creating a session, call generateToken(). Require parameter: SessionId
                // Giving the token a publisher role, expire time 1 hour from now, and connectionData to pass to other users in the session
                $token = $apiObj->generateToken($session_id, RoleConstants::PUBLISHER, time() + (60*60), "hello world!" );
                $data = array(
                    "apiKey" => $api_key,
                    "sessionId" => $session_id,
                    "token" => $token
                );
                $respuesta['exito'] = true;
                $respuesta['data'] = $data;
            }
            else {
                $respuesta['mensaje'] = "No se encontro el TokBox API secret";
            }
        }
        else {
            $respuesta['mensaje'] = "No se encontro el TokBox API key";
        }

        return $respuesta;
    }
}
?>