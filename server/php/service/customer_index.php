<?php
/**
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 26/07/2018
 * Time: 03:22 PM
 */

require_once("../config/constant.php");
require_once("../model/categoria.php");
require_once("../model/cliente.php");
require_once("../model/compra.php");
require_once('../service/service.php');
class Customer_Index extends Service {

    public function consultar($usuario) {
        $respuesta = array( 'exito' => false, 'mensaje' => '');

        $cliente = Cliente::consultar_cliente_por_id_usuario($usuario->id_usuario);
        if (isset($cliente)) {
            $id_cliente = $cliente['id_cliente'];
            $categorias = Compra::consultar_categorias_compras_disponibles_por_cliente($id_cliente);
            $llamadas = Compra::consultar_compras_disponibles_por_cliente($id_cliente);
            for ($i=0; $i<count($llamadas); $i++) {
                $llamadas[$i]['_fecha_vencimiento_format'] = null;
                if ($llamadas[$i]['fecha_vencimiento'] != null) {
                    $llamadas[$i]['_fecha_vencimiento_format'] = date('d/m/Y \a \l\a\s H:i:s', strtotime($llamadas[$i]['fecha_vencimiento']));
                }
            }
            $data = array(
                "categorias" => $categorias,
                "llamadas" => $llamadas
            );
            $respuesta['exito'] = true;
            $respuesta['data'] = $data;
        }
        else {
            $respuesta['mensaje'] = "Cliente no encontrado";
        }

        return $respuesta;
    }
}
?>
