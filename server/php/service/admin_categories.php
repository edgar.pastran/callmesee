<?php
/**
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 23/07/2018
 * Time: 10:08 AM
 */

require_once("../config/constant.php");
require_once("../model/categoria.php");
require_once('../service/service.php');
class Admin_Categories extends Service {

    public function consultar() {
        $respuesta = array( 'exito' => false, 'mensaje' => '');

        $data = Categoria::consultar_categorias();
        $respuesta['exito'] = true;
        $respuesta['data'] = $data;

        return $respuesta;
    }

    public function nuevo($categoria) {
        return Categoria::registrar_categoria($categoria);
    }

    public function editar($categoria) {
        return Categoria::actualizar_categoria($categoria);
    }

    public function desactivar($id_categoria) {
        return Categoria::desactivar_categoria($id_categoria);
    }

    public function activar($id_categoria) {
        return Categoria::activar_categoria($id_categoria);
    }
}
?>