<?php
/**
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 23/07/2018
 * Time: 08:50 AM
 */

require_once("config/constant.php");
require_once("lib/fileUploader.php");
require_once("model/usuario.php");
require_once('service/service.php');
class _Profile extends Service {

    public function actualizar_datos_basicos($usuario) {
        $respuesta = Usuario::actualizar_usuario($usuario);
        if ($respuesta["exito"]) {
            $usuario->nombre_completo = $usuario->nombres." ".$usuario->apellidos;
            $respuesta['usuario'] = $usuario;
        }

        return $respuesta;
    }

    public function cambiar_clave($usuario) {
        $respuesta = array( 'exito' => false, 'mensaje' => '');

        $usuario_consultado = Usuario::consultar_usuario_por_id_clave($usuario->id_usuario, $usuario->clave);
        if (isset($usuario_consultado)) {
            $respuesta = Usuario::actualizar_clave($usuario->id_usuario, $usuario->clave_nueva);
        }
        else {
            $respuesta['mensaje'] = 'Clave actual incorrecta.';
        }

        return $respuesta;
    }

    public function cambiar_imagen($id_usuario) {
        $respuesta = array( 'exito' => false, 'mensaje' => '');

        $image_field_name = 'imagen';
        if (isset($_FILES[$image_field_name]['name']) && $_FILES[$image_field_name]['name'] != '') {
            $image_directory = $this->DIR_BASE_PATH.Constant::$DIRECTORIO_IMAGENES_USUARIOS;
            $respuesta = FileUploader::upload_image($image_field_name, $image_directory, $id_usuario);
            if ($respuesta["exito"]) {
                // Actualizar el campo imagen del usuario
                $imagen_usuario = $respuesta["file_name"];
                $respuesta = Usuario::actualizar_imagen($id_usuario, $imagen_usuario);
                if ($respuesta["exito"]) {
                    $usuario["imagen"] = $this->URL_BASE_PATH.Constant::$DIRECTORIO_IMAGENES_USUARIOS.$imagen_usuario;
                    $respuesta['usuario'] = $usuario;
                }
            }
        }
        else {
            $respuesta['mensaje'] = 'Es necesario seleccionar el archivo.';
        }

        return $respuesta;
    }

    public function remover_imagen($usuario) {
        $respuesta = array( 'exito' => false, 'mensaje' => '');

        $usuario_consultado = Usuario::consultar_usuario_por_id($usuario->id_usuario);
        if (isset($usuario_consultado)) {
            $imagen_usuario = "";
            if (isset($usuario_consultado["imagen"])) {
                $imagen_usuario = $this->DIR_BASE_PATH.Constant::$DIRECTORIO_IMAGENES_USUARIOS.($usuario_consultado["imagen"]);
            }
            $respuesta = Usuario::actualizar_imagen($usuario->id_usuario, null);
            if ($respuesta["exito"]) {
                if ($imagen_usuario != "" && file_exists($imagen_usuario)) {
                    unlink($imagen_usuario);
                }
                $usuario->imagen = $this->URL_BASE_PATH.Constant::$ARCHIVO_IMAGEN_USUARIO;
                $respuesta['usuario'] = $usuario;
            }
        }
        else {
            $respuesta['mensaje'] = 'Usuario no encontrado.';
        }

        return $respuesta;
    }
}
?>