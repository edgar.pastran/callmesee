<?php
/**
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 26/07/2018
 * Time: 03:22 PM
 */

require_once("../config/constant.php");
require_once("../lib/OpenTok/OpenTokSDK.php");
require_once("../lib/OpenTok/OpenTokArchive.php");
require_once("../lib/OpenTok/OpenTokSession.php");
require_once("../model/asesor.php");
require_once("../model/cliente.php");
require_once("../model/configuracion.php");
require_once("../model/consulta.php");
require_once("../model/consulta_pregunta.php");
require_once("../model/consulta_respuesta.php");
require_once('../service/service.php');
class Adviser_Consultation extends Service {

    public function consultar($usuario, $id_consulta) {
        $respuesta = array( 'exito' => false, 'mensaje' => '');

        $asesor = Asesor::consultar_asesor_por_id_usuario($usuario->id_usuario);
        if (isset($asesor)) {
            $consulta = Consulta::consultar_consulta_por_id($id_consulta);
            if (isset($consulta)) {
                $id_asesor = $asesor["id_asesor"];
                if ($id_asesor == $consulta["id_asesor"]) {
                    $id_cliente = $consulta["id_cliente"];
                    $cliente = Cliente::consultar_cliente_por_id($id_cliente);
                    if (isset($cliente)) {
                        $cliente['_imagen'] = $this->get_cliente_imagen($cliente['_imagen']);
                        $consulta["preguntas"] = Consulta_Pregunta::consultar_consulta_preguntas_por_consulta($id_consulta);
                        for ($i=0; $i<count($consulta["preguntas"]); $i++) {
                            $id_consulta_pregunta = $consulta["preguntas"][$i]["id_consulta_pregunta"];
                            $consulta["preguntas"][$i]["respuestas"] = Consulta_Respuesta::consultar_consulta_respuestas_por_consulta_pregunta($id_consulta_pregunta);
                        }
                        $data = array(
                            "cliente" => $cliente,
                            "consulta" => $consulta
                        );
                        $respuesta['exito'] = true;
                        $respuesta['data'] = $data;
                    }
                    else {
                        $respuesta['mensaje'] = "Cliente no encontrado";
                    }
                }
                else {
                    $respuesta['mensaje'] = "Esta consulta no esta dirigida a su usuario";
                }
            }
            else {
                $respuesta['mensaje'] = "Consulta no encontrada";
            }
        }
        else {
            $respuesta['mensaje'] = "Asesor no encontrado";
        }

        return $respuesta;
    }

    public function rechazar_consulta($usuario, $id_consulta) {
        $respuesta = array( 'exito' => false, 'mensaje' => '');

        $asesor = Asesor::consultar_asesor_por_id_usuario($usuario->id_usuario);
        if (isset($asesor)) {
            $consulta = Consulta::consultar_consulta_por_id($id_consulta);
            if (isset($consulta)) {
                $id_asesor = $asesor["id_asesor"];
                if ($id_asesor == $consulta["id_asesor"]) {
                    $respuesta = Consulta::rechazar_consulta($id_consulta);
                }
                else {
                    $respuesta['mensaje'] = "Esta consulta no esta dirigida a su usuario";
                }
            }
            else {
                $respuesta['mensaje'] = "Consulta no encontrada";
            }
        }
        else {
            $respuesta['mensaje'] = "Asesor no encontrado";
        }

        return $respuesta;
    }

    public function aceptar_consulta($usuario, $id_consulta) {
        $respuesta = array( 'exito' => false, 'mensaje' => '');

        $asesor = Asesor::consultar_asesor_por_id_usuario($usuario->id_usuario);
        if (isset($asesor)) {
            $consulta = Consulta::consultar_consulta_por_id($id_consulta);
            if (isset($consulta)) {
                $id_asesor = $asesor["id_asesor"];
                if ($id_asesor == $consulta["id_asesor"]) {
                    $id_sesion = self::get_id_sesion();
                    if ($id_sesion != null) {
                        $respuesta = Consulta::aceptar_consulta($id_consulta, $id_sesion);
                        if ($respuesta['exito']) {
                            $respuesta = Asesor::colocar_no_disponible_asesor($id_asesor);
                        }
                    }
                    else {
                        $respuesta['mensaje'] = "No se pudo crear un ID de sesion para la video llamada";
                    }
                }
                else {
                    $respuesta['mensaje'] = "Esta consulta no esta dirigida a su usuario";
                }
            }
            else {
                $respuesta['mensaje'] = "Consulta no encontrada";
            }
        }
        else {
            $respuesta['mensaje'] = "Asesor no encontrado";
        }

        return $respuesta;
    }

    public function guardar_comentarios_llamada($consulta) {
        $respuesta = Consulta::actualizar_consulta($consulta);
        if ($respuesta['exito']) {
            $id_asesor = $respuesta['data']['id_asesor'];
            $respuesta = Asesor::colocar_disponible_asesor($id_asesor);
        }

        return $respuesta;
    }

    private function get_id_sesion() {
        $id_sesion = null;

        $api_key = self::get_tokbox_api_key();
        $api_secret = self::get_tokbox_api_secret();
        if ($api_key != null && $api_secret != null) {
            // Creating an OpenTok Object
            $apiObj = new OpenTokSDK($api_key, $api_secret);
            // Creating Simple Session object, passing IP address to determine closest production server
            // Enable p2p connections
            $session = $apiObj->createSession( $_SERVER["REMOTE_ADDR"], array(SessionPropertyConstants::P2P_PREFERENCE=> "enabled") );
            $id_sesion = $session->getSessionId();
        }

        return $id_sesion;
    }

    private function get_tokbox_api_key() {
        $configuracion = Configuracion::consultar_configuracion_por_nombre(Constant::$TOKBOX_API_KEY);
        return isset($configuracion)?$configuracion['valor']:null;
    }

    private function get_tokbox_api_secret() {
        $configuracion = Configuracion::consultar_configuracion_por_nombre(Constant::$TOKBOX_API_SECRET);
        return isset($configuracion)?$configuracion['valor']:null;
    }

    private function get_cliente_imagen($nombre_imagen) {
        $url_imagen = $this->URL_BASE_PATH.Constant::$ARCHIVO_IMAGEN_USUARIO;
        if (($nombre_imagen != null) && (file_exists($this->DIR_BASE_PATH.Constant::$DIRECTORIO_IMAGENES_USUARIOS.$nombre_imagen))) {
            $url_imagen = $this->URL_BASE_PATH.Constant::$DIRECTORIO_IMAGENES_USUARIOS.$nombre_imagen;
        }
        return $url_imagen;
    }

}
?>

