<?php
/**
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 23/07/2018
 * Time: 09:30 AM
 */

require_once("config/constant.php");
require_once("config/mailer.php");
require_once("model/usuario.php");
require_once("service/service.php");
class _Resend extends Service {

    public function reenviar_correo($email) {
        $respuesta = array( 'exito' => false, 'mensaje' => '');

        $usuario = Usuario::consultar_usuario_por_id($email, Constant::$REGISTRO_POR_VERIFICAR);
        if (isset($usuario)) {
            $respuesta = Usuario::actualizar_codigo_verificacion($usuario['id_usuario']);
            if ($respuesta["exito"]) {
                $codigo_verificacion = $respuesta['data']['codigo_verificacion'];
                $url_verificacion = $this->URL_BASE_PATH.Constant::$PAGINA_VERIFICACION_USUARIO;
                Mailer::send_verification_mail($usuario["correo"], $url_verificacion, $codigo_verificacion, $usuario["nombres"], $usuario["apellidos"]);
            }
        }
        else {
            $respuesta['mensaje'] = 'No se encontro un usuario con registro por verificar asociado a este correo.';
        }

        return $respuesta;
    }
}
?>