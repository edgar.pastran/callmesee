<?php
/**
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 19/07/2018
 * Time: 05:02 PM
 */
class Service {

    public function __construct() {
        $protocol = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
        $hostName = $_SERVER['HTTP_HOST'];
        $currentPath = $_SERVER['PHP_SELF'];
        $dirPath = $_SERVER['DOCUMENT_ROOT'];
        $pathInfo = pathinfo($currentPath);
        $directories = explode("/", $pathInfo['dirname']);
        $root_directory = $directories[1];
        $isLocal = $this->is_local_version();

        $this->DIR_BASE_PATH = $dirPath."/".($isLocal?$root_directory."/":"");
        $this->URL_BASE_PATH = $protocol.$hostName."/".($isLocal?$root_directory."/":"");
    }

    protected function is_local_version() {
        $hostName = $_SERVER['HTTP_HOST'];
        return (
            ($hostName == "localhost") ||
            (strpos($hostName, "10.") !== false) ||
            (strpos($hostName, "pablotest") !== false)
        );
    }

    protected function is_development_version() {
        return (
          $this->is_local_version()
        );
    }
}
?>