<?php
/**
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 23/07/2018
 * Time: 09:41 AM
 */

require_once("config/constant.php");
require_once("config/mailer.php");
require_once("model/cliente.php");
require_once("model/usuario.php");
require_once("service/service.php");
class _Verification extends Service {

    public function verificar_usuario($usuario) {
        $respuesta = array( 'exito' => false, 'mensaje' => '');

        $usuario = Usuario::consultar_usuario_por_id_codigo_verificacion($usuario->id_usuario, $usuario->codigo_verificacion);
        if (isset($usuario)) {
            $clave_sin_codificar = $usuario['clave'];
            $respuesta = Usuario::verificar_usuario($usuario);
            if ($respuesta["exito"]) {
                if ($usuario['id_rol'] == Constant::$ROL_CLIENTE) {
                    $respuesta = Cliente::registrar_cliente($usuario);
                }
                if ($respuesta["exito"]) {
                    Mailer::send_registration_mail($usuario['correo'], $clave_sin_codificar, $usuario['nombres'], $usuario['apellidos']);
                }
            }
        }
        else {
            $respuesta['mensaje'] = 'No se econcontro un usuario por verificar con este correo.';
        }

        return $respuesta;
    }
}
?>
