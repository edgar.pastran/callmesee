<?php
/**
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 23/07/2018
 * Time: 08:12 AM
 */

require_once("config/constant.php");
require_once("model/asesor.php");
require_once("model/usuario.php");
require_once('service/service.php');
class _Login extends Service {

    public function iniciar_sesion($usuario) {
        $respuesta = array( 'exito' => false, 'mensaje' => '');

        $usuario = Usuario::consultar_usuario_por_id_clave($usuario->id_usuario, $usuario->clave);
        if (isset($usuario)) {
            $respuesta = Usuario::actualizar_fecha_ultima_actividad($usuario['id_usuario']);
            if ($respuesta['exito']) {
                // Si es asesor colocarlo disponible
                if ($usuario['id_rol'] == Constant::$ROL_ASESOR) {
                    $asesor = Asesor::consultar_asesor_por_id_usuario($usuario['id_usuario']);
                    if (isset($asesor)) {
                        Asesor::colocar_disponible_asesor($asesor['id_asesor']);
                    }
                }
                // Armar los datos del usuario
                $imagen = $this->URL_BASE_PATH.Constant::$ARCHIVO_IMAGEN_USUARIO;
                if (($usuario['imagen'] != null) && (file_exists($this->DIR_BASE_PATH.Constant::$DIRECTORIO_IMAGENES_USUARIOS.$usuario['imagen']))) {
                    $imagen = $this->URL_BASE_PATH.Constant::$DIRECTORIO_IMAGENES_USUARIOS.$usuario['imagen'];
                }
                $usuario = array(
                    "id_usuario" => $usuario['id_usuario'],
                    "correo" => $usuario['correo'],
                    "nombres" => $usuario['nombres'],
                    "apellidos" => $usuario['apellidos'],
                    "nombre_completo" => $usuario['nombres']." ".$usuario['apellidos'],
                    "telefono" => (isset($usuario['telefono'])?$usuario['telefono']:""),
                    "imagen" => $imagen,
                    "directorio_vistas" => strtolower($usuario['id_rol'])."/"
                );
                $respuesta['usuario'] = $usuario;
            }
        }
        else {
            $respuesta['mensaje'] = 'Combinacion de Correo y Clave incorrecta.';
        }
        return $respuesta;
    }
}
?>