<?php
/**
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 26/07/2018
 * Time: 03:22 PM
 */

require_once("../config/constant.php");
require_once("../model/paquete.php");
require_once('../service/service.php');
class Customer_Package extends Service {

    public function consultar($id_paquete) {
        $respuesta = array( 'exito' => false, 'mensaje' => '');

        $paquete = Paquete::consultar_paquete_por_id($id_paquete, Constant::$REGISTRO_ACTIVO);
        $respuesta['exito'] = true;
        $respuesta['data'] = $paquete;

        return $respuesta;
    }
}
?>
