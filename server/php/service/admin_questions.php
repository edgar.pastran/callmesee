<?php
/**
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 23/07/2018
 * Time: 03:12 PM
 */

require_once("../config/constant.php");
require_once("../model/categoria.php");
require_once("../model/pregunta.php");
require_once("../model/respuesta.php");
require_once('../service/service.php');
class Admin_Questions extends Service {

    public function consultar() {
        $respuesta = array( 'exito' => false, 'mensaje' => '');

        $respuesta['exito'] = true;
        $respuesta['data'] = Pregunta::consultar_preguntas();

        return $respuesta;
    }

    public function consultar_categorias() {
        $respuesta = array( 'exito' => false, 'mensaje' => '');

        $respuesta['exito'] = true;
        $respuesta['data'] = Categoria::consultar_categorias(Constant::$REGISTRO_ACTIVO);

        return $respuesta;
    }

    public function consultar_respuestas($id_pregunta) {
        $respuesta = array( 'exito' => false, 'mensaje' => '');

        $respuesta['exito'] = true;
        $respuesta['data'] = Respuesta::consultar_respuestas_por_pregunta($id_pregunta);

        return $respuesta;
    }

    public function nuevo($pregunta) {
        return Pregunta::registrar_pregunta($pregunta);
    }

    public function nueva_respuesta($respuesta) {
        return Respuesta::registrar_respuesta($respuesta);
    }

    public function editar($pregunta) {
        return Pregunta::actualizar_pregunta($pregunta);
    }

    public function editar_respuesta($respuesta) {
        return Respuesta::actualizar_respuesta($respuesta);
    }

    public function desactivar($id_pregunta) {
        return Pregunta::desactivar_pregunta($id_pregunta);
    }

    public function desactivar_respuesta($id_respuesta) {
        return Respuesta::desactivar_respuesta($id_respuesta);
    }

    public function activar($id_pregunta) {
        return Pregunta::activar_pregunta($id_pregunta);
    }

    public function activar_respuesta($id_respuesta) {
        return Respuesta::activar_respuesta($id_respuesta);
    }
}
?>