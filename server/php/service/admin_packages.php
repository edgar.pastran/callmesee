<?php
/**
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 23/07/2018
 * Time: 02:56 PM
 */

require_once("../config/constant.php");
require_once("../model/categoria.php");
require_once("../model/paquete.php");
require_once('../service/service.php');
class Admin_Packages extends Service {

    public function consultar() {
        $respuesta = array( 'exito' => false, 'mensaje' => '');

        $data = Paquete::consultar_paquetes();
        $respuesta['exito'] = true;
        $respuesta['data'] = $data;

        return $respuesta;
    }

    public function consultar_categorias_tipos() {
        $respuesta = array( 'exito' => false, 'mensaje' => '');

        $categorias = Categoria::consultar_categorias(Constant::$REGISTRO_ACTIVO);
        $tipos = array(
            array(
                "id" => Constant::$TIPO_PAQUETE_POR_LLAMADAS,
                "value" => Constant::$TIPO_PAQUETE_POR_LLAMADAS
            ),
            array(
                "id" => Constant::$TIPO_PAQUETE_POR_SUSCRIPCION,
                "value" => Constant::$TIPO_PAQUETE_POR_SUSCRIPCION
            ),
            array(
                "id" => Constant::$TIPO_PAQUETE_GRATIS,
                "value" => Constant::$TIPO_PAQUETE_GRATIS
            )
        );
        $data = array(
            "categorias" => $categorias,
            "tipos" => $tipos
        );
        $respuesta['exito'] = true;
        $respuesta['data'] = $data;

        return $respuesta;
    }

    public function nuevo($paquete) {
        return Paquete::registrar_paquete($paquete);
    }

    public function editar($paquete) {
        return Paquete::actualizar_paquete($paquete);
    }

    public function desactivar($id_paquete) {
        return Paquete::desactivar_paquete($id_paquete);
    }

    public function activar($id_paquete) {
        return Paquete::activar_paquete($id_paquete);
    }
}
?>