<?php
/**
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 23/07/2018
 * Time: 09:37 AM
 */

require_once("config/constant.php");
require_once("config/mailer.php");
require_once("model/usuario.php");
require_once("service/service.php");
class _Signup extends Service {

    public function crear_usuario($usuario) {
        $respuesta = array( 'exito' => false, 'mensaje' => '');

        $otro_usuario = Usuario::consultar_usuario_por_id($usuario->id_usuario);
        if (isset($otro_usuario)) {
            $respuesta['mensaje'] = 'Ya existe un usuario con este correo.';
        }
        else {
            $respuesta = Usuario::registrar_usuario_rol_cliente($usuario);
            if ($respuesta["exito"]) {
                require_once("config/mailer.php");

                $url_verificacion = $this->URL_BASE_PATH.Constant::$PAGINA_VERIFICACION_USUARIO;
                $codigo_verificacion = $respuesta['data']->codigo_verificacion;
                Mailer::send_verification_mail($usuario->correo, $url_verificacion, $codigo_verificacion, $usuario->nombres, $usuario->apellidos);
            }
        }

        return $respuesta;
    }
}
?>