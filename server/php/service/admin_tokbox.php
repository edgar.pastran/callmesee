<?php
/**
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 23/07/2018
 * Time: 02:45 PM
 */

require_once("../config/constant.php");
require_once("../model/configuracion.php");
require_once('../service/service.php');
class Admin_TokBox extends Service {

    public function consultar() {
        $respuesta = array( 'exito' => false, 'mensaje' => '');

        $data = Configuracion::consultar_configuraciones('tokbox');
        $respuesta['exito'] = true;
        $respuesta['data'] = $data;

        return $respuesta;
    }

    public function guardar($configuraciones) {
        $respuesta = array( 'exito' => false, 'mensaje' => '');

        for ($i=0; $i<count($configuraciones); $i++) {
            $configuracion = $configuraciones[$i];
            $respuesta = Configuracion::guardar_configuracion($configuracion);
            if ($respuesta['exito'] == false) {
                break;
            }
        }
        if ($respuesta['exito']) {
            $data = Configuracion::consultar_configuraciones('tokbox');
            $respuesta['data'] = $data;
        }

        return $respuesta;
    }

}
?>