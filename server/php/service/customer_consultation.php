<?php
/**
 * Created by PhpStorm.
 * User: Edgar Pastran
 * Date: 26/07/2018
 * Time: 03:22 PM
 */

require_once("../config/constant.php");
require_once("../model/asesor.php");
require_once("../model/cliente.php");
require_once("../model/compra.php");
require_once("../model/consulta.php");
require_once("../model/consulta_pregunta.php");
require_once("../model/consulta_respuesta.php");
require_once("../model/mensaje.php");
require_once("../model/pregunta.php");
require_once("../model/respuesta.php");
require_once('../service/service.php');
class Customer_Consultation extends Service {

    public function consultar($usuario, $id_compra) {
        $respuesta = array( 'exito' => false, 'mensaje' => '');

        $cliente = Cliente::consultar_cliente_por_id_usuario($usuario->id_usuario);
        if (isset($cliente)) {
            $id_cliente = $cliente['id_cliente'];
            $compra = Compra::consultar_compra_disponible_por_cliente_id($id_cliente, $id_compra);
            if ($compra != null) {
                $id_categoria = $compra['_id_categoria'];
                // Preguntas
                $preguntas = Pregunta::consultar_preguntas_activas_por_categoria($id_categoria);
                for ($i=0; $i<count($preguntas); $i++) {
                    $id_pregunta = $preguntas[$i]['id_pregunta'];
                    $respuestas = Respuesta::consultar_respuestas_por_pregunta($id_pregunta, Constant::$REGISTRO_ACTIVO);
                    $preguntas[$i]['respuestas'] = $respuestas;
                }
                $compra['preguntas'] = $preguntas;
                // Asesores
                $asesores = Asesor::consultar_asesores_disponibles_por_categoria($id_categoria);
                for ($i=0; $i<count($asesores); $i++) {
                    $imagen = $this->URL_BASE_PATH.Constant::$ARCHIVO_IMAGEN_USUARIO;
                    if (($asesores[$i]['_imagen'] != null) && (file_exists($this->DIR_BASE_PATH.Constant::$DIRECTORIO_IMAGENES_USUARIOS.$asesores[$i]['_imagen']))) {
                        $imagen = $this->URL_BASE_PATH.Constant::$DIRECTORIO_IMAGENES_USUARIOS.$asesores[$i]['_imagen'];
                    }
                    $asesores[$i]['_imagen'] = $imagen;
                }
                $compra['asesores'] = $asesores;

                $respuesta['exito'] = true;
                $respuesta['data'] = $compra;
            }
            else {
                $respuesta['mensaje'] = "Compra no vigente o existente";
            }
        }
        else {
            $respuesta['mensaje'] = "Cliente no encontrado";
        }

        return $respuesta;
    }

    public function guardar($usuario, $consulta) {
        $respuesta = array( 'exito' => false, 'mensaje' => '');

        $cliente = Cliente::consultar_cliente_por_id_usuario($usuario->id_usuario);
        if (isset($cliente)) {
            $id_cliente = $cliente['id_cliente'];
            $id_compra = $consulta["_id_compra"];
            $compra = Compra::consultar_compra_disponible_por_cliente_id($id_cliente, $id_compra);
            if ($compra != null) {
                $id_asesor = $consulta["id_asesor"];
                $asesor = Asesor::consultar_asesor_por_id($id_asesor);
                if (isset($asesor) && $asesor["disponible"] == Constant::$SI) {
                    $id_categoria = $compra['_id_categoria'];
                    $minutos = $compra['_minutos'];

                    $consulta["id_cliente"] = $id_cliente;
                    $consulta["id_categoria"] = $id_categoria;
                    $consulta["segundos_maximo"] = $minutos * 60;
                    $respuesta = Consulta::registrar_consulta($consulta);
                    if ($respuesta['exito']) {
                        $id_consulta = $respuesta['codigo'];
                        $consulta_registrada = $respuesta['data'];
                        if (isset($consulta["preguntas"])) {
                            for ($i = 0; $i < count($consulta["preguntas"]); $i++) {
                                $id_pregunta = $consulta["preguntas"][$i]["id_pregunta"];
                                $enunciado = $consulta["preguntas"][$i]["enunciado"];
                                $consulta_pregunta = array(
                                    "id_consulta" => $id_consulta,
                                    "id_pregunta" => $id_pregunta,
                                    "enunciado" => $enunciado
                                );
                                $respuesta = Consulta_Pregunta::registrar_consulta_pregunta($consulta_pregunta);
                                if ($respuesta['exito']) {
                                    if (isset($consulta["preguntas"][$i]["respuestas"])) {
                                        $id_consulta_pregunta = $respuesta['codigo'];
                                        for ($j = 0; $j < count($consulta["preguntas"][$i]["respuestas"]); $j++) {
                                            $id_respuesta = $consulta["preguntas"][$i]["respuestas"][$j]["id_respuesta"];
                                            $enunciado = $consulta["preguntas"][$i]["respuestas"][$j]["enunciado"];
                                            $consulta_respuesta = array(
                                                "id_consulta_pregunta" => $id_consulta_pregunta,
                                                "id_respuesta" => $id_respuesta,
                                                "enunciado" => $enunciado
                                            );
                                            $respuesta = Consulta_Respuesta::registrar_consulta_respuesta($consulta_respuesta);
                                            if ($respuesta['exito'] == false) {
                                                break;
                                            }
                                        }
                                    }
                                }
                                else {
                                    break;
                                }
                            }
                        }
                        if ($respuesta['exito']) {
                            // Registrar el mensaje para el asesor
                            $mensaje = array(
                                "id_usuario_remitente" => $cliente['id_usuario'],
                                "id_usuario_destinatario" => $asesor['id_usuario'],
                                "id_consulta" => $id_consulta,
                                "titulo" => "Consulta de " . $consulta_registrada["_categoria_nombre"],
                                "texto" => $consulta_registrada["motivo"],
                                "url_apertura" => "adviser/consultation.html?id_consultation=" . $id_consulta
                            );
                            $respuesta = Mensaje::registrar_mensaje($mensaje);
                        }
                    }
                }
                else {
                    $respuesta['mensaje'] = "El operador no se encuentra disponible en este momento";
                }
            }
            else {
                $respuesta['mensaje'] = "La compra no fue encontrada o no esta vigente";
            }
        }
        else {
            $respuesta['mensaje'] = "Cliente no encontrado";
        }

        return $respuesta;
    }

    public function consultar_respuesta_asesor($id_consulta) {
        $respuesta = array( 'exito' => false, 'mensaje' => '');

        $consulta = Consulta::consultar_consulta_por_id($id_consulta);
        if (isset($consulta)) {
            $respuesta['exito'] = true;
            $respuesta['data'] = $consulta;
        }
        else {
            $respuesta['mensaje'] = "Consulta no encontrada";
        }

        return $respuesta;
    }

    public function guardar_inicio_llamada($id_compra, $id_consulta) {
        $respuesta = Compra::consumir_compra($id_compra, $id_consulta);

        if ($respuesta['exito']) {
            $respuesta = Consulta::actualizar_inicio_llamada($id_consulta);
        }

        return $respuesta;
    }

    public function guardar_comentarios_llamada($consulta) {
        return Consulta::actualizar_consulta($consulta);
    }

    public function expirar_consulta($id_consulta) {
        return Consulta::expirar_consulta($id_consulta);
    }
}
?>
