/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     31/07/2018 03:40:23 p.m.                     */
/*==============================================================*/


/*==============================================================*/
/* Table: ASESOR                                                */
/*==============================================================*/
create table ASESOR
(
   id_asesor            int not null auto_increment,
   id_usuario           varchar(50) not null,
   disponible           enum('SI', 'NO') not null default 'SI',
   estado               enum('ACTIVO', 'INACTIVO') not null default 'ACTIVO',
   primary key (id_asesor)
);

/*==============================================================*/
/* Table: ASESOR_CATEGORIAS                                     */
/*==============================================================*/
create table ASESOR_CATEGORIAS
(
   id_asesor            int not null,
   id_categoria         int not null,
   primary key (id_asesor, id_categoria)
);

/*==============================================================*/
/* Table: CATEGORIA                                             */
/*==============================================================*/
create table CATEGORIA
(
   id_categoria         int not null auto_increment,
   nombre               varchar(50) not null,
   descripcion          varchar(250),
   imagen               varchar(50),
   estado               enum('ACTIVO', 'INACTIVO') not null default 'ACTIVO',
   primary key (id_categoria)
);

/*==============================================================*/
/* Table: CLIENTE                                               */
/*==============================================================*/
create table CLIENTE
(
   id_cliente           int not null auto_increment,
   id_usuario           varchar(50) not null,
   saldo                double not null default 0,
   llamadas             int not null default 0,
   estado               enum('ACTIVO', 'INACTIVO') not null default 'ACTIVO',
   primary key (id_cliente)
);

/*==============================================================*/
/* Table: COMPRA                                                */
/*==============================================================*/
create table COMPRA
(
   id_compra            int not null auto_increment,
   id_pago              int not null,
   id_paquete           int not null,
   id_cliente           int not null,
   fecha                datetime not null default CURRENT_TIMESTAMP,
   fecha_vencimiento    datetime,
   consumido            enum('SI', 'NO') not null default 'NO',
   fecha_consumo        datetime,
   id_consulta          int,
   estado               enum('ACTIVO', 'INACTIVO') not null default 'ACTIVO',
   primary key (id_compra)
);

/*==============================================================*/
/* Table: CONFIGURACION                                         */
/*==============================================================*/
create table CONFIGURACION
(
   id_configuracion     int not null auto_increment,
   nombre               varchar(50) not null,
   valor                varchar(100) not null,
   primary key (id_configuracion)
);

/*==============================================================*/
/* Table: CONSULTA                                              */
/*==============================================================*/
create table CONSULTA
(
   id_consulta          int not null auto_increment,
   id_cliente           int not null,
   id_categoria         int not null,
   motivo               varchar(250) not null,
   fecha_creacion       datetime not null,
   segundos_maximo      int not null,
   id_asesor            int not null,
   condicion            enum('SIN CONTESTAR', 'ACEPTADA', 'RECHAZADA', 'ESPERA EXCEDIDA') not null default 'SIN CONTESTAR',
   fecha_cambio_condicion datetime,
   id_sesion            varchar(150),
   fecha_inicio_llamada datetime,
   fecha_fin_llamada    datetime,
   segundos             int,
   calificacion_cliente tinyint,
   comentario_cliente   varchar(250),
   calificacion_asesor  tinyint,
   comentario_asesor    varchar(250),
   estado               enum('ACTIVO', 'INACTIVO') not null default 'ACTIVO',
   primary key (id_consulta)
);

/*==============================================================*/
/* Table: CONSULTA_PREGUNTA                                     */
/*==============================================================*/
create table CONSULTA_PREGUNTA
(
   id_consulta_pregunta int not null auto_increment,
   id_consulta          int not null,
   id_pregunta          int not null,
   enunciado            varchar(250) not null,
   primary key (id_consulta_pregunta)
);

/*==============================================================*/
/* Table: CONSULTA_RESPUESTA                                    */
/*==============================================================*/
create table CONSULTA_RESPUESTA
(
   id_consulta_pregunta int not null,
   id_respuesta         int not null,
   enunciado            varchar(250) not null,
   primary key (id_consulta_pregunta, id_respuesta)
);

/*==============================================================*/
/* Table: MENSAJE                                               */
/*==============================================================*/
create table MENSAJE
(
   id_mensaje           int not null auto_increment,
   id_usuario_remitente varchar(50) not null,
   id_usuario_destinatario varchar(50) not null,
   id_consulta          int,
   fecha_creacion       datetime not null default CURRENT_TIMESTAMP,
   titulo               varchar(100) not null default '',
   texto                varchar(250) not null,
   url_apertura         varchar(250) not null,
   leido                enum('SI', 'NO') not null default 'NO',
   fecha_lectura        datetime,
   estado               enum('ACTIVO', 'INACTIVO') not null default 'ACTIVO',
   primary key (id_mensaje)
);

/*==============================================================*/
/* Table: MENU                                                  */
/*==============================================================*/
create table MENU
(
   id_menu              varchar(50) not null,
   id_rol               varchar(20) not null,
   tipo                 enum('CONTENEDOR', 'ITEM') not null,
   nombre               varchar(50) not null,
   nivel                tinyint not null,
   orden                tinyint not null,
   id_menu_padre        varchar(50),
   descripcion          varchar(250),
   archivo              varchar(100),
   icono                varchar(50),
   estado               enum('ACTIVO', 'INACTIVO') not null default 'ACTIVO',
   primary key (id_menu)
);

/*==============================================================*/
/* Table: PAGO                                                  */
/*==============================================================*/
create table PAGO
(
   id_pago              int not null auto_increment,
   id_referencia        varchar(12) not null,
   id_cliente           int not null,
   total                double not null,
   medio_creador        varchar(50) not null,
   medio_pago           varchar(50) not null,
   observacion          TEXT,
   fecha_creacion       datetime not null default CURRENT_TIMESTAMP,
   confirmado           enum('SI', 'NO') not null default 'NO',
   fecha_confirmacion   datetime,
   email_pagador        varchar(50),
   monto_pagado         double not null default 0,
   estado               enum('ACTIVO', 'INACTIVO') not null default 'ACTIVO',
   primary key (id_pago)
);

/*==============================================================*/
/* Index: INDEX_ID_REFERENCIA                                   */
/*==============================================================*/
create unique index INDEX_ID_REFERENCIA on PAGO
(
   id_referencia
);

/*==============================================================*/
/* Table: PAGO_PAQUETES                                         */
/*==============================================================*/
create table PAGO_PAQUETES
(
   id_pago              int not null,
   id_paquete           int not null,
   cantidad             int not null,
   precio               double not null,
   llamadas             int not null,
   minutos              int not null,
   dias                 int,
   subtotal             double not null,
   estado               enum('ACTIVO', 'INACTIVO') not null default 'ACTIVO',
   primary key (id_pago, id_paquete)
);

/*==============================================================*/
/* Table: PAQUETE                                               */
/*==============================================================*/
create table PAQUETE
(
   id_paquete           int not null auto_increment,
   id_categoria         int not null,
   nombre               varchar(50) not null,
   tipo                 enum('POR LLAMADAS', 'POR SUSCRIPCION', 'GRATIS') not null,
   precio               double not null,
   llamadas             int not null,
   minutos              int not null,
   dias                 int,
   estado               enum('ACTIVO', 'INACTIVO') not null default 'ACTIVO',
   primary key (id_paquete)
);

/*==============================================================*/
/* Table: PREGUNTA                                              */
/*==============================================================*/
create table PREGUNTA
(
   id_pregunta          int not null auto_increment,
   id_categoria         int not null,
   enunciado            varchar(250) not null,
   orden                tinyint not null,
   estado               enum('ACTIVO', 'INACTIVO') not null default 'ACTIVO',
   primary key (id_pregunta)
);

/*==============================================================*/
/* Table: RESPUESTA                                             */
/*==============================================================*/
create table RESPUESTA
(
   id_respuesta         int not null auto_increment,
   id_pregunta          int not null,
   enunciado            varchar(250) not null,
   orden                tinyint not null,
   estado               enum('ACTIVO', 'INACTIVO') not null default 'ACTIVO',
   primary key (id_respuesta)
);

/*==============================================================*/
/* Table: ROL                                                   */
/*==============================================================*/
create table ROL
(
   id_rol               varchar(20) not null,
   descripcion          varchar(250),
   estado               enum('ACTIVO', 'INACTIVO') not null default 'ACTIVO',
   primary key (id_rol)
);

/*==============================================================*/
/* Table: USUARIO                                               */
/*==============================================================*/
create table USUARIO
(
   id_usuario           varchar(50) not null,
   clave                varchar(50) not null,
   correo               varchar(50) not null,
   nombres              varchar(50) not null,
   apellidos            varchar(50) not null,
   telefono             varchar(20),
   imagen               varchar(50),
   id_rol               varchar(20),
   super_usuario        enum('SI', 'NO') not null default 'NO',
   fecha_ultima_actividad datetime,
   codigo_verificacion  varchar(12),
   estado               enum('ACTIVO', 'INACTIVO', 'POR VERIFICAR') not null default 'ACTIVO',
   primary key (id_usuario)
);

alter table ASESOR add constraint FK_REFERENCE_10 foreign key (id_usuario)
      references USUARIO (id_usuario) on delete restrict on update restrict;

alter table ASESOR_CATEGORIAS add constraint FK_REFERENCE_6 foreign key (id_asesor)
      references ASESOR (id_asesor) on delete restrict on update restrict;

alter table ASESOR_CATEGORIAS add constraint FK_REFERENCE_7 foreign key (id_categoria)
      references CATEGORIA (id_categoria) on delete restrict on update restrict;

alter table CLIENTE add constraint FK_REFERENCE_11 foreign key (id_usuario)
      references USUARIO (id_usuario) on delete restrict on update restrict;

alter table COMPRA add constraint FK_REFERENCE_24 foreign key (id_cliente)
      references CLIENTE (id_cliente) on delete restrict on update restrict;

alter table COMPRA add constraint FK_REFERENCE_25 foreign key (id_pago, id_paquete)
      references PAGO_PAQUETES (id_pago, id_paquete) on delete restrict on update restrict;

alter table COMPRA add constraint FK_REFERENCE_26 foreign key (id_consulta)
      references CONSULTA (id_consulta) on delete restrict on update restrict;

alter table CONSULTA add constraint FK_REFERENCE_16 foreign key (id_categoria)
      references CATEGORIA (id_categoria) on delete restrict on update restrict;

alter table CONSULTA add constraint FK_REFERENCE_8 foreign key (id_cliente)
      references CLIENTE (id_cliente) on delete restrict on update restrict;

alter table CONSULTA add constraint FK_REFERENCE_9 foreign key (id_asesor)
      references ASESOR (id_asesor) on delete restrict on update restrict;

alter table CONSULTA_PREGUNTA add constraint FK_REFERENCE_12 foreign key (id_consulta)
      references CONSULTA (id_consulta) on delete restrict on update restrict;

alter table CONSULTA_PREGUNTA add constraint FK_REFERENCE_13 foreign key (id_pregunta)
      references PREGUNTA (id_pregunta) on delete restrict on update restrict;

alter table CONSULTA_RESPUESTA add constraint FK_REFERENCE_14 foreign key (id_consulta_pregunta)
      references CONSULTA_PREGUNTA (id_consulta_pregunta) on delete restrict on update restrict;

alter table CONSULTA_RESPUESTA add constraint FK_REFERENCE_15 foreign key (id_respuesta)
      references RESPUESTA (id_respuesta) on delete restrict on update restrict;

alter table MENSAJE add constraint FK_REFERENCE_19 foreign key (id_usuario_remitente)
      references USUARIO (id_usuario) on delete restrict on update restrict;

alter table MENSAJE add constraint FK_REFERENCE_21 foreign key (id_usuario_destinatario)
      references USUARIO (id_usuario) on delete restrict on update restrict;

alter table MENSAJE add constraint FK_REFERENCE_22 foreign key (id_consulta)
      references CONSULTA (id_consulta) on delete restrict on update restrict;

alter table MENU add constraint FK_REFERENCE_20 foreign key (id_rol)
      references ROL (id_rol) on delete restrict on update restrict;

alter table PAGO add constraint FK_REFERENCE_17 foreign key (id_cliente)
      references CLIENTE (id_cliente) on delete restrict on update restrict;

alter table PAGO_PAQUETES add constraint FK_REFERENCE_18 foreign key (id_paquete)
      references PAQUETE (id_paquete) on delete restrict on update restrict;

alter table PAGO_PAQUETES add constraint FK_REFERENCE_27 foreign key (id_pago)
      references PAGO (id_pago) on delete restrict on update restrict;

alter table PAQUETE add constraint FK_REFERENCE_23 foreign key (id_categoria)
      references CATEGORIA (id_categoria) on delete restrict on update restrict;

alter table PREGUNTA add constraint FK_REFERENCE_4 foreign key (id_categoria)
      references CATEGORIA (id_categoria) on delete restrict on update restrict;

alter table RESPUESTA add constraint FK_REFERENCE_5 foreign key (id_pregunta)
      references PREGUNTA (id_pregunta) on delete restrict on update restrict;

alter table USUARIO add constraint FK_REFERENCE_1 foreign key (id_rol)
      references ROL (id_rol) on delete restrict on update restrict;

