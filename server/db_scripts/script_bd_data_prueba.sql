/**************************************************************************
 * DATOS EN LA TABLA USUARIO - INICIO
 **************************************************************************/
INSERT INTO USUARIO (id_usuario, clave, correo, nombres, apellidos, id_rol, imagen) VALUES ('operador@callmesee.com', 'e10adc3949ba59abbe56e057f20f883e', 'operador@callmesee.com', 'Operador', 'Prueba', 'ADVISER', 'operador@callmesee.com.JPG');
INSERT INTO USUARIO (id_usuario, clave, correo, nombres, apellidos, id_rol, imagen) VALUES ('cliente@callmesee.com', 'e10adc3949ba59abbe56e057f20f883e', 'cliente@callmesee.com', 'Cliente', 'Prueba', 'CUSTOMER', 'cliente@callmesee.com.JPG');
/**************************************************************************
 * DATOS EN LA TABLA USUARIO - FIN
 **************************************************************************/

/**************************************************************************
 * DATOS EN LA TABLA CLIENTE - INICIO
 **************************************************************************/
INSERT INTO CLIENTE (id_usuario) VALUES ('cliente@callmesee.com');
/**************************************************************************
 * DATOS EN LA TABLA CLIENTE - FIN
 **************************************************************************/

/**************************************************************************
 * DATOS EN LA TABLA ASESOR - INICIO
 **************************************************************************/
INSERT INTO ASESOR (id_usuario) VALUES ('operador@callmesee.com');
/**************************************************************************
 * DATOS EN LA TABLA ASESOR - FIN
 **************************************************************************/
