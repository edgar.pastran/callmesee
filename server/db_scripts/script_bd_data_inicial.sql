/**************************************************************************
 * DATOS EN LA TABLA ROL - INICIO
 **************************************************************************/
INSERT INTO ROL (id_rol, descripcion) VALUES ('ADMIN', 'Usuario Adminsitrador');
INSERT INTO ROL (id_rol, descripcion) VALUES ('ADVISER', 'Usuario Asesor');
INSERT INTO ROL (id_rol, descripcion) VALUES ('CUSTOMER', 'Usuario Cliente');
/**************************************************************************
 * DATOS EN LA TABLA ROL - FIN
 **************************************************************************/

/**************************************************************************
 * DATOS EN LA TABLA USUARIO - INICIO
 **************************************************************************/
INSERT INTO USUARIO (id_usuario, clave, correo, nombres, apellidos, id_rol, super_usuario, imagen) VALUES ('edgar@employgate.com', 'e10adc3949ba59abbe56e057f20f883e', 'edgar@employgate.com', 'Edgar', 'Pastran', 'ADMIN', 'SI', 'edgar@employgate.com.JPG');
INSERT INTO USUARIO (id_usuario, clave, correo, nombres, apellidos, id_rol, imagen) VALUES ('pmrada@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'pmrada@gmail.com', 'Pablo', 'Rada', 'ADMIN', 'pmrada@gmail.com.JPG');
/**************************************************************************
 * DATOS EN LA TABLA USUARIO - FIN
 **************************************************************************/
